﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Debug="true" Inherits="CAPROJ.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>U&K Login</title>
     <link rel="SHORTCUT ICON" href="images/favicon.ico"/>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="CssLogin/style.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/jQuery-2.2.0.min.js" type="text/javascript"></script>
</head>
    
<body style="overflow:hidden; background-image: url('../images/users_profile/ajaya.sual/login1.jpg'); background-repeat: no-repeat; background-size:cover">
    <center>
        <br />
        <br />
   <div class="box" style="width: 600px; height:600px; align-content: center" >
       <br />
       <br />
       <br />
   <center style="padding-top:20px">
      <img src="images/uk_logo.jpg" width="50%" height="50%"/>
       <br />
                <font color="#737373" size="10px" style="text-shadow: 0px 2px 2px gray">Admin Login</font>
       
   <div>
      <section class="main" >
         <form id="form1" class="form-1" runat="server">
            <div  id="fldUser" runat="server">
               <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
               <p class="field">
                  <asp:TextBox ID="Username" runat="server" placeholder ="Username"></asp:TextBox>
                  <i class="icon-user icon-large"></i>
               </p>
            </div>
            <div>
               <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
               <br />
               <div  id="fldPass" runat="server">
                  <p class="field">
                     <asp:TextBox ID="Password" runat="server" placeholder ="Password" TextMode="Password"></asp:TextBox>
                     <i class="icon-lock icon-large"></i>
                  </p>
               </div>
               <div>
                   <center>
                  <asp:Button ID="Login1" runat="server" Text="Login" OnClick="Login1_Click" class="btn btn-block btn-danger" BackColor="#b01717"/>
                   <asp:Label ID="lblNotification" runat="server" Text="" ForeColor="#b01717" style="font-size:large;"></asp:Label> 
                       </center>                      
               </div>
               <div>
                  <center>
                     <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Create an account?</asp:LinkButton>
                  </center>
               </div>
            </div>
         </form>
      </section>
   </div>
       </div>
   <div class="push"></div>
        </center>
</body>
    
</html>