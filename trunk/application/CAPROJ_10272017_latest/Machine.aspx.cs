﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data;
using System.Data.SqlClient;

namespace CAPROJ
{
    public partial class Machine : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadMachines();
            }


        }
        public void LoadMachines()
        {
            _SQL.getDropdown("Select * from md_Machine", "Machine_id", "Machine_name", DropDownList1);
        }
        public void LoadMachineData()
        {

            //Load Machine
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_Machine where Machine_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();
                
                MachineID.Text = dr["Machine_ID"].ToString();
                MachineName.Text = dr["Machine_name"].ToString().Trim();
                Brand.Text = dr["Machine_brand"].ToString().Trim();
                DateTime DatePurchased1 = Convert.ToDateTime(dr["Date_Purchased"].ToString().Trim());
                //if text mode=date
                DatePurchased.Text = DatePurchased1.ToString("yyyy-MM-dd");
                YOU.Text = dr["Years_use"].ToString().Trim();
                DOU.Text = dr["description"].ToString().Trim();
                Quantity.Text = dr["quantity"].ToString().Trim();

                //Disable the controls
               
                MachineName.ReadOnly = true;
                Brand.ReadOnly = true;
                DatePurchased.ReadOnly = true;
                YOU.ReadOnly = true;
                DOU.ReadOnly = true;
                Quantity.ReadOnly = true;

                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();
           
            


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMachineData();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            MachineName.ReadOnly = false;
            Brand.ReadOnly = false;
            DatePurchased.ReadOnly = false;
            YOU.ReadOnly = true;
            DOU.ReadOnly = false;
            Quantity.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                _SQL.Open();
                //Machine data
                _SQL.AddParameters("Machine_ID", MachineID.Text.Trim());
                _SQL.AddParameters("Machine_name", MachineName.Text.Trim());
                _SQL.AddParameters("machine_brand", Brand.Text.Trim());
                _SQL.AddParameters("date_purchased", DatePurchased.Text.Trim());
                _SQL.AddParameters("years_use", YOU.Text.Trim());
                _SQL.AddParameters("description", DOU.Text.Trim());
                _SQL.AddParameters("quantity", Quantity.Text.Trim());
                string query = @"INSERT INTO md_Machine(Machine_name,machine_brand,date_purchased,years_use,description,quantity)
                                       VALUES(@Machine_name,@machine_brand,@date_purchased,@years_use,@description,@quantity);";

                _SQL.ExecuteNonQuery(query);
                _SQL.Close();


                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Machine Successfully Added!'); window.location='" + Request.ApplicationPath + "MachineList.aspx';", true);
            }
            else
            {
                _SQL.Open();
                _SQL.AddParameters("Machine_ID", MachineID.Text.Trim());
                _SQL.AddParameters("Machine_name", MachineName.Text.Trim());
                _SQL.AddParameters("machine_brand", Brand.Text.Trim());
                _SQL.AddParameters("date_purchased", DatePurchased.Text.Trim());
                _SQL.AddParameters("years_use", YOU.Text.Trim());
                _SQL.AddParameters("description", DOU.Text.Trim());
                _SQL.AddParameters("quantity", Quantity.Text.Trim());
                _SQL.ExecuteNonQuery(@"UPDATE md_Machine SET Machine_name=@Machine_name,machine_brand=@machine_brand,
                                           date_purchased=@date_purchased,years_use=@years_use,description=@description,quantity=@quantity
                                          WHERE Machine_ID=@Machine_ID");
                _SQL.Close();

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Machine Successfully Updated!'); window.location='" + Request.ApplicationPath + "Machine.aspx';", true);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_Machine WHERE Machine_ID='" + MachineID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Machine Successfully Deleted!'); window.location='" + Request.ApplicationPath + "Machine.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Machine.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            MachineID.Text = "";
            MachineName.Text = "";
            Brand.Text = "";
            DatePurchased.Text = "";
            YOU.Text = "";
            DOU.Text = "";
            Quantity.Text = "";

            //Enable the controls
            MachineName.ReadOnly = false;
            Brand.ReadOnly = false;
            DatePurchased.ReadOnly = false;
            YOU.ReadOnly = true;
            DOU.ReadOnly = false;
            Quantity.ReadOnly = false;
            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }

        protected void DatePurchased_TextChanged(object sender, EventArgs e)
        {
            _SQL.Open();
            YOU.Text = _SQL.ExecuteScalar("select dbo.CalculateYearsOfUse('" + DatePurchased.Text + "',getdate())").ToString();
            _SQL.Close();
        }
    }

}