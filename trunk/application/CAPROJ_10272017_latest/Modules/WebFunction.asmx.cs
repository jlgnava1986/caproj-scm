﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using CAPROJ.Modules;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CAPROJ.WebFunctions
{
    /// <summary>
    /// Summary description for WebFunctions
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [System.Web.Script.Services.ScriptService]

    public class WebFunctions : System.Web.Services.WebService
    {
        [WebMethod]
        public void SetControlClear(Control page)
        {
            foreach (Control ctrl in page.Controls)
            {
                if (ctrl is TextBox)
                {
                    if (((TextBox)(ctrl)).AccessKey != "F")
                    {
                        ((TextBox)(ctrl)).Text = "";
                    }
                }

                if (ctrl.Controls.Count > 0)
                {
                    // Use recursion to find all nested controls
                    SetControlClear(ctrl);
                }
            }
        } // End Here

        // Enable/Disable controls in page 
        [WebMethod]
        public void SetEnableControls(Control page, bool enable)
        {
            foreach (Control ctrl in page.Controls)
            {

                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Enabled = enable;
                }

                if (ctrl.Controls.Count > 0)
                {
                    // Use recursion to find all nested controls
                    SetEnableControls(ctrl, enable);
                }
            }
        } // End Here

        //Encrypt Passwords
        [WebMethod]
        public string EncryptData(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            //  Convert strings into byte arrays.
            //  Let us assume that strings only contain ASCII codes.
            //  If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            //  encoding.
            byte[] initVectorBytes;
            initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes;
            saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            //  Convert our plaintext into a byte array.
            //  Let us assume that plaintext contains UTF8-encoded characters.
            byte[] plainTextBytes;
            plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            //  First, we must create a password, from which the key will be derived.
            //  This password will be generated from the specified passphrase and 
            //  salt value. The password will be created using the specified hash 
            //  algorithm. Password creation can be done in several iterations.
            PasswordDeriveBytes password;
            password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            //  Use the password to generate pseudo-random bytes for the encryption
            //  key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes;
            keyBytes = password.GetBytes((keySize / 8));
            //  Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey;
            symmetricKey = new RijndaelManaged();
            //  It is reasonable to set encryption mode to Cipher Block Chaining
            //  (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;
            //  Generate encryptor from the existing key bytes and initialization 
            //  vector. Key size will be defined based on the number of the key 
            //  bytes.
            ICryptoTransform encryptor;
            encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            //  Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream;
            memoryStream = new MemoryStream();
            //  Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream;
            cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            //  Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            //  Finish encrypting.
            cryptoStream.FlushFinalBlock();
            //  Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes;
            cipherTextBytes = memoryStream.ToArray();
            //  Close both streams.
            memoryStream.Close();
            cryptoStream.Close();
            //  Convert encrypted data into a base64-encoded string.
            string cipherText;
            cipherText = Convert.ToBase64String(cipherTextBytes);
            //  Return encrypted string.
            return cipherText;
        }

        //This functions are for Document Processing        
        [WebMethod]
        public string GetProcessID(string strDocID, string strRefNo) {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                SqlDataReader dr = cn.ExecuteReader("EXEC SP_GET_PROCESS_ID @docid, @refno");
                if (dr.HasRows)
                {
                    dr.Read();
                    return dr[0].ToString();
                }
                else {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
            finally
            {
                cn.Close();
            }
        }


        [WebMethod]
        public string ReadNotif()
        {

            // string post_id = Request.Params["post_id"];
            SQLProcedures cn = new SQLProcedures();
            try
            {

                cn.Open();
                cn.ClearParameters();

                cn.AddParameters("entity_id", Session["vali"].ToString());
                // cn cn.AddParameters("userid", Session["user_id"].ToString());.AddParameters("userid", Session["user_id"].ToString());
           
                string query = "UPDATE tf_notification SET  bt_isread = 1 WHERE ch_entity_id =@entity_id ; ";

                cn.ExecuteNonQuery(query);
                //  Response.Redirect("NewsFeed.aspx");

                return "success";
            }
            catch (Exception ex)
            {
                return "error";
            }
            finally
            {
                cn.Close();

            }


            //   return GetNewsFeedData(pageIndex).GetXml();
        }



        [WebMethod]
        public string GetNextProcessID(string strDocID, string strRefNo)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                SqlDataReader dr = cn.ExecuteReader("EXEC SP_GET_NEXT_PROCESS_ID @docid, @refno");
                if (dr.HasRows)
                {
                    dr.Read();
                    return dr[0].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
            finally
            {
                cn.Close();
            }
        }

        [WebMethod]
        public Boolean CheckProcessIfPosted(string strDocID, string strRefNo)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                SqlDataReader dr = cn.ExecuteReader("EXEC SP_CHECK_PROCESS_IF_POSTED @docid, @refno");
                if (dr.HasRows)
                {
                    dr.Read();
                    return Convert.ToBoolean(dr[0]);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        [WebMethod]
        public void InsertDocsProcessHistory(string strDocID, string strRefNo, string nextprocessID)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                cn.AddParameters("processID", nextprocessID);
                cn.AddParameters("userID", Session["user_id"].ToString());
                cn.ExecuteNonQuery("EXEC SP_INSERT_DOCS_APPROVAL_HISTORY @docid, @refno, @processID, @userID");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        [WebMethod]
        public void InsertDocsProcessHistoryDisapprove(string strDocID, string strRefNo)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                cn.AddParameters("userID", Session["user_id"].ToString());
                cn.ExecuteNonQuery("EXEC SP_INSERT_DOCS_APPROVAL_HISTORY_DISAPPROVED @docid, @refno, @userID");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        [WebMethod]
        public void LevelUpDocumentProcess(string strDocID, string strRefNo, string nextprocessID)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                cn.AddParameters("processID", nextprocessID);
                cn.ExecuteNonQuery("EXEC SP_GET_DOCUMENT_APPROVED @docid, @refno, @processID");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        [WebMethod]
        public void DisapproveDocument(string strDocID, string strRefNo)
        {
            SQLProcedures cn = new SQLProcedures();
            PublicProperties PublicProperty = new PublicProperties();
            try
            {
                cn.Open();
                cn.AddParameters("docid", strDocID);
                cn.AddParameters("refno", strRefNo);
                cn.ExecuteNonQuery("EXEC SP_GET_DOCUMENT_DISAPPROVED @docid, @refno");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
    
        //For Crystal Reports
        [WebMethod]

         public void GenerateReport(string strReportName,string strSubReportName,string strFormReportTitle,string strReportPath, 
                string[] strFormulaParam,string[] strFormulaValue,DataTable dtMain,DataTable dtSub,DataSet ds,int int_Height,int int_Width, 
                bool bln_enablePrint,int intPaperSize,string projectcode,string coid,string branchid, ReportDocument rpt) 
        {

            string pubReportText;
            ReportDocument pubReportDocument = rpt;
            FormulaFieldDefinitions pubFormulaFields;
            FormulaFieldDefinition pubFormulaField;

            ConnectionInfo Connection_Info = new ConnectionInfo();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            Tables CrTables;
           // CrystalDecisions.CrystalReports.Engine.Table CrTable;

            try {    
                pubReportDocument = new ReportDocument();
                pubReportDocument.Load(strReportPath.Trim().Replace("'",""));
                Connection_Info.ServerName = "10.100.100.10";
                Connection_Info.DatabaseName = "EDMS";
                Connection_Info.UserID = "sa";
                Connection_Info.Password = "sqladmin";
                CrTables = pubReportDocument.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables) {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = Connection_Info;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }
    
                foreach (ReportDocument subrep in pubReportDocument.Subreports) {
                    foreach (CrystalDecisions.CrystalReports.Engine.Table table in subrep.Database.Tables)
                    {
                        table.LogOnInfo.ConnectionInfo = Connection_Info;
                        table.ApplyLogOnInfo(table.LogOnInfo);
                    }
        
                }
    
                pubFormulaFields = pubReportDocument.DataDefinition.FormulaFields;
                if(pubFormulaFields.Count > 0 && strFormulaParam != null)
                {
                   for (int i = 0; (i <= (strFormulaParam.Length - 1)); i++) 
                   {
                        if ((strFormulaParam[i].Trim().Length > 0)) {
                            pubReportDocument.DataDefinition.FormulaFields[strFormulaParam[i]].Text = strFormulaValue[i];
                        }                   
                   }
                }
    
                //if (dtMain) {
                //    IsNot;
                //    null;
                //    pubReportDocument.Database.Tables[0].SetDataSource(dtMain);
                //}
                //else if (ds) {
                //    IsNot;
                //    null;
                //    pubReportDocument.Database.Tables[0].SetDataSource(ds.Tables[0]);
                //} 
    
                pubReportText = strFormReportTitle.Trim();

                //pubBln_EnablePrinting = bln_enablePrint;
                if (!(projectcode == "")) {
                    pubReportDocument.SetParameterValue("projcode", projectcode.Trim());
                }
    
                if (!(coid == "")) {
                    pubReportDocument.SetParameterValue("coid", coid.Trim());
                }
        
                if (!(branchid == "")) {
                    pubReportDocument.SetParameterValue("@branchid", branchid.Trim());
                }
            
            }
            catch (Exception ex) {
            }

        }


    }
}
