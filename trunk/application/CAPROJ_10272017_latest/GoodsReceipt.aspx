﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="GoodsReceipt.aspx.cs" Inherits="CAPROJ.GoodsReceipt" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Goods Receipt
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <%--HEADER--%>
                                <div class="col-md-12">
                                    <div class="row">
                                            <div class="col-md-2">
                                                <small>Goods Receipt #</small>
                                                <asp:TextBox runat="server" id="txtGoodsReceipt" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                    </div>
                                   <div id="printArea" class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <small>Purchase Order #</small>
                                                <asp:DropDownList ID="dropPO" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="dropPO_SelectedIndexChanged1"></asp:DropDownList>
                                            </div>                                
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                 <small>Supplier Name</small>
                                                 <asp:TextBox ID="txtSupplierName" runat="server" CssClass="form-control" placeholder="Supplier Name" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-2">
                                            <small>Delivery Date</small>
                                            <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control" placeholder="DeliveryDate" ReadOnly="true" ></asp:TextBox>
                                         </div>
                                        <div class="col-md-2">
                                            <small>Date Order</small>
                                            <asp:TextBox ID="txtDateOrder" runat="server" CssClass="form-control" placeholder="Date Order" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <small>Date Received</small>
                                            <asp:TextBox  id="txtDateReceived" runat="server" CssClass="form-control" placeholder="Date Received" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>  
                                </div> 
                                  
                                <%--END HEADER--%>

                                <%--GRIDVIEW--%>
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridGoodsReceipt" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true" ShowFooter="true"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Item Name">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="item_name" runat="server" class="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Remaining Quantity">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="quantity" runat="server" class="form-control" TextMode="Number" OnTextChanged="quantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Received Quantity">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="received_quantity" runat="server" class="form-control" TextMode="Number"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Unit Price">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="unit_price" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Discount %">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="discount" runat="server" class="form-control" TextMode="Number"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tax %">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="tax" runat="server" class="form-control" TextMode="Number" Enabled="false"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    Total Payment Due:
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="total" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="Right" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="totalPaymentDue" runat="server" class="form-control" Font-Bold="true"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="white" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                                
                              <%--END GRIDVIEW--%>

                            </div>
                        </div>
                        <%--SAVE BUTTON--%>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtRemarks" runat="server" class="form-control" placeholder="Remarks" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-block btn-danger" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                            </div>
                            <div class="col-md-3">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-block btn-danger" BackColor="#b01717" OnClick="btnCancel_Click"></asp:Button>
                            </div>
                            <div class="col-md-2">
                                <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnPrint" Text='Print' OnClientClick="printContent('printArea')" BackColor="#b01717"></asp:Button>
                            </div>
                        </div>

                        <%--END SAVE BUTTON--%>

                    </div>
                </ContentTemplate>
                <Triggers>
                    
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
     <script type="text/javascript">
        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>
</asp:Content>

