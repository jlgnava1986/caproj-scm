﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SLedger1.aspx.cs" Inherits="ETIIOnlineServices.SLedger1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Commission Ledger</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
                <div class="col-xs-12 no-padding">
                    <%--<div class="box" style="padding:10px 10px 10px 10px">--%>
                        <div class="row">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-xs-12">
                                Note: For your question with regards to your account please proceed or call Account Management Department
                                <div class="row">
                                    <div class="pull-left col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <h3><asp:Label ID="lblHeader" runat="server" Text="Label">Receivables</asp:Label></h3>
                                    </div>
                                    <div class="pull-left col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 0px 15px">
                                        <asp:DropDownList ID="dropFilter1" runat="server" CssClass="form-control" OnSelectedIndexChanged="dropFilter_OnSelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="Updated - Complete Docs">Updated - Complete Docs</asp:ListItem>
                                            <asp:ListItem Value="Updated - Incomplete Initial Docs">Updated - Incomplete Initial Docs</asp:ListItem>
                                            <asp:ListItem Value="Updated - Incomplete Additional Docs">Updated - Incomplete Additional Docs</asp:ListItem>
                                            <asp:ListItem Value="Past Due - Complete Docs">Past Due - Complete Docs</asp:ListItem>
                                            <asp:ListItem Value="Past Due - Incomplete Docs">Past Due - Incomplete Docs</asp:ListItem>
                                            <asp:ListItem Value="Cancelled Accounts">Cancelled Accounts</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="ALL">All Accounts</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="pull-left col-xs-12 col-sm-8 col-md-8 col-lg-8 no-padding">
                                        <h3><asp:Label ID="Label1" runat="server" Text="Label">Summary</asp:Label></h3>
                                </div>
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridSummaryCommission" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="caption" HeaderText="" ItemStyle-Font-Bold="true">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="total_count"  ItemStyle-Font-Bold="true" HeaderText="Count">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="gross_total"  ItemStyle-Font-Bold="true" HeaderText="Gross" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="released_total"  ItemStyle-Font-Bold="true" HeaderText="Released" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="balanced_total"  ItemStyle-Font-Bold="true" HeaderText="Remaining" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridCommRec" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"
                                        OnPageIndexChanging="GridCommRec_PageIndexChanging"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="vc_client_name" HeaderText="Client Name" Visible="true">
                                                <HeaderStyle HorizontalAlign="Left" Width="380px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="ch_unit_id" HeaderText="Unit ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="160px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="nu_comm_rate" HeaderText="Rate">
                                                <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="mo_tcp" HeaderText="TCP" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="190px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="mo_comm_amount" HeaderText="Gross" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="ReleaseComm" HeaderText="Released" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="BalanceComm" HeaderText="Remaining" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_type" HeaderText="Type" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_aging_desc" HeaderText="Account Status" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_res_status" HeaderText="Reservation Status">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="sd_res_date" HeaderText="Date Cancelled" dataformatstring="{0:MM/dd/yyyy}">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_reason_desc" HeaderText="Reason for Cancellation">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    <%--</div>   --%>     
                </div>
        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</asp:Content>

