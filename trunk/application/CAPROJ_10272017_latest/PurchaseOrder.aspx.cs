﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class PurchaseOrder : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }

                LoadSupplierList();
                SetInitialRow();
            }

        }

        public void LoadSupplierList()
        {
            _SQL.getDropdown("select * from md_supplier", "Supplier_ID", "supplier_name", dropSupplierSelection);
        }

        private void SetInitialRow()
        {
            //nagcreate tayo ng new datatable
            DataTable dt = new DataTable();
            DataRow dr = null;
            //nag add tayo ng new columns, which is based dun sa columns ng gridview na inadd mo kanina
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("product_name", typeof(string)));
            dt.Columns.Add(new DataColumn("quantity", typeof(int)));
            dt.Columns.Add(new DataColumn("unit_price", typeof(double)));
            dt.Columns.Add(new DataColumn("discount", typeof(double)));
            dt.Columns.Add(new DataColumn("tax", typeof(double)));
            dt.Columns.Add(new DataColumn("total", typeof(double)));

            //nagcreate tayo ng new blank row
            dr = dt.NewRow();
            //then nag add tayo ng default value
            dr["RowNumber"] = "1";
            dr["product_name"] = string.Empty;
            dr["quantity"] = "0";
            dr["unit_price"] = "0";
            dr["discount"] = "0";
            dr["tax"] = "12";
            dr["total"] = "0";
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridPurchaseOrder.DataSource = dt;
            GridPurchaseOrder.DataBind();

            //Change CSS of disabled textboxs
            TextBox quantity = (TextBox)GridPurchaseOrder.Rows[0].Cells[2].FindControl("quantity");
            TextBox unitPrice = (TextBox)GridPurchaseOrder.Rows[0].Cells[3].FindControl("unit_price");
            TextBox discount = (TextBox)GridPurchaseOrder.Rows[0].Cells[4].FindControl("discount");
            TextBox tax = (TextBox)GridPurchaseOrder.Rows[0].Cells[5].FindControl("tax");
            TextBox total = (TextBox)GridPurchaseOrder.Rows[0].Cells[6].FindControl("total");
            quantity.Text = "0";
            unitPrice.Text = "0";
            //disable lang yung unitprice
            unitPrice.Enabled = false;
            //add lang ng css
            unitPrice.CssClass = "form-control";
            discount.Text = "0";
            tax.Text = "12";
            tax.Enabled = false;
            tax.CssClass = "form-control";
            total.Text = "0";
            total.Enabled = false;
            total.CssClass = "form-control";

            //Load Items on Dropdownlist
            //nag add tayo ng list dun sa dropdown ng items based dun sa sinelect na supplier
            DropDownList dropItemList = (DropDownList)GridPurchaseOrder.Rows[0].Cells[1].FindControl("dropItems");
            dropItemList.CssClass = "form-control";
            _SQL.getDropdown("select * from md_supplier_products where Supplier_ID='" + dropSupplierSelection.SelectedValue.ToString().Trim() + "'", "product_name", "product_name", dropItemList);

            //questions? gets ko po

        }

        protected void dropSupplierSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Load Items on Dropdownlist
                        //eto yung dropdown dun sa gridview
                        DropDownList dropItemList = (DropDownList)GridPurchaseOrder.Rows[rowIndex].Cells[1].FindControl("dropItems");
                        //set lang ng css
                        dropItemList.CssClass = "form-control";
                        //then populate yung list ng items based sa pinili ko na supplier
                        _SQL.getDropdown("select * from md_supplier_products where Supplier_ID='" + dropSupplierSelection.SelectedValue.ToString().Trim() + "'", "product_name", "product_name", dropItemList);
                        //dropItemList.SelectedIndexChanged += new EventHandler(dropItemList_SelectedIndexChanged);

                        rowIndex++;
                        //questions? gets po
                    }
                }
            }
        }
        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //list ng columns dun sa gridview
                        DropDownList itemList = (DropDownList)GridPurchaseOrder.Rows[rowIndex].Cells[2].FindControl("dropItems");
                        TextBox quantity = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[2].FindControl("quantity");
                        TextBox unitPrice = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[3].FindControl("unit_price");
                        TextBox discount = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[4].FindControl("discount");
                        TextBox tax = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[5].FindControl("tax");
                        TextBox total = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[6].FindControl("total");

                        //disable unit price
                        unitPrice.Enabled = false;
                        //add css
                        unitPrice.CssClass = "form-control";
                        tax.Enabled = false;
                        tax.CssClass = "form-control";
                        total.Enabled = false;
                        total.CssClass = "form-control";

                        //create new row
                        drCurrentRow = dtCurrentTable.NewRow();

                        //add value on row
                        dtCurrentTable.Rows[i - 1]["product_name"] = itemList.SelectedValue.ToString();
                        dtCurrentTable.Rows[i - 1]["quantity"] = quantity.Text;
                        dtCurrentTable.Rows[i - 1]["unit_price"] = unitPrice.Text;
                        dtCurrentTable.Rows[i - 1]["discount"] = discount.Text;
                        dtCurrentTable.Rows[i - 1]["tax"] = tax.Text;
                        dtCurrentTable.Rows[i - 1]["total"] = total.Text;

                        rowIndex++;
                    }
                    //set the new row to the gridview
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    GridPurchaseOrder.DataSource = dtCurrentTable;
                    GridPurchaseOrder.DataBind();

                    //questions? kuya pano po yung purchase order number? automatic po ba yun?
                    //yes automatic yun, pero mamaya pa natin iadd yun upon click ng submit button ok po gets

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            //everytime na iciclick yung ButtonAdd, icacall niya yung function na AddNewRowToGrid
            AddNewRowToGrid();
        }

        //bale yung SetPreviousData, same lang siya ng AddNewRowToGrid, parang dinidisplay niya lang sa gridview yung mga data ng previous rows na inadd mo

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //list ng columns
                        DropDownList itemList = (DropDownList)GridPurchaseOrder.Rows[rowIndex].Cells[1].FindControl("dropItems");
                        TextBox quantity = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[2].FindControl("quantity");
                        TextBox unitPrice = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[3].FindControl("unit_price");
                        TextBox discount = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[4].FindControl("discount");
                        TextBox tax = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[5].FindControl("tax");
                        TextBox total = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[6].FindControl("total");
                        //add items on new row - dropdownlist
                        _SQL.getDropdown("select * from md_supplier_products where Supplier_ID='" + dropSupplierSelection.SelectedValue.ToString().Trim() + "'", "product_name", "product_name", itemList);

                        //add css
                        itemList.CssClass = "form-control";
                        //add values
                        itemList.SelectedValue = dt.Rows[i]["product_name"].ToString();
                        if (dt.Rows[i]["quantity"].ToString() != "")
                        {
                            quantity.Text = dt.Rows[i]["quantity"].ToString();
                        }
                        else
                        {
                            quantity.Text = "0";
                        }
                        if (dt.Rows[i]["unit_price"].ToString() != "")
                        {
                            double price = Convert.ToDouble(dt.Rows[i]["unit_price"].ToString());
                            unitPrice.Text = price.ToString("N");
                        }
                        else
                        {
                            unitPrice.Text = "0";
                        }
                        unitPrice.CssClass = "form-control";
                        if (dt.Rows[i]["discount"].ToString() != "")
                        {
                            discount.Text = dt.Rows[i]["discount"].ToString();
                        }
                        else
                        {
                            discount.Text = "0";
                        }
                        //tax, fixed 12 %
                        if (dt.Rows[i]["tax"].ToString() != "")
                        {
                            tax.Text = dt.Rows[i]["tax"].ToString();
                        }
                        else
                        {
                            tax.Text = "12";
                        }
                        tax.CssClass = "form-control";
                        if (dt.Rows[i]["total"].ToString() != "")
                        {
                            double totalAmt = Convert.ToDouble(dt.Rows[i]["total"].ToString());
                            total.Text = totalAmt.ToString("N");
                        }
                        else
                        {
                            total.Text = "0";
                        }
                        total.CssClass = "form-control";

                        rowIndex++;
                    }

                    //questions? gets na po
                }
            }

        }

        //everytime na magpapalit tayo ng selection sa items, icacall niya yung function na to
        protected void dropItemList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //eto yung pang identify kung anong row yung sinelect natin
            DropDownList selected = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)selected.Parent.Parent);
            //eto yung price(textbox)
            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");

            //nagquery tayo, kinuha natin sa databse kung ano ang price ng item na pinili natin
            _SQL.Open();
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_supplier_products where product_name='" + selected.SelectedValue.ToString().Trim() + "'");
            if (dr.HasRows)
            {
                dr.Read();
                double price = Convert.ToDouble(dr["price"].ToString());
                //inadd natin yung value ng price ng item
                unitPrice.Text = price.ToString("N");
            }

            //start computation
            TextBox txtQuantity = (TextBox)grdrDropDownRow.FindControl("quantity");
            TextBox txtDiscount = (TextBox)grdrDropDownRow.FindControl("discount");
            TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
            //yung computetotalperrow, yan yung magcocompute ng total amount per row
            double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
            txtTotal.Text = total.ToString("N");

            //add total amount on footer
            //compute natin yung total amount dun sa footer, sum up lang natin lahat ng totals per row
            TextBox txtTotalAmountDue = (TextBox)GridPurchaseOrder.FooterRow.FindControl("totalPaymentDue");
            double totalAmountDue = 0;
            for (int i = 0; i < GridPurchaseOrder.Rows.Count; i++)
            {
                TextBox amount = (TextBox)GridPurchaseOrder.Rows[i].Cells[6].FindControl("total");
                totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
            }
            //disable lang yung totalamount dun sa baba
            txtTotalAmountDue.Enabled = false;
            //add css
            txtTotalAmountDue.CssClass = "form-control";
            txtTotalAmountDue.Text = totalAmountDue.ToString("N");
        }

        //add tayo ng ComputeTotalPerRow
        public double ComputeTotalPerRow(double quantity, double price, double discountPercentage)
        {
            double total = 0;
            double origPrice = 0;
            double discountAmount = 0;
            double discountedPrice = 0;
            double taxAmount = 0;

            //formula ng computation
            origPrice = quantity * price;
            discountAmount = origPrice * (discountPercentage / 100);
            discountedPrice = origPrice - discountAmount;
            taxAmount = discountedPrice * .12;
            total = discountedPrice + taxAmount;

            return total;
        }

        //same computation lang siya kagaya nung sa taas
        //tama, parang ganito lang ang ginawa natin
        //nag add tayo ng event sa quantity(textbox)
        //na everytime na magpapalit yung value niya, magrerecompute tayo ng total gets po

        protected void quantity_TextChanged(object sender, EventArgs e)
        {
            TextBox txtQuantity = (TextBox)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)txtQuantity.Parent.Parent);
            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");

            //start computation
            TextBox txtDiscount = (TextBox)grdrDropDownRow.FindControl("discount");
            TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
            double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
            txtTotal.Text = total.ToString("N");

            //add total amount on footer
            TextBox txtTotalAmountDue = (TextBox)GridPurchaseOrder.FooterRow.FindControl("totalPaymentDue");
            double totalAmountDue = 0;
            for (int i = 0; i < GridPurchaseOrder.Rows.Count; i++)
            {
                TextBox amount = (TextBox)GridPurchaseOrder.Rows[i].Cells[6].FindControl("total");
                totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
            }
            txtTotalAmountDue.Text = totalAmountDue.ToString("N");
        }

        //same computation lang sila ni unit quantity ok po
        protected void discount_TextChanged(object sender, EventArgs e)
        {
            TextBox txtDiscount = (TextBox)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)txtDiscount.Parent.Parent);
            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");

            //start computation
            TextBox txtQuantity = (TextBox)grdrDropDownRow.FindControl("quantity");
            TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
            double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
            txtTotal.Text = total.ToString("N");

            //add total amount on footer
            TextBox txtTotalAmountDue = (TextBox)GridPurchaseOrder.FooterRow.FindControl("totalPaymentDue");
            double totalAmountDue = 0;
            for (int i = 0; i < GridPurchaseOrder.Rows.Count; i++)
            {
                TextBox amount = (TextBox)GridPurchaseOrder.Rows[i].Cells[6].FindControl("total");
                totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
            }
            txtTotalAmountDue.Text = totalAmountDue.ToString("N");
        }

        private void UpdateDataTableBeforeSaving()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridPurchaseOrder.Rows[rowIndex].Cells[2].FindControl("dropItems");
                        TextBox quantity = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[2].FindControl("quantity");
                        TextBox unitPrice = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[3].FindControl("unit_price");
                        TextBox discount = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[4].FindControl("discount");
                        TextBox tax = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[5].FindControl("tax");
                        TextBox total = (TextBox)GridPurchaseOrder.Rows[rowIndex].Cells[6].FindControl("total");
                        //_SQL.getDropdown("select * from md_supplier_products where Supplier_ID='" + dropSupplierSelection.SelectedValue.ToString().Trim() + "'", "product_name", "product_name", itemList);

                        unitPrice.Enabled = false;
                        unitPrice.CssClass = "form-control";
                        tax.Enabled = false;
                        tax.CssClass = "form-control";
                        total.Enabled = false;
                        total.CssClass = "form-control";

                        drCurrentRow = dtCurrentTable.NewRow();

                        dtCurrentTable.Rows[i - 1]["product_name"] = itemList.SelectedValue.ToString();
                        dtCurrentTable.Rows[i - 1]["quantity"] = quantity.Text;
                        dtCurrentTable.Rows[i - 1]["unit_price"] = unitPrice.Text;
                        dtCurrentTable.Rows[i - 1]["discount"] = discount.Text;
                        dtCurrentTable.Rows[i - 1]["tax"] = tax.Text;
                        dtCurrentTable.Rows[i - 1]["total"] = total.Text;

                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    //GridPurchaseOrder.DataSource = dtCurrentTable;
                    //GridPurchaseOrder.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //revision
            //check if delivery date not less than date order
            //if (Convert.ToDateTime(txtDateDelivery.Text) > Convert.ToDateTime(txtDateOrder.Text))
            //{
            //insert purchase order header
            //insert lang tayo sa table md_purchase_order_header base dun sa pinili natin na supplier at saka kung ano
            //nilagay natin sa date delivery & date order, remarks
            _SQL.Open();
            _SQL.AddParameters("supplier_id", dropSupplierSelection.SelectedValue.ToString().Trim());
            _SQL.AddParameters("delivery_date", txtDateDelivery.Text);
            _SQL.AddParameters("date_order", txtDateOrder.Text);
            _SQL.AddParameters("remarks", txtRemarks.Text);
            _SQL.ExecuteNonQuery("insert into md_purchase_order_header(Supplier_ID,delivery_date,date_order,remarks,status)values(@supplier_id,@delivery_date,@date_order,@remarks,'active')");
            _SQL.Close();

            //insert purchase order details
            //yung function na to, ang ginagawa niya lang is kinukuha niya lang yung values na nakainput sa gridview
            UpdateDataTableBeforeSaving();

            string query = "";
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            _SQL.Open();
            //then sa part na to, sinesave na natin yung values na yun sa database
            foreach (DataRow row in dt.Rows)
            {
                if (row["product_name"].ToString() != "")
                {
                    query = query + @"INSERT INTO md_purchase_order_detail(PurchaseOrder_ID,product_name,quantity,unit_price,discount,tax,total) 
                                     VALUES((select max(PurchaseOrder_ID) from md_purchase_order_header),'" + row["product_name"].ToString() + "','" + row["quantity"].ToString() + "','" + row["unit_price"].ToString() + "','" + row["discount"].ToString() +
                                            "','" + row["tax"].ToString() + "','" + row["total"].ToString() + "'); ";
                }
            }
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            //messagebox lang, tapos refresh ang page
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Purchase Order Successfully Submitted!'); window.location='" + Request.ApplicationPath + "PurchaseOrder.aspx';", true);
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Delivery Date should be greater than the Purchase Date!');", true);
        //}           
    

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //refresh page upon clicking the button cancel
        Response.Redirect("PurchaseOrder.aspx");
    }
}
}