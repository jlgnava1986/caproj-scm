﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CAPROJ
{
    public partial class SalesOrderReport : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadSalesOrderReport(txtFilter.Text);
                LoadReport();
            }
        
        }

        public void LoadSalesOrderReport(string filter)
        {
            string SQL = "select * from dbo.md_sales_orders_report where ('" + filter + "'='') or ('" + filter + "'<>'' and (cnt like '%' + '" + filter + "' +'%' or customer_name like '%' + '" + filter + "' +'%' or sales_order like '%' + '" + filter + "' +'%' or total_price like '%' + '" + filter + "' + '%'))";
            GridSalesOrder.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSalesOrder.DataSource = dt;
                GridSalesOrder.DataBind();
                GridSalesOrder.Visible = true;
                ViewState["ViewingTable"] = dt;
            }
        }
        public void LoadReport()
        {
            ReportDocument reportviewer = new ReportDocument();
            FormulaFieldDefinitions pubFormulaFields;
            FormulaFieldDefinition pubFormulaField;

            ConnectionInfo Connection_Info = new ConnectionInfo();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            Tables CrTables;

            string[] strFormulaParam = { "printedby" };
            string[] strFormulaValue = { "'" + Session["fullname"].ToString() + "'" };
            reportviewer.Load(Server.MapPath("Report/SalesOrderReport1.rpt"));

            Connection_Info.ServerName = "LAPTOP-AI3BIIL8\\ALYSSA";
            Connection_Info.DatabaseName = "CAPROJ";
            Connection_Info.UserID = "sa";
            Connection_Info.Password = "abc@123";
            CrTables = reportviewer.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = Connection_Info;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            foreach (ReportDocument subrep in reportviewer.Subreports)
            {
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in subrep.Database.Tables)
                {
                    table.LogOnInfo.ConnectionInfo = Connection_Info;
                    table.ApplyLogOnInfo(table.LogOnInfo);
                }

            }

            DataTable dtMain = (DataTable)ViewState["ViewingTable"];
            reportviewer.Database.Tables[0].SetDataSource(dtMain);

            CrystalReportViewer1.ReportSource = reportviewer;

            pubFormulaFields = reportviewer.DataDefinition.FormulaFields;
            if (pubFormulaFields.Count > 0 && strFormulaParam != null)
            {
                for (int i = 0; (i <= (strFormulaParam.Length - 1)); i++)
                {
                    if ((strFormulaParam[i].Trim().Length > 0))
                    {
                        reportviewer.DataDefinition.FormulaFields[strFormulaParam[i]].Text = strFormulaValue[i];
                    }
                }
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadSalesOrderReport(txtFilter.Text);
            LoadReport();
        }
    }
}