﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using CAPROJ.Modules;
using System.Web.Script.Services;
using System.Threading.Tasks;
using SendGrid;
using System.Net.Mail;
using System.Net;
using NativeExcel.Core;
using System.Text;
using System.IO;

namespace CAPROJ.Modules
{
    /// <summary>
    /// Summary description for SQLProcedures
    /// Link: https://gist.github.com/netdeveloperexpert/721727f61673f8b017fbd0cfc0987084
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SQLProcedures : System.Web.Services.WebService
    {

        public string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        SqlConnection dataCon;
        SqlTransaction dataTrn;
        SqlCommand dataCmd;
    
        public delegate void EventHandler(object state);
        public static event EventHandler EventState;

        public SQLProcedures() {
            dataCon = new SqlConnection(ConString);
            dataCmd = new SqlCommand("", dataCon);
            dataTrn = null;
        }

        public object Connection
        {
            get { return dataCon; }
        }

        public void Open()
        {
            //EventState(ConnectionState.Connecting);
            if (dataCon.State == ConnectionState.Broken |
                dataCon.State == ConnectionState.Closed) dataCon.Open();
            dataCmd.Parameters.Clear();
            dataTrn = null;
        }

        public void AddParameters(string prmName, object value)
        {
            dataCmd.Parameters.AddWithValue(prmName, value == null ? System.DBNull.Value : value);
        }

        public void ClearParameters()
        {
            dataCmd.Parameters.Clear();
        }

        public void BeginTrans()
        {
            if (dataCon.State == ConnectionState.Open)
                dataTrn = dataCon.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void CommitTrans()
        {
            if (dataTrn != null) dataTrn.Commit();
        }

        public void RollbackTrans()
        {
            if (dataTrn != null) dataTrn.Rollback();
        }

        [WebMethod]
        public SqlDataReader ExecuteReader(string cmdText, CommandType cmdtype = CommandType.Text)
        {
            //EventState(ConnectionState.Executing);
            if (dataCon.State == ConnectionState.Open)
            {
                dataCmd.CommandTimeout = dataCon.ConnectionTimeout;
                dataCmd.CommandText = cmdText;
                dataCmd.CommandType = cmdtype;
                //EventState(ConnectionState.Closed);
                return dataCmd.ExecuteReader();
            }
            else return null;
        }

        [WebMethod]
        public object ExecuteScalar(string cmdText, CommandType cmdtype = CommandType.Text)
        {
            //EventState(ConnectionState.Executing);
            if (dataCon.State == ConnectionState.Open)
            {
                dataCmd.CommandTimeout = dataCon.ConnectionTimeout;
                dataCmd.CommandText = cmdText;
                dataCmd.CommandType = cmdtype;
                //EventState(ConnectionState.Closed);
                return dataCmd.ExecuteScalar();
            }
            else return null;
        }

        [WebMethod]
        public int ExecuteNonQuery(string cmdText, CommandType cmdtype = CommandType.Text)
        {
            //EventState(ConnectionState.Executing);
            if (dataCon.State == ConnectionState.Open)
            {
                dataCmd.CommandTimeout = dataCon.ConnectionTimeout;
                dataCmd.CommandText = cmdText;
                dataCmd.CommandType = cmdtype;
                dataCmd.Transaction = dataTrn;
                //EventState(ConnectionState.Closed);
                return dataCmd.ExecuteNonQuery();
            }
            else return -1;
        }

        [WebMethod]
        public DataSet FillDataSet(string cmdText, CommandType cmdtype = CommandType.Text)
        {
            //EventState(ConnectionState.Executing);
            dataCmd.CommandTimeout = dataCon.ConnectionTimeout;
            dataCmd.CommandText = cmdText;
            dataCmd.CommandType = cmdtype;
            SqlDataAdapter da = new SqlDataAdapter(dataCmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da = null;
            //EventState(ConnectionState.Closed);
            return ds;
        }

        [WebMethod]
        public DataTable FillDataTable(string cmdText, CommandType cmdtype = CommandType.Text)
        {
            //EventState(ConnectionState.Executing);
            dataCmd.CommandTimeout = dataCon.ConnectionTimeout;
            dataCmd.CommandText = cmdText;
            dataCmd.CommandType = cmdtype;
            SqlDataAdapter da = new SqlDataAdapter(dataCmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            da = null;
            //EventState(ConnectionState.Closed);
            return dt;
        }

        public void Close()
        {
            //EventState(ConnectionState.Closed);
            dataCon.Close();
            dataTrn = null;
        }
        
        //This portion is for execute command
        [WebMethod]
        public void getExecute(string SQL)
        {
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                MyCmd.Transaction = MyCmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                MyCmd.ExecuteNonQuery();
                MyCmd.Transaction.Commit();
            }
            catch (Exception ex)
            {
                MyCmd.Transaction.Rollback();
                Console.WriteLine(ex.Message);
            }
            finally
            {
                MyCmd.Connection.Close();
                MyCon.Close(); MyCon.Dispose();
            }
        } // End Here

        //This portion is for How to cast the DataSet
        [WebMethod]
        public DataSet getDataSet(string SQL)
        {
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataAdapter MyAdapter = null;
            DataSet MyDataset = null;

            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                MyAdapter = new SqlDataAdapter(MyCmd);
                MyDataset = new DataSet();
                MyAdapter.Fill(MyDataset);
            }
            catch (Exception ex)
            {
                MyDataset.Clear(); MyDataset = null;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                MyCmd.Connection.Close(); MyCmd.Transaction.Dispose();
                MyAdapter.Dispose(); MyAdapter = null;
                MyCon.Close(); MyCon.Dispose();
            }
            return MyDataset;
        } // End Here

        // This portion is use to cast the read adapter
        [WebMethod]
        public SqlDataReader getRead(string SQL)
        {   
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataReader MyRead = null;

            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                MyRead = MyCmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                MyRead.Dispose(); MyRead = null;
                Console.WriteLine(ex.Message);
            }
            return MyRead;
        } // End Here

        //This function is used to display text  in Labels
        [WebMethod]
        public SqlDataReader getDisplay(string SQL, Label lbl)
        {
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataReader MyRead = null;

            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                lbl.Text = MyCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
            }
            return MyRead;
        } // End Here

        //This function is used to display text  in Textbox
        [WebMethod]
        public SqlDataReader getStudentInfo(string SQL, TextBox txtStudentName, TextBox txtCourse, TextBox txtSection)
        {
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataReader MyRead = null;

            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                MyRead = MyCmd.ExecuteReader();
                if(MyRead.Read())
                {
                    txtStudentName.Text = MyRead.GetValue(1).ToString();
                    if (txtStudentName.Text == null)
                    {
                        txtStudentName.Enabled = true;
                    }
                    else
                    {
                        txtStudentName.Enabled = false;
                    }

                    txtCourse.Text = MyRead.GetValue(2).ToString();
                    if (txtCourse.Text == null)
                    {
                        txtCourse.Enabled = true;
                    }
                    else
                    {
                        txtCourse.Enabled = false;
                    }

                    txtSection.Text = MyRead.GetValue(3).ToString();
                    if (txtSection.Text == null)
                    {
                        txtSection.Enabled = true;
                    }
                    else
                    {
                        txtSection.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return MyRead;
        } // End Here

      
        // This function is use to cast the getTable
        public DataTable getTable(string SQL)
        {
            SqlConnection Mycon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataAdapter MyAdapter;
            DataTable MyTable = null;
            if (Mycon.State == ConnectionState.Closed) { Mycon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, Mycon);
                MyAdapter = new SqlDataAdapter(MyCmd);
                MyTable = new DataTable();
                MyAdapter.Fill(MyTable);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MyTable.Clear(); MyTable = null;
            }
            finally
            {
                MyCmd.Connection.Close(); MyCmd.Dispose();
                MyAdapter = null;
                Mycon.Close(); Mycon.Dispose();
            }
            return MyTable;
        }
        //End Here

        // Populate the data into dropdownlist
        public void getDropdown(string SQL, string Value, string Display, DropDownList Cmb)
        {
            SqlConnection MyCon = new SqlConnection(ConString.ToString());
            SqlCommand MyCmd = null;
            SqlDataReader MyRead = null;

            if (MyCon.State == ConnectionState.Closed) { MyCon.Open(); }
            try
            {
                MyCmd = new SqlCommand(SQL, MyCon);
                MyRead = MyCmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                MyRead.Dispose(); MyRead = null;
                Console.WriteLine(ex.Message);
            }

            ListItem list;
            try
            {

                //Cmb.ClearSelection();
                Cmb.Items.Clear();
                while(MyRead.Read())
                {
                    list = new ListItem(MyRead[Display.ToString()].ToString().Trim(), MyRead[Value.ToString()].ToString().Trim());
                    Cmb.Items.Add(list);
                }

                //Cmb.DataSource = MyRead;
                //Cmb.DataValueField = Value.ToString();
                //Cmb.DataTextField = Display.ToString();
                Cmb.DataBind();
            }
            catch (Exception ex)
            {
                Cmb.ClearSelection();
                MyRead.Close(); MyRead.Dispose(); MyRead = null;
                Console.WriteLine(ex.Message);
            }
        } // End here

        // This portion is to set the grid
        [WebMethod]
        public void getGrid(string SQL, GridView Grd)
        {
            DataTable MyTable = null; MyTable = getTable(SQL.ToString());
            try
            {
                if (MyTable.Rows.Count == 0)
                {

                    //Page.ClientScript.RegisterStartupScript(GetType(), "Test Message", "alert('Date From is required');", true);
                    //Grd.DataSource = null;
                }
                else
                {
                    Grd.DataSource = null;
                    Grd.DataSource = MyTable;
                    Grd.DataBind();
                }
            }
            catch (Exception ex)
            {
                MyTable = null; Grd.DataSource = null;
                Console.WriteLine(ex.Message);
            }
            finally { MyTable = null; }
        } // End Here

    }
}
