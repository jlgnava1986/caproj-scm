﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data;
using System.Data.SqlClient;

namespace CAPROJ
{
    public partial class RawMaterials : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadRawMaterialsList();
            }


        }
        public void LoadRawMaterialsList()
        {
            _SQL.getDropdown("Select * from md_raw_materials", "Material_ID", "material_name", DropDownList1);
        }
        public void LoadRawMaterials()
        {
            
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_raw_materials where Material_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();

                RawMaterial_ID.Text = dr["Material_ID"].ToString();
                RawMaterialName.Text = dr["material_name"].ToString().Trim();
                DropDownList2.SelectedValue = dr["category"].ToString().Trim();
                UnitPrice.Text = dr["unit_price"].ToString().Trim();
                MaxQuantity.Text = dr["max_quantity"].ToString().Trim();
                LeadTime.Text = dr["lead_time"].ToString().Trim();
                //Disable the controls

                RawMaterialName.ReadOnly = true;
                DropDownList2.Enabled = false;
                UnitPrice.ReadOnly = true;
                MaxQuantity.ReadOnly = true;
                LeadTime.ReadOnly = true;
                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();




        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRawMaterials();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RawMaterialName.ReadOnly = false;
            DropDownList2.Enabled = true;
            UnitPrice.ReadOnly = false;
            MaxQuantity.ReadOnly = false;
            LeadTime.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                _SQL.Open();
                //Machine data
                _SQL.AddParameters("material_name", RawMaterialName.Text.Trim());
                _SQL.AddParameters("category", DropDownList2.SelectedValue);
                _SQL.AddParameters("unit_price", UnitPrice.Text.Trim());
                _SQL.AddParameters("max_quantity", MaxQuantity.Text.Trim());
                _SQL.AddParameters("lead_time", LeadTime.Text.Trim());
                string query = @"INSERT INTO md_raw_materials(material_name,category, unit_price, max_quantity, lead_time)
                                       VALUES(@material_name,@category, @unit_price, @max_quantity, @lead_time);";

                _SQL.ExecuteNonQuery(query);
                _SQL.Close();


                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Raw Material Successfully Added!'); window.location='" + Request.ApplicationPath + "RawMaterialsList.aspx';", true);
            }
            else
            {
                _SQL.Open();
                _SQL.AddParameters("Material_ID", DropDownList1.SelectedValue);
                _SQL.AddParameters("material_name", RawMaterialName.Text.Trim());
                _SQL.AddParameters("category", DropDownList2.SelectedValue);
                _SQL.AddParameters("unit_price", UnitPrice.Text.Trim());
                _SQL.AddParameters("max_quantity", MaxQuantity.Text.Trim());
                _SQL.AddParameters("lead_time", LeadTime.Text.Trim());
                _SQL.ExecuteNonQuery(@"UPDATE md_raw_materials SET material_name=@material_name,
                                           category=@category, unit_price=@unit_price, max_quantity=@max_quantity lead_time=@lead_time where Material_ID=@Material_ID");
                _SQL.Close();

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Raw Material Successfully Updated!'); window.location='" + Request.ApplicationPath + "RawMaterials.aspx';", true);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_raw_materials WHERE Material_ID='" + RawMaterial_ID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Raw Material Successfully Deleted!'); window.location='" + Request.ApplicationPath + "RawMaterials.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("RawMaterials.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            RawMaterialName.Text = "";
            UnitPrice.Text= "";
            MaxQuantity.Text = "";
            LeadTime.Text = "";

            //Enable the controls
            RawMaterialName.ReadOnly = false;
            DropDownList2.Enabled = true;
            UnitPrice.ReadOnly = false;
            MaxQuantity.ReadOnly = false;
            LeadTime.ReadOnly = false;
            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }
    }

}