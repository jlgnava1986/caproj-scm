﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data.SqlClient;


namespace CAPROJ
{
    public partial class Register : System.Web.UI.Page
    {

        SQLProcedures _SQL = new SQLProcedures();
        //for connections to the sql database
        PublicProperties PublicProperty = new PublicProperties();
        //to be able to display messages
        WebFunctions.WebFunctions func = new WebFunctions.WebFunctions();
        //to encrypt, such as passwords etc
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadGroupAccess();
            }
        }

        protected void Register1_Click(object sender, EventArgs e)
        {
            Label5.Text = "";
            Label6.Text = "";
            Label7.Text = "";
            Label8.Text = "";
            Label9.Text = "";
            if (FirstName1.Text =="")
            {
                Label5.Text = "First Name required to register";
            }
            if (LastName1.Text=="")
            {
                Label6.Text = "Last Name required to register";
            }
            if (DropDownList2.SelectedValue.ToString().Trim() == "Department")
            {
                Label7.Text = "Department required to register";
            }
            if (DropDownList1.SelectedValue.ToString().Trim() == "Access Level")
            {
                Label10.Text = "Access Level required to register";
            }
            if (UserName1.Text == "")
            {
                Label8.Text = "Username is required to register";
            }
            if (Password1.Text == "")
            {
                Label9.Text = "Password is required to register";
            }
            if (UserName1.Text != "" && Password1.Text != "" && DropDownList2.SelectedValue != "Department" && DropDownList1.SelectedValue != "Access Level")
            {
                _SQL.Open();
                _SQL.AddParameters("firstname", FirstName1.Text.Trim());
                _SQL.AddParameters("lastname", LastName1.Text.Trim());
                _SQL.AddParameters("department", DropDownList2.SelectedValue);
                _SQL.AddParameters("userID", UserName1.Text.Trim());
                _SQL.AddParameters("password", func.EncryptData(Password1.Text.Trim(), "", "", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256));
                _SQL.AddParameters("groupAccess", DropDownList1.SelectedValue.ToString().Trim());
                _SQL.ExecuteNonQuery("INSERT INTO md_administration(first_name,last_name,department,username,password,ch_group_access,bt_activated) VALUES(@firstname,@lastname,@department,@userID,@password,@groupAccess,'Pending')");
                _SQL.Close();
                PublicProperty.validMessage = "You are now registered! Please login with your own credentials.";
                Response.Redirect("Default.aspx");

            }
        }

        protected void LoadGroupAccess()
        {
            _SQL.getDropdown("select ch_group_access='', vc_group_name='Access Level' union select ch_group_access,vc_group_name from md_menu_group order by ch_group_access", "ch_group_access", "vc_group_name", DropDownList1);

        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            PublicProperty.validMessage = "";
            Response.Redirect("Default.aspx");
        }
    }
}