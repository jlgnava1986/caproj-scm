﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AI.aspx.cs" Async="true" Inherits="ETIIOnlineServices.AI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Inteliggent Assistant</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
            <asp:Panel ID="panelUploadFile" runat="server">
              <div class="col-xs-12 no-padding">
                <p><em>Go ahead, try it&hellip;</em></p>
                <p class="voice_instructions">Say "Hello!"</p>
                <div>
                    <audio id="audio1" style="visibility:hidden" controls>Canvas not supported</audio>
                </div>
                <div id="rate"></div>
                <div id="outputdetails" style="display:block;"></div>
                <div class="box-body table-responsive no-padding">
                    <asp:GridView ID="gvRecordList" runat="server" AutoGenerateColumns="false" BorderStyle="None" Class="table" GridLines="None" ShowHeaderWhenEmpty="false" style="display: none;">
                        <Columns>
                            <asp:BoundField ItemStyle-Width="150px" DataField="ch_unit_id" HeaderText="Unit ID" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="vc_status_desc" HeaderText="Status" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="ch_unittype_desc" HeaderText="Type" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="ch_unitmodel_desc" HeaderText="Model" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="nu_sqm" HeaderText="Sqm" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="mo_lp_with_cwtdisc" HeaderText="List Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}" />
                        </Columns>
                    <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                    </asp:GridView>
                </div>
                <br />
                <div class="Pager"></div>

                <script src="js/annyang.min.js" type="text/javascript"></script>
                 <script type="text/javascript">
                 $(document).ready(function () {
                     //Setting up the variables
                     var audioElm = document.getElementById("audio1"); // Audio element
                     var ratedisplay = document.getElementById("rate"); // Rate display area
                     var query = ""; //dynamic query based on voice command tags

                     //Audio Listener Event handler
                     audioElm.addEventListener("ratechange", function () {
                         ratedisplay.innerHTML = "Rate: " + audioElm.playbackRate;
                     }, false);

                     //1. Annyang Speech Recognition waits for the voice command
                     if (annyang) {
                         var commands = {
                             'floor *tag unit *tag1': function(tag, tag1) { GetOutput(tag, tag1); },
                             'unit *tag1 floor *tag': function(tag, tag1) { GetOutput(tag1, tag); },
                             '*anywords VRC floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('VRC', tag, tag1); },
                             '*anywords VRZ floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('VRC', tag, tag1); },
                             '*anywords BRC floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('VRC', tag, tag1); },
                             '*anywords VRD floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('VRD', tag, tag1); },
                             '*anywords BRD floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('VRD', tag, tag1); },
                             '*anywords MRF floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput2('MRF', tag, tag1); },
                             '*anywords floor *tag unit *tag1': function(anywords, tag, tag1) { GetOutput1(anywords, tag, tag1); },
                             'show all *tag units': function (tag) { DisplayRecordList(tag,'', '0'); },
                             'show all *tag units on *proj': function (tag, proj) { DisplayRecordList(tag, proj, '0'); },
                             'show me all *tag units': function (tag) { DisplayRecordList(tag, '', ''); },
                             'show me all *tag units on *proj': function (tag, proj) { DisplayRecordList(tag, proj, '0'); },
                             '*anywords units less than *tag million': function (anywords, tag) { DisplayRecordList('<', '', tag); },
                             '*anywords units greater than *tag million': function (anywords, tag) { DisplayRecordList('>', '', tag); },
                             '*anywords units less than *tag million on *proj': function (anywords, tag, proj) { DisplayRecordList('<', proj, tag); },
                             '*anywords units greater than *tag million on *proj': function (anywords, tag, proj) { DisplayRecordList('>', proj, tag); },
                         }

                         annyang.addCommands(commands);
                         annyang.start();
                         annyang.debug(true);
                     }

                     //2. Get the output based on the voice command
                     //Floor & Unit Tags
                     function GetOutput(tag, tag1) {
                         //Create the dynamic query
                         var qry = "select *,mo_lp_with_cwtdisc1=CONVERT(varchar, CAST(mo_lp_with_cwtdisc AS money), 1) From mf_unit_status_details where ch_floor_id='" + tag.lpad("0", 2) + "' and nu_unit_number='" + tag1.lpad("0", 3) + "' and ch_proj_code='VRC'";
                         //Check database record
                         GetRecords(qry);
                         //Get the audio response
                         GetAudioResponse(qry);
                     }
                     //*Anywords + Floor & Unit Tags
                     function GetOutput1(anywords, tag, tag1) {
                         //Create the dynamic query
                         var qry = "select *,mo_lp_with_cwtdisc1=CONVERT(varchar, CAST(mo_lp_with_cwtdisc AS money), 1) From mf_unit_status_details where ch_floor_id='" + tag.lpad("0", 2) + "' and nu_unit_number='" + tag1.lpad("0", 3) + "' and ch_proj_code='VRC'";
                         //Check database record
                         GetRecords(qry);
                         //Get the audio response
                         GetAudioResponse(qry);
                     }
                     //Project, Floor & Unit Tags
                     function GetOutput2(proj, tag, tag1) {
                         //Create the dynamic query
                         var qry = "select *,mo_lp_with_cwtdisc1=CONVERT(varchar, CAST(mo_lp_with_cwtdisc AS money), 1) From mf_unit_status_details where ch_floor_id='" + tag.lpad("0", 2) + "' and nu_unit_number='" + tag1.lpad("0", 3) + "' and ch_proj_code='" + proj + "'";
                         //Check database record
                         GetRecords(qry);
                         //Get the audio response
                         GetAudioResponse(qry);
                     }
                     //Display Record List
                     function DisplayRecordList(tag,proj,amount) {
                         //Check database record
                         GetRecordList(1,tag,proj,amount);
                     }


                     function playAudio(audioElm, audioResponse) {
                         // Get file from text box and assign it to the source of the audio element 
                         audioElm.src = audioResponse;
                         audioElm.play();
                     }

                     function pauseAudio(audioElm) {
                         audioElm.pause();
                     }

                     //Formatting
                     String.prototype.lpad = function (padString, length) {
                         var str = this;
                         while (str.length < length)
                             str = padString + str;
                         return str;
                     }

                     //Check the records on the database
                     function GetRecords(query) {
                         var jsonData = JSON.stringify({
                             sqlquery: query
                         });
                         if (query != "") {
                             $.ajax({
                                 type: "POST",
                                 url: "/AI.aspx/GetRecords",
                                 data: jsonData,
                                 contentType: "application/json; charset=utf-8",
                                 dataType: "json",
                                 success: function (response) {
                                     document.getElementById("outputdetails").innerHTML = response.d;
                                     document.getElementById('<%= gvRecordList.ClientID %>').style.display = "none";
                                     document.getElementById("outputdetails").style.display = "block";
                                 },
                                 failure: function (response) {
                                     alert(response.d);
                                 },
                                 error: function (response) {
                                     alert(response.d);
                                 }
                             });
                         } else {
                             alert('Woops! Something is wrong...');
                         }
                     }

                     //Get the audio response based on the voice command
                     function GetAudioResponse(query) {
                         var jsonData = JSON.stringify({
                             sqlquery: query
                         });
                         if (query != "") {
                             $.ajax({
                                 type: "POST",
                                 url: "/AI.aspx/GetAudioResponse",
                                 data: jsonData,
                                 contentType: "application/json; charset=utf-8",
                                 dataType: "json",
                                 success: function (response) {
                                     if (document.getElementById("audio1")) {
                                         if (audioElm.paused == true) {
                                             playAudio(audioElm, response.d);    //  if player is paused, then play the file
                                         } else {
                                             pauseAudio(audioElm);   //  if player is playing, then pause
                                         }
                                     }
                                 },
                                 failure: function (response) {
                                     alert(response.d);
                                 },
                                 error: function (response) {
                                     alert(response.d);
                                 }
                             });
                         } else {
                             alert('Woops! Something is wrong...');
                         }
                     }

                     function GetRecordList(pageIndex,query,proj,amount) {
                         var recordListFilter = JSON.stringify({
                             pageIndex: pageIndex,
                             sqlquery: query,
                             proj: proj,
                             amount: amount
                         });

                         $.ajax({
                             type: "POST",
                             url: "/AI.aspx/GetRecordList",
                             data: recordListFilter,
                             contentType: "application/json; charset=utf-8",
                             dataType: "json",
                             success: OnSuccess,
                             failure: function (response) {
                                 console.log(response)
                                 alert(response.d);
                             },
                             error: function (response) {
                                 alert(response.d);
                                 console.log(response)
                             }
                         });
                     }

                     function OnSuccess(response) {
                         var xmlDoc = $.parseXML(response.d);                         
                         var xml = $(xmlDoc);
                         var customers = xml.find("RecordList");
                         var row = $("[id*=gvRecordList] tr:last-child").clone(true);
                         if(customers.length > 0){
                             document.getElementById('<%= gvRecordList.ClientID %>').style.display = "block";
                             document.getElementById("outputdetails").style.display = "none";
                             $("[id*=gvRecordList] tr").not($("[id*=gvRecordList] tr:first-child")).remove();
                             $.each(customers, function () {
                                 var customer = $(this);
                                 $("td", row).eq(0).html($(this).find("ch_unit_id").text());
                                 $("td", row).eq(1).html($(this).find("vc_status_desc").text());
                                 $("td", row).eq(2).html($(this).find("ch_unittype_desc").text());
                                 $("td", row).eq(3).html($(this).find("ch_unitmodel_desc").text());
                                 $("td", row).eq(4).html($(this).find("nu_sqm").text());
                                 $("td", row).eq(5).html($(this).find("mo_lp_with_cwtdisc").text());
                                 $("[id*=gvRecordList]").append(row);
                                 row = $("[id*=gvRecordList] tr:last-child").clone(true);
                             });                         
                         }
                         else{
                            document.getElementById('<%= gvRecordList.ClientID %>').style.display = "none";
                         };
                     };
                 });
                 </script>
              </div>
            </asp:Panel>
        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
    <%--<script src="js/speechkitt.min.js" type="text/javascript"></script>--%>
</asp:Content>

