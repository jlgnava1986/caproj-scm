﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="ETIIOnlineServices.DocsApproval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Documents Approval
                <small>List Of All Request</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../../Portal.aspx"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Documents Approval</li>
            </ol>
        </section>

        <section class="content">
            <form runat="server" id="Form1">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Request List</h3>
<%--                                <div class="pull-right" style="padding:10px 5px 5px 0px">
                                        <asp:DropDownList ID="dropFilter" runat="server" CssClass="form-control" 
                                            AutoPostBack="true" OnSelectedIndexChanged="dropFilter_OnSelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="0">Documents for Authorization</asp:ListItem>
                                            <asp:ListItem Value="1">All Documents</asp:ListItem>
                                            <asp:ListItem Value="2">Approved Documents</asp:ListItem>
                                            <asp:ListItem Value="3">Disapproved Documents</asp:ListItem>
                                        </asp:DropDownList>
                                </div>
--%>                            </div><!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                    <asp:ScriptManager runat="Server" ID="ScriptManager1"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid" runat="server" AutoGenerateColumns="False" 
                                            GridLines="None"
                                            BorderStyle="None" Class="table" AllowSorting="True" OnRowCommand="ViewDocumentStatus" OnPageIndexChanging="Grid_PageIndexChanging" AllowPaging="true"
                                            ShowHeaderWhenEmpty="True" EmptyDataText="No documents to be authorized." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <%--<asp:BoundField DataField="Type" HeaderText="Doc ID">
                                                    <HeaderStyle HorizontalAlign="Left" Width="70px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>--%>
                                                    <asp:BoundField DataField="ch_doc_id" HeaderText="Request ID">
                                                    <HeaderStyle HorizontalAlign="Left" Width="110px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="display:none;"/>
                                                </asp:BoundField>

                                                    <asp:BoundField DataField="ch_reference_no" HeaderText="Request No">
                                                    <HeaderStyle HorizontalAlign="Left" Width="110px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="display:none;"/>
                                                </asp:BoundField>
                                                    
                                                <asp:BoundField DataField="vc_request_type" HeaderText="Type">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="vc_prepared_by" HeaderText="Prepared By">
                                                    <HeaderStyle HorizontalAlign="Left" Width="180px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="vc_docprocess_desc" HeaderText="Description">
                                                    <HeaderStyle HorizontalAlign="Left" width="150px"/>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="vc_remarks" HeaderText="Remarks">
                                                    <HeaderStyle HorizontalAlign="Left" width="380px"/>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                    <asp:Button ID="btnView" CommandName="ViewDocStatus" runat="server" CssClass="btn btn-sm btn-warning"
                                                    Text='View Status' CommandArgument='<%#((GridViewRow)Container).RowIndex%>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Approve" CommandArgument='<%#((GridViewRow)Container).RowIndex%>'
                                                            Text='Approve'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btn btn-sm btn-danger" CommandName="Disapprove"  CommandArgument='<%#((GridViewRow)Container).RowIndex%>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCC99" />
                                            <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                            <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                            <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Grid" EventName="RowCommand" />
                                    </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                                        <ProgressTemplate>
                                            <div class="divWaiting">            
                                            <center>
                                                <div style="width:40%; height:40%;">
                                                        Please wait...
                                                    <div class="progress progress-striped active" width="40%" height="40%">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                        </div>
                                                    </div>
                                                </div>
                                            </center>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>     

                                    <%-- APPROVAL STATUS --%>
                                    <div class="modal" id="modalApprove">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Remarks</h4>
                                            </div>
                                            <div class="modal-body">
                                                <asp:Textbox id="txtRemarks" text="" runat="server" class="form-control" TextMode="MultiLine" rows="3" placeholder="Remarks is optional. . ." />
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                <asp:button id="btnApprove1" runat="server" type="button" 
                                                    class="btn btn-primary" Text="Approve" onclick="btnApprove1_Click"/>
                                            </div>
                                        </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                    <%-- DISAPPROVAL STATUS --%>
                                    <div class="modal" id="modalDisapprove">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Remarks</h4>
                                            </div>
                                            <div class="modal-body">
                                                <asp:Textbox id="txtRemarksDisapprove" text="" runat="server" class="form-control" TextMode="MultiLine" rows="3" placeholder="Remarks is optional. . ." />
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                <asp:button id="Button1" runat="server" type="button" 
                                                    class="btn btn-danger" Text="Disapprove" onclick="btnDisapprove1_Click"/>
                                            </div>
                                        </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                    <%-- DOCUMENT PROCESS STATUS --%>
                                    <div class="modal" id="docHistory">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Document Status</h4>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="GridStatus" runat="server"
                                                        CssClass="table table-bordered table-hover"
                                                        BackColor="White" ForeColor="Black"
                                                        FieldHeaderStyle-Wrap="false"
                                                        FieldHeaderStyle-Font-Bold="true" 
                                                        FieldHeaderStyle-BackColor="LavenderBlush"
                                                        FieldHeaderStyle-ForeColor="Black"
                                                        BorderStyle="Groove" AutoGenerateRows="False" AutoGenerateColumns="false"
                                                        ShowHeaderWhenEmpty="True" EmptyDataText="No Records Found.">
                                                        <Columns>
                                                            <asp:BoundField DataField="vc_docprocess_desc" HeaderText="Status">
                                                                <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="vc_user_name" HeaderText="User">
                                                                <HeaderStyle HorizontalAlign="Left" Width="170px" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="sd_processed_date" HeaderText="Date" dataformatstring="{0:g}">
                                                                <HeaderStyle HorizontalAlign="Left" width="100px"/>
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="Grid"  EventName="RowCommand" />  
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            </div>
<%--                                                <div class="modal-body">
                                            </div>--%>
<%--                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
--%>                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                            </div><!-- /.box-body -->
                        </div>
                    </div>
               </div>
            </form>
            </section>
        </aside>    
    <p>&nbsp;</p>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</asp:Content>
