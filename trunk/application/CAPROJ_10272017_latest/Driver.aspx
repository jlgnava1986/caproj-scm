﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Driver.aspx.cs" Inherits="CAPROJ.Driver" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Dashboard_Portal" runat="server">
   <aside class="right-side">
      <section class="content-header">
         <h1>
            Driver Master Data
         </h1>
      </section>
      <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
               <div class="row">
                  <div class="col-md-12">
                     <%-- 1 column is equal to 12 so if its 6 its only half of the page --%>
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#Gen" data-toggle="tab">General</a></li>
                           <li><a href="#Delivery" data-toggle="tab">Delivery</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="Gen">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="row">
                                       <div class="col-md-2">
                                          <div class="form-group">
                                             Driver ID: 
                                             <asp:Label ID="DriverID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                        <div class="col-md-6">
                                           <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>
                                       </div>
                                         <div class="col-md-4">
                                           
                                       </div>
                                       
                                    </div>
                                     </br>
                                     <br />
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <small>
                                                     Driver Name
                                                 </small>
                                                <asp:TextBox ID="DriverName" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>Address</small>
                                                 <asp:TextBox ID="Address" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>City</small>
                                                 <asp:TextBox ID="City" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Province</small>
                                                 <asp:TextBox ID="Province" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                         </div>
                                             <div class="col-md-6">
                                             <small>
                                                 Zip Code
                                             </small>
                                                <asp:TextBox ID="ZipCode" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                 <br />
                                                <small> Email</small>
                                                 
                                                 <asp:TextBox ID="Email" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                                 <br />
                                                 <small>Mobile Phone</small>
                                                 <asp:TextBox ID="Mobile" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                             </div>
                                     </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="Delivery">
                              <div class="row">
                                 <div class="col-md-12">
                                     <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             Driver ID: 
                                             <asp:Label ID="DriverrID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                       
                                     
                                    </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                
                                                 <small>Brand of Car</small>
                                                <asp:TextBox ID="CarBrand" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                            </div>
                                             <div class="form-group">
                                                
                                                 <small>Color of Car</small>
                                                <asp:TextBox ID="CarColor" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                            </div>
                                             <div class="form-group">
                                                
                                                 <small>Plate No.</small>
                                                <asp:TextBox ID="PlateNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                            </div>
                                           

                                         </div>
                                             <div class="col-md-6">
                                         
                                             </div>
                                    </div>
                                 </div>
                              </div>
                        </div>
                         
                     </div>
                      <div class="row">
                             <div class="col-md-2">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-block btn-danger" Text="Cancel" BackColor="#b01717" OnClick="btnCancel_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnAdd" runat="server" class="btn btn-block btn-danger" Text="Add" BackColor="#b01717" OnClick="btnAdd_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnEdit" runat="server" class="btn btn-block btn-danger" Text="Edit" BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnDelete" runat="server" class="btn btn-block btn-danger" Text="Delete" BackColor="#b01717" OnClick="btnDelete_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnSave" runat="server" class="btn btn-block btn-danger" Text="Save" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                             </div>
                         </div>
                  </div>
               </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
         </asp:UpdatePanel>
      </section>
   </aside>
</asp:Content>