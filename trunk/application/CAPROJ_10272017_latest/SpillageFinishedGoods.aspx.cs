﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class SpillageFG : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadScheduleForViewing();
                
            }
        }

        
        //Load Schedule Details for Viewing and Editing
        public void LoadScheduleForViewing()
        {
            //Load Supplier Products
            string SQL = "select a.*,b.finished_goods_name from dbo.md_spillage_finished_goods a inner join md_finished_goods b on a.FinishedGoods_ID = b.FinishedGoods_ID";
            GridSpillageFG.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSpillageFG.DataSource = dt;
                GridSpillageFG.DataBind();
                GridSpillageFG.Visible = true;
               
                ViewState["ViewingTable"] = dt;
              
            }
        }

    }
}