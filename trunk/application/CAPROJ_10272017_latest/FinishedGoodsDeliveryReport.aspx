﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="FinishedGoodsDeliveryReport.aspx.cs" Inherits="CAPROJ.FinishedGoodsDeliveryReport" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Delivery Report - Finished Goods
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <%--FILTER--%>
                                   <div class="row" style="padding-right:5px;padding-bottom:10px;">
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="dropColumns" runat="server" CssClass="form-control" OnSelectedIndexChanged="dropColumns_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="cnt">#</asp:ListItem>
                                            <asp:ListItem Value="customer_name">Customer Name</asp:ListItem>
                                            <asp:ListItem Value="product_no">Finished Goods No.</asp:ListItem>
                                            <asp:ListItem Value="finished_goods_name">Finished Goods Name</asp:ListItem>
                                            <asp:ListItem Value="date_received">Date Received</asp:ListItem>
                                            <asp:ListItem Value="total">Total</asp:ListItem>
                                          </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtFilter" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" TextMode="Date" Enabled="false" placeholder="From"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="ToDate" runat="server" CssClass="form-control" TextMode="Date" Enabled="false" placeholder="To"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnFilter" Text='Filter' BackColor="#b01717" OnClick="btnFilter_Click"></asp:Button>
                                    </div>
                                </div>

                                <%--GRIDVIEW--%>
                                <div class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridScheduleViewing" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="false" ShowHeaderWhenEmpty="true" ShowHeader="true"
                                                BorderStyle="None" Class="table" AllowSorting="True" OnSorting="GridScheduleViewing_Sorting">
                                                <Columns>
                                                    <asp:BoundField DataField="cnt" HeaderText="#" SortExpression="cnt">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="customer_name" HeaderText="Customer Name" SortExpression="customer_name">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="product_no" HeaderText="Finished Goods No." SortExpression="product_no">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="finished_goods_name" HeaderText="Finished Goods Name" SortExpression="finished_goods_name">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="date_received" HeaderText="Date Received" DataFormatString="{0:MM/dd/yyyy}" SortExpression="date_received">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="total" HeaderText="Total Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}" SortExpression="total">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="white" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>
                              <%--END GRIDVIEW--%>

                            </div>
                        </div>
                        <%--PRINT BUTTON--%>
                         <div class="row">
                                    <div class="col-md-10">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnPrint" Text='Print' BackColor="#b01717" OnClientClick="Print();"></asp:Button>
                                    </div>
                                </div>

                        <%--END PRINT BUTTON--%>

                        <%--CRYSTAL REPORT--%>
                                 <div id="dvReport" class="col-md-12" style="display:none;">
                                    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                                        ToolPanelView="None" PrintMode="Pdf"></CR:CrystalReportViewer>
                                </div>
                        <%--END CRYSTAL REPORT--%>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="dropColumns" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
    <%--SCRIPTS--%>
    <script type="text/javascript">  
        function Print() {  
            var dvReport = document.getElementById("dvReport");  
            var frame1 = dvReport.getElementsByTagName("iframe")[0];  
            if (navigator.appName.indexOf("Internet Explorer") != -1 || navigator.appVersion.indexOf("Trident") != -1) {  
                frame1.name = frame1.id;  
                window.frames[frame1.id].focus();  
                window.frames[frame1.id].print();  
            } else {  
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;  
                frameDoc.print();  
            }  
        }  
    </script>    
</asp:Content>