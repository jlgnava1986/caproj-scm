﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AgentPortal.aspx.cs" Inherits="ETIIOnlineServices.AgentPortal" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Dashboard
<%--                <small>Control panel</small>
--%>            </h1>
        </section>

        <section class="content">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" role="tablist">
                        <!-- Tab 1 Header -->
                        <li class="active"><a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">Commission</a></li>
                        <!-- Tab 2 Header -->
                        <li><a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">Available Units</a></li>
                        <!-- Tab 3 Header -->
                        <li><a href="#tab_3" aria-controls="tab_3" role="tab" data-toggle="tab">Availability Map</a></li>
                        <%--<!-- Project Drop Down-->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <asp:Label ID="lblSelectedProj" runat="server" Text="Label">Vivaldi Residences Cubao</asp:Label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a id="A1" role="menuitem" name="VRC" tabindex="-1" runat="server" OnServerClick="OnProjectClick" style="cursor:pointer">
                                    Vivaldi Residences Cubao
                                </a></li>
                                <li role="presentation"><a id="A2" role="menuitem" name="VRD" tabindex="-1"  runat="server" OnServerClick="OnProjectClick" style="cursor:pointer">
                                    Vivaldi Residences Davao
                                </a></li>
                                <li role="presentation"><a id="A3" role="menuitem" name="MRF" tabindex="-1" runat="server" OnServerClick="OnProjectClick" style="cursor:pointer">
                                    Milan Residences Fairview
                                </a></li>
                                    <%--<PagerSettings Mode="NextPrevious" PreviousPageText="Previous" NextPageText="Next   "/>
                            </ul>
                        </li>--%>
                        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- Tab 1 Details Commission -->
                        <div class="tab-pane active" id="tab_1">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="box-header">
                                    <h3 class="box-title">
                                        <asp:Label ID="lblHeader" runat="server" Text="Label">Commission</asp:Label>
                                        <div class="pull-right">
                                            <asp:DropDownList ID="dropFilter" runat="server" CssClass="form-control" OnSelectedIndexChanged="dropFilter_OnSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Selected="True" Value="0">In Process</asp:ListItem>
                                                <asp:ListItem Value="1">Released</asp:ListItem>
                                                <asp:ListItem Value="2">For Releasing</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Button ID="btnBackCommission" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                           Text='Back' OnCommand="ShowCommissionSummary"/>
                                        <h3>
                                        </h3>
                                        <h3>
                                        </h3>
                                    </h3>
                                </div>
                                <!-- Commission Summary -->
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridCommissionSummary" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"
                                        OnPageIndexChanging="GridCommissionSummary_PageIndexChanging"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="ch_batch_no" HeaderText="Batch No." Visible="true">
                                                <HeaderStyle HorizontalAlign="Left" Width="180px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="ch_cdf_no" HeaderText="CDF No.">
                                                <HeaderStyle HorizontalAlign="Left" Width="180px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="Remarks" HeaderText="Description">
                                                <HeaderStyle HorizontalAlign="Left" Width="380px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="netcomm" HeaderText="Total Amount" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewDetailsComm" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                        Text='View Details' OnCommand="LoadCommissionDetails"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                                <!-- Commission Details -->
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridCommissionDetails" runat="server" AutoGenerateColumns="False" 
                                            GridLines="None"  AllowPaging="true"
                                            OnPageIndexChanging="GridCommissionDetails_PageIndexChanging"              
                                            BorderStyle="None" Class="table" AllowSorting="True" PagerStyle-BackColor="#F7F7DE">
                                            <Columns>
                                                <asp:BoundField DataField="ch_floor_id" HeaderText="Floor">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="nu_unit_number" HeaderText="Unit No.">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="dpinstallmentno" HeaderText="Installment No.">
                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="vc_org_name" HeaderText="Client Name">
                                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="netcomm" HeaderText="Amount" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCC99" />
                                            <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                            <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                            <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                        </asp:GridView>
                                </div>
                                <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                                    <ProgressTemplate>
                                      <div class="divWaiting">            
                                        <center>
                                            <div style="width:40%; height:40%;">
                                                    Please wait...
                                                <div class="progress progress-striped active" width="40%" height="40%">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                    </div>
                                                </div>
                                            </div>
                                        </center>
                                      </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                           
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <!-- Tab 2 Details Unit Inventory -->
                        <div class="tab-pane" id="tab_2">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="box-header">
                                    <h3 class="box-title">
                                        Unit Pricelist Inventory
                                        <div class="pull-right">
                                            <asp:DropDownList ID="DropDownProject" runat="server" CssClass="form-control" OnSelectedIndexChanged="OnProjectClick" AutoPostBack="true">
                                                <asp:ListItem Selected="True" Value="VRC">Vivaldi Residences Cubao</asp:ListItem>
                                                <asp:ListItem Value="VRD">Vivaldi Residences Davao</asp:ListItem>
                                                <asp:ListItem Value="MRF">Milan Residences Fairview</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
<%--                                        <asp:Button ID="btnExportPDF" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                           Text='Export To PDF'/>--%>
                                        <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                           Text='Back' OnCommand="ShowInventorySummary"/>
                                    </h3>
                                </div>
                                <!-- Open Units Summary Per Type-->
                                    <asp:GridView ID="Grid" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="false"
                                        OnPageIndexChanging="Grid_PageIndexChanging"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                                <asp:BoundField DataField="ch_unittype_desc" HeaderText="Unit Type">
                                                    <HeaderStyle HorizontalAlign="Left" Width="470px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="int_open" HeaderText="No. Of Units">
                                                    <HeaderStyle HorizontalAlign="Left" Width="430px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnViewDetails" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                            Text='View Details' OnCommand="LoadUnitInventoryDetailsPerType"/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnExportPDF" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-danger" CommandName="Action"
                                                            Text='Export to PDF'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                <!-- Open Units Details Per Type-->
                                <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridPricelistDetails" runat="server" AutoGenerateColumns="False" 
                                            GridLines="None" Class="table table-responsive" AllowPaging="true"
                                            OnPageIndexChanging="Grid_PageIndexChanging"              
                                            BorderStyle="None" AllowSorting="True"  PageSize="50">
                                                                                            <Columns>
                                                    <asp:BoundField DataField="ch_floor_id" HeaderText="Floor">
                                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="nu_unit_number" HeaderText="Unit No.">
                                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="nu_sqm" HeaderText="Area (sqm)">
                                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="ch_unitmodel_desc" HeaderText="Unit Model">
                                                        <HeaderStyle HorizontalAlign="Left" Width="250px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>


                                                    <asp:BoundField DataField="ch_orientation_desc" HeaderText="Orientation">
                                                        <HeaderStyle HorizontalAlign="Left" Width="250px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="mo_list_price" HeaderText="List Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnCompute" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                            Text='Sample Comp.'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                </Columns>
                                            <RowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCC99" />
                                            <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                            <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                            <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                        </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <Triggers>
<%--                                <asp:AsyncPostBackTrigger ControlID="ctl00$Dashboard_Portal$Grid$ctl02$btnViewDetails" EventName="Click" />--%>
                            </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        <!-- Tab 3 - Availablity Map -->
                        <div class="tab-pane" id="tab_3">
<%--                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowPrintButton="False" ShowFindControls="False" ShowExportControls="False"></rsweb:ReportViewer> --%>
                                <iframe src="http://10.100.100.10/ReportServer/Pages/ReportViewer.aspx?/SalesAdmin/Availability%20Map%20-%20External%20-%20Touch&rs:Command=Render&rc:Parameters=true&rc:Zoom=75" frameborder="0" style="width: 100%;height: 500px;"></iframe>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
        </section>
    </aside>
<%--    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="Css/waitMe/waitMe.js"></script>
    <script type="text/javascript">
        function PleaseWait(effect, sender) {
            $('#' + sender).waitMe({
                effect: 'ios',
                //place text under the effect (string).
                text: '',
                //background for container (string).
                bg: 'rgba(255,255,255,0.7)',
                //color for background animation and text (string).
                color: '#000',
                //change width for elem animation (string).
                sizeW: '',
                //change height for elem animation (string).
                sizeH: '',
                // url to image
                source: ''
            });
        };

        function HideWaitScreen(sender) {
            $('#' + sender).waitMe('hide');
        };
    </script>
--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
    <script type="text/javascript">
        $(function () {
            "use strict";

            // AREA CHART
            var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666, item2: 2666 },
                        { y: '2011 Q2', item1: 2778, item2: 2294 },
                        { y: '2011 Q3', item1: 4912, item2: 1969 },
                        { y: '2011 Q4', item1: 3767, item2: 3597 },
                        { y: '2012 Q1', item1: 6810, item2: 1914 },
                        { y: '2012 Q2', item1: 5670, item2: 4293 },
                        { y: '2012 Q3', item1: 4820, item2: 3795 },
                        { y: '2012 Q4', item1: 15073, item2: 5967 },
                        { y: '2013 Q1', item1: 10687, item2: 4460 },
                        { y: '2013 Q2', item1: 8432, item2: 5713 }
                    ],
                xkey: 'y',
                ykeys: ['item1', 'item2'],
                labels: ['Item 1', 'Item 2'],
                lineColors: ['#a0d0e0', '#3c8dbc'],
                hideHover: 'auto'
            });

            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666 },
                        { y: '2011 Q2', item1: 2778 },
                        { y: '2011 Q3', item1: 4912 },
                        { y: '2011 Q4', item1: 3767 },
                        { y: '2012 Q1', item1: 6810 },
                        { y: '2012 Q2', item1: 5670 },
                        { y: '2012 Q3', item1: 4820 },
                        { y: '2012 Q4', item1: 15073 },
                        { y: '2013 Q1', item1: 10687 },
                        { y: '2013 Q2', item1: 8432 }
                    ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Item 1'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });

            //DONUT CHART
            var donut = new Morris.Donut({
                element: 'sales-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#0079AA"],
                data: [
                        { label: "Download Sales", value: 12 },
                        { label: "In-Store Sales", value: 30 },
                        { label: "Mail-Order Sales", value: 20 }
                    ],
                hideHover: 'auto'
            });

            //BAR CHART
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                        { y: '2006', a: 100, b: 90 },
                        { y: '2007', a: 75, b: 65 },
                        { y: '2008', a: 50, b: 40 },
                        { y: '2009', a: 75, b: 65 },
                        { y: '2010', a: 50, b: 40 },
                        { y: '2011', a: 75, b: 65 },
                        { y: '2012', a: 100, b: 90 }
                    ],
                barColors: ['#0079AA', '#f56954'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['CPU', 'DISK'],
                hideHover: 'auto'
            });
        });
        </script>
</asp:Content>
