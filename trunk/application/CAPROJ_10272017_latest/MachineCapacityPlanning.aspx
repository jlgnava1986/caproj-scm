﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="MachineCapacityPlanning.aspx.cs" Inherits="CAPROJ.MachineCapacityPlanning" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Machine
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Gridview for adding new record -->
                                        <div class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridSchedule" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true" ShowFooter="true"
                                                BorderStyle="None" Class="table" AllowSorting="True">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Machine ID">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="dropMachine" runat="server" CssClass="form-control" 
                                                                AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="dropMachine_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Machine Name">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="machine_name" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Design Capacity">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="design_capacity" runat="server" class="form-control" TextMode="number"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Effective Capacity">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="effective_capacity" runat="server" class="form-control" TextMode="number"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Machine Status">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="machine_status" runat="server" class="form-control"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <FooterTemplate>
                                                            <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" CssClass="btn btn-sm btn-info" OnClick="ButtonAdd_Click"/>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="white" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>
                                        <!-- Gridview for viewing records -->
                                        <div class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridScheduleViewing" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true"
                                                BorderStyle="None" Class="table" AllowSorting="True">
                                                <Columns>
                                                    <asp:BoundField DataField="Machine_ID" HeaderText="Machine ID">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="machine_name" HeaderText="Machine Name">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="design_capacity" HeaderText="Design Capacity">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="effective_capacity" HeaderText="Effective Capacity">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="machine_status" HeaderText="Machine Status">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="white" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnCancel" Text='Cancel' BackColor="#b01717" OnClick="btnCancel_Click"></asp:Button>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnEdit" Text='Edit' BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnSave" Text='Save' BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div> <!-- /.row -->
                    </div>        
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostbackTrigger ControlID="btnEdit" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
</asp:Content>
