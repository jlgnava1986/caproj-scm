﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AvailabilityMap.aspx.cs" Inherits="ETIIOnlineServices.AvailabilityMap" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Availability Map</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <div class="col-md-12">
                <%--<div class="box">--%>
<%--                        <controls>
                            <add tagPrefix="RSWEB" namespace="Microsoft.Reporting.WebForms" assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"/>
                        </controls>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowBackButton="False" 
                            ShowExportControls="False" ShowFindControls="False" 
                            ShowPageNavigationControls="False">
                            <ServerReport DisplayName="Availability Map" 
                                ReportPath="Sales Admin/Availability Map - External - Touch" 
                                ReportServerUrl="http://122.54.25.42/reportserver/" />
                        </rsweb:ReportViewer>
--%>
                    <iframe src="http://52.187.78.66:8080/ReportServer/Pages/ReportViewer.aspx?/Portal - Availability%20Map%20-%20External&rs:Command=Render&rc:Parameters=true&rc:Zoom=75" frameborder="0" style="width: 100%;height: 1000px;"></iframe>
                <%--</div>--%>
            </div>        
        </section>
    </aside>

    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(function () {
            "use strict";

            // AREA CHART
            var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666, item2: 2666 },
                        { y: '2011 Q2', item1: 2778, item2: 2294 },
                        { y: '2011 Q3', item1: 4912, item2: 1969 },
                        { y: '2011 Q4', item1: 3767, item2: 3597 },
                        { y: '2012 Q1', item1: 6810, item2: 1914 },
                        { y: '2012 Q2', item1: 5670, item2: 4293 },
                        { y: '2012 Q3', item1: 4820, item2: 3795 },
                        { y: '2012 Q4', item1: 15073, item2: 5967 },
                        { y: '2013 Q1', item1: 10687, item2: 4460 },
                        { y: '2013 Q2', item1: 8432, item2: 5713 }
                    ],
                xkey: 'y',
                ykeys: ['item1', 'item2'],
                labels: ['Item 1', 'Item 2'],
                lineColors: ['#a0d0e0', '#3c8dbc'],
                hideHover: 'auto'
            });

            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666 },
                        { y: '2011 Q2', item1: 2778 },
                        { y: '2011 Q3', item1: 4912 },
                        { y: '2011 Q4', item1: 3767 },
                        { y: '2012 Q1', item1: 6810 },
                        { y: '2012 Q2', item1: 5670 },
                        { y: '2012 Q3', item1: 4820 },
                        { y: '2012 Q4', item1: 15073 },
                        { y: '2013 Q1', item1: 10687 },
                        { y: '2013 Q2', item1: 8432 }
                    ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Item 1'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });

            //DONUT CHART
            var donut = new Morris.Donut({
                element: 'sales-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#0079AA"],
                data: [
                        { label: "Download Sales", value: 12 },
                        { label: "In-Store Sales", value: 30 },
                        { label: "Mail-Order Sales", value: 20 }
                    ],
                hideHover: 'auto'
            });

            //BAR CHART
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                        { y: '2006', a: 100, b: 90 },
                        { y: '2007', a: 75, b: 65 },
                        { y: '2008', a: 50, b: 40 },
                        { y: '2009', a: 75, b: 65 },
                        { y: '2010', a: 50, b: 40 },
                        { y: '2011', a: 75, b: 65 },
                        { y: '2012', a: 100, b: 90 }
                    ],
                barColors: ['#0079AA', '#f56954'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['CPU', 'DISK'],
                hideHover: 'auto'
            });
        });
        </script>
</asp:Content>
