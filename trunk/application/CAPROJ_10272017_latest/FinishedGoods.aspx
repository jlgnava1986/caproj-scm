﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="FinishedGoods.aspx.cs" Inherits="CAPROJ.FinishedGoods" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
      <section class="content-header">
         <h1>
            Finished Goods Master Data
         </h1>
      </section>
      <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
               <div class="row">
                  <div class="col-md-12">
                     <%-- 1 column is equal to 12 so if its 6 its only half of the page --%>
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#Gen" data-toggle="tab">General</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="Gen">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="row">
                                       <div class="col-md-2">
                                          <div class="form-group">
                                             Finished Goods ID:  
                                             <asp:Label ID="FinishedGoods_ID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                        <div class="col-md-6">
                                           <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                                       </div>
                                         <div class="col-md-4">
                                           
                                       </div>
                                       
                                    </div>
    
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <small>Finished Goods Name </small>
                                                 <asp:TextBox ID="FinishedGoodsName" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>Category</small>
                                                 <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                                     <asp:ListItem Value="Windows">Windows</asp:ListItem>
                                                     <asp:ListItem Value="Doors">Doors</asp:ListItem>
                                                     <asp:ListItem Value="Panel">Panel</asp:ListItem>
                                                 </asp:DropDownList>
                                                 <br />
                                                  <small>Unit Price </small>
                                                 <asp:TextBox ID="UnitPrice" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                 <br />
                                                 <small>Lead Time </small>
                                                 <asp:TextBox ID="LeadTime" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                             </div>
                                         </div>
                                         
                                     </div>
                                     <br>
                                     <br></br>
                                     </br>
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                         
                     </div>
                        <div class="row">
                             <div class="col-md-2">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-block btn-danger" Text="Cancel" BackColor="#b01717" OnClick="btnCancel_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnAdd" runat="server" class="btn btn-block btn-danger" Text="Add" BackColor="#b01717" OnClick="btnAdd_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnEdit" runat="server" class="btn btn-block btn-danger" Text="Edit" BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnDelete" runat="server" class="btn btn-block btn-danger" Text="Delete" BackColor="#b01717" OnClick="btnDelete_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnSave" runat="server" class="btn btn-block btn-danger" Text="Save" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                             </div>
                         </div>
                     
                  </div>
               </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
         </asp:UpdatePanel>
      </section>
   </aside>
</asp:Content>