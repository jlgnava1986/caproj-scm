﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DeliveryCustomer.aspx.cs" Inherits="CAPROJ.DeliveryCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Delivery Customer 
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                  <asp:GridView ID="GridCustomerDelivery" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"                                       
                                        BorderStyle="None" Class="table" AllowSorting="True" OnSorting="GridCustomerDelivery_Sorting">
                                        <Columns>
                                            <asp:BoundField DataField="DeliverCustomer_ID" HeaderText="Delivery #" SortExpression="DeliverCustomer_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Customer_ID" HeaderText="Customer ID" SortExpression="Customer_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="customer_name" HeaderText="Customer Name" SortExpression="customer_name">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SalesOrder_ID" HeaderText="Sales Order #" SortExpression="SalesOrder_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="date_order" HeaderText="Date Ordered" DataFormatString="{0:MM/dd/yyyy}" SortExpression="date_order">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="schedule" HeaderText="Scheduled Delivery" DataFormatString="{0:MM/dd/yyyy}" SortExpression="schedule">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="date_received" HeaderText="Date Received" DataFormatString="{0:MM/dd/yyyy}" SortExpression="date_received">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="status" HeaderText="Delivery Status" SortExpression="status">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                    </asp:GridView>
                            </div>
                        </div>
                    </div>        
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>

</asp:Content>

