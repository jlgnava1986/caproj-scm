﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data;
using System.Data.SqlClient;

namespace CAPROJ
{
    public partial class FinishedGoods : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadFinishedGoodsList();
            }


        }
        public void LoadFinishedGoodsList()
        {
            _SQL.getDropdown("Select * from md_finished_goods", "FinishedGoods_ID", "finished_goods_name", DropDownList1);
        }
        public void LoadFinishedGoods()
        {
            
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_finished_goods where FinishedGoods_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();

                FinishedGoods_ID.Text = dr["FinishedGoods_ID"].ToString();
                FinishedGoodsName.Text = dr["finished_goods_name"].ToString().Trim();
                DropDownList2.SelectedValue = dr["category"].ToString().Trim();
                UnitPrice.Text = dr["unit_price"].ToString().Trim();
                LeadTime.Text = dr["lead_time"].ToString().Trim();

                //Disable the controls

                FinishedGoodsName.ReadOnly = true;
                DropDownList2.Enabled = false;
                UnitPrice.ReadOnly = true;
                LeadTime.ReadOnly = true;
                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();




        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadFinishedGoods();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            FinishedGoodsName.ReadOnly = false;
            DropDownList2.Enabled = true;
            UnitPrice.ReadOnly = false;
            LeadTime.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                _SQL.Open();
                //Machine data
                _SQL.AddParameters("finished_goods_name", FinishedGoodsName.Text.Trim());
                _SQL.AddParameters("category", DropDownList2.SelectedValue);
                _SQL.AddParameters("unit_price", UnitPrice.Text.Trim());
                _SQL.AddParameters("lead_time", LeadTime.Text.Trim());

                string query = @"INSERT INTO md_finished_goods(finished_goods_name,category, unit_price, lead_time)
                                       VALUES(@finished_goods_name,@category, @unit_price, @lead_time);";

                _SQL.ExecuteNonQuery(query);
                _SQL.Close();


                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Finished Goods Successfully Added!'); window.location='" + Request.ApplicationPath + "FinishedGoodsList.aspx';", true);
            }
            else
            {
                _SQL.Open();
                _SQL.AddParameters("FinishedGoods_ID", DropDownList1.SelectedValue);
                _SQL.AddParameters("finished_goods_name", FinishedGoodsName.Text.Trim());
                _SQL.AddParameters("category", DropDownList2.SelectedValue);
                _SQL.AddParameters("unit_price", UnitPrice.Text.Trim());
                _SQL.AddParameters("lead_time", LeadTime.Text.Trim());
                _SQL.ExecuteNonQuery(@"UPDATE md_finished_goods SET finished_goods_name=@finished_goods_name,
                                           category=@category, unit_price=@unit_price, lead_time=@lead_time where FinishedGoods_ID=@FinishedGoods_ID");
                _SQL.Close();

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Finished Goods Successfully Updated!'); window.location='" + Request.ApplicationPath + "FinishedGoods.aspx';", true);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_finished_goods WHERE FinishedGoods_ID='" + FinishedGoods_ID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Finished Goods Successfully Deleted!'); window.location='" + Request.ApplicationPath + "FinishedGoods.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("FinishedGoods.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            FinishedGoodsName.Text = "";
            UnitPrice.Text = "";
            LeadTime.Text = "";
            //Enable the controls
            FinishedGoodsName.ReadOnly = false;
            UnitPrice.Text = "";
            DropDownList2.Enabled = true;
            LeadTime.Text = "";
            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }
    }

}