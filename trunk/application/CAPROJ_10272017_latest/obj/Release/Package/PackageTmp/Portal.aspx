﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Portal.aspx.cs" Inherits="ETIIOnlineServices.Portal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">    
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Dashboard
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="box box-primary">
                                <div class="info-box">
                                    <div class="small-box">
                                        <span class="info-box-icon">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/projects/vrc.jpg"></asp:Image>
                                        </span>
                                        <div class="inner">
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Open:<asp:Label ID="lblOpenUnitsCntVRC" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Reserved:<asp:Label ID="lblReservedUnitsCntVRC" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Onhold:<asp:Label ID="lblHoldUnitsCntVRC" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                        </div>
                                        <asp:LinkButton ID="lnkDetailsVRC" runat="server" CssClass="small-box-footer" onclick="lnkDetailsVRC_Click">
                                            <font color="#0079AA">View Details <i class="fa fa-arrow-circle-right"></i></font>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="box box-primary">
                                <div class="info-box">
                                    <div class="small-box">
                                        <span class="info-box-icon">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/projects/vrd.jpg"></asp:Image>
                                        </span>
                                        <div class="inner">
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Open:<asp:Label ID="lblOpenUnitsCntVRD" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Reserved:<asp:Label ID="lblReservedUnitsCntVRD" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Onhold:<asp:Label ID="lblHoldUnitsCntVRD" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                        </div>
                                        <asp:LinkButton ID="lnkDetailsVRD" runat="server" CssClass="small-box-footer" onclick="lnkDetailsVRD_Click">
                                            <font color="#0079AA">View Details <i class="fa fa-arrow-circle-right"></i></font>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="box box-primary">
                                <div class="info-box">
                                    <div class="small-box">
                                        <span class="info-box-icon">
                                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/projects/mrf.jpg"></asp:Image>
                                        </span>
                                        <div class="inner">
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Open:<asp:Label ID="lblOpenUnitsCntMRF" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Reserved:<asp:Label ID="lblReservedUnitsCntMRF" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                            <span class="info-box-text">&nbsp;&nbsp;&nbsp;&nbsp;Onhold:<asp:Label ID="lblHoldUnitsCntMRF" runat="server" Text="" Font-Bold="true"></asp:Label></span>
                                        </div>
                                        <asp:LinkButton ID="lnkDetailsMRF" runat="server" CssClass="small-box-footer" onclick="lnkDetailsMRF_Click">
                                            <font color="#0079AA">View Details <i class="fa fa-arrow-circle-right"></i></font>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">                           
                                <!--Start of chart-->                                
                                    <div class="box box-primary">
                                        <div class="box-header" style="background-color:#0079aa">
                                            <h1 class="box-title">
                                                <font color="white">Sales & Cancellation Report</font>
                                            </h1>                                         
                                                <!--Put the calendar to top of other controls-->
                                                <script type="text/javascript">
                                                    function OnClientShown(sender, args) {
                                                        sender._popupBehavior._element.style.zIndex = 10005;
                                                    }
                                                </script>
                                                <div class="pull-right" style="padding:5px 5px 5px 5px">
                                                    <!-- Year Drop Down-->
                                                    <asp:DropDownList ID="DropYear" runat="server" 
                                                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropYear_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="pull-right" style="padding:5px 5px 5px 5px">
                                                    <!-- Project Drop Down-->
                                                    <asp:DropDownList ID="DropDownListProject" runat="server" 
                                                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                        </div>
                                        <center>
                                            <div id="js-legend" class="chart-legend"></div>
                                        </center>
                                        <div class="box-body chart-responsive">
                                            <div id="chartAppend" class="chart">
                                                <canvas id="canvas" style="height:250px"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                
                                </span></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="box box-primary">
                                    <div class="box-header" style="background-color:#0079aa">
                                        <h1 class="box-title">
                                            <font color="white">Sales & Cancellation Summary</font>
                                        </h1>
                                    </div>
                                    <div class="box-body table-responsive">
                                        <asp:GridView ID="GridSalesCancSummary" runat="server" AutoGenerateColumns="False" 
                                            GridLines="None"  AllowPaging="true"
                                            BorderStyle="None" Class="table" AllowSorting="True" ShowFooter="false">
                                            <Columns>
                                                <asp:BoundField DataField="ch_proj_code" HeaderText="Project" ItemStyle-Font-Bold="true">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="RESERVED"  ItemStyle-Font-Bold="true" HeaderText="Sales">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="CANCELLED"  ItemStyle-Font-Bold="true" HeaderText="Cancelled">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="NET"  ItemStyle-Font-Bold="true" HeaderText="Net Reserved">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle HorizontalAlign="Center" />
                                            <FooterStyle Font-Bold="true" BackColor="#E6E6E6" />
                                            <HeaderStyle Font-Bold="True"/>
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                        </asp:GridView>
                                        <div style="font-weight:bold; background-color:#E6E6E6;">
                                            <table class="table" id="GridSalesCancSummaryFooter" cellspacing=0 style="border-style:None;border-collapse:collapse;">
                                              <tr align="center">
                                                <td align="left" valign="middle" style="width:25%">TOTAL</td>
                                                <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkTotalSales" runat="server" onclick="lnkTotalSales_Click"><asp:Label ID="lblTotalSales" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkTotalCancelled" runat="server" onclick="lnkTotalCancelled_Click"><asp:Label ID="lblTotalCancelled" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkNetSales" runat="server" onclick="lnkNetSales_Click"><asp:Label ID="lblNet" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                              </tr>
                                            </table>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Start of Dialog Box -->
                            <div class="modal fade" id="modalSales" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" style="width:auto">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button id="btnClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"></i>List of <asp:Label ID="lblResCanc" runat="server" Text=""></asp:Label> Accounts - <asp:Label ID="lblYear" runat="server" Text=""></asp:Label></h4>
                                        </div>
                                        <%--<form runat="server">--%>
                                            <div class="modal-body" style="position:relative;overflow-x:auto;overflow-y:auto;max-height:500px;width:auto;padding:15px;">                                    
                                                <div class="form-group">
                                                    <div class="box-body table-responsive no-padding">
                                                        <asp:GridView ID="GridCommRec" runat="server" AutoGenerateColumns="False" 
                                                            GridLines="None"  AllowPaging="false"
                                                            OnPageIndexChanging="GridCommRec_PageIndexChanging"
                                                            BorderStyle="None" Class="table" AllowSorting="True">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="vc_client_name" HeaderText="Client Name" Visible="true">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="380px" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="ch_unit_id" HeaderText="Unit ID">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="160px" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="nu_comm_rate" HeaderText="Rate">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="mo_tcp" HeaderText="TCP" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="190px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="mo_comm_amount" HeaderText="Gross" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="ReleaseComm" HeaderText="Released" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="BalanceComm" HeaderText="Remaining" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="vc_type" HeaderText="Type" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="sd_res_date" HeaderText="Status Date" dataformatstring="{0:MM/dd/yyyy}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="vc_res_status" HeaderText="Reservation Status">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="sd_res_date" HeaderText="Date Cancelled" dataformatstring="{0:MM/dd/yyyy}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="vc_reason_desc" HeaderText="Reason for Cancellation">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <RowStyle HorizontalAlign="Center" />
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <HeaderStyle BackColor="#ffffff" Font-Bold="True"/>
                                                            <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                            <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        <%--</form>--%>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Dialog Box -->
                            <!-- Start of Dialog Box - Disclaimer -->
                            <asp:HiddenField ID="disclaimer" runat="server"></asp:HiddenField>
                            <div class="modal fade" id="modalDisclaimer" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <asp:Button ID="btnClose" runat="server" Text="&times;" CssClass="close" 
                                                onclick="btnClose_Click"></asp:Button>
<%--                                            <button id="Button1" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                                            <h4 class="modal-title">Disclaimer</h4>
                                        </div>
                                        <div class="modal-body">                                    
                                            <div class="form-group">
                                                <div class="box-body table-responsive no-padding">
                                                    The information on this portal is for general reference only and may not contain the updated information from the Company. The Company makes no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability,
                                                    suitability or availability with respect to the information provided in this portal. It is incumbent upon the user of this portal to obtain separate confirmation from the Company.
                                                    <br/><br/>
                                                    Any information or data gathered from this portal shall be treated with utmost confidentiality. Any dissemination of the information contained herein without the Company’s written consent shall be a breach on the use of this portal and any subsequent access shall be denied, with reservation to any other remedy or action that the Company may take for such violation.
                                                    <br/><br/>
                                                    The Company will take legal action against the user of this portal for any loss or damage to third persons occasioned by any dissemination without the Company’s consent or misrepresentation of the information contained in this portal other than to the authorized user.
                                                    <br/><br/>
                                                    The Company takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer clearfix">
                                              <asp:Button ID="btnAgree" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-success pull-right" CommandName="Action" Text='I Agree' onclick="btnAgree_Click"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Dialog Box -->
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="small-box" style="background-color:#0079aa">
                                    <div class="inner">
                                    <font color="white">
                                        <h3>
                                            <asp:Label ID="lblUpdatedAccts" runat="server" Text=""></asp:Label>                                        
                                        </h3>
                                        <p>
                                            ACCOUNTS<br/> 
                                            With Updated and Complete Docs
                                        </p>
                                    </font>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-ios7-cart-outline"></i>
                                    </div>
                                    <asp:LinkButton ID="lnkUpdated" runat="server" CssClass="small-box-footer" PostBackUrl="~/SLedger1.aspx?Filter=0">
                                        View Details <i class="fa fa-arrow-circle-right"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>                    
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <!-- LINE CHART -->
                                <div class="box box-primary">
                                    <div class="box-header" style="background-color:#0079aa">
                                        <h1 class="box-title">
                                            <font color="white">Net Commission Forecast</font><font color="red"> (Beta Test)</font>
                                        </h1>                                         
                                        <div class="pull-right" style="padding:5px 5px 5px 5px">
                                            <!-- To Drop Down-->
                                            <asp:DropDownList ID="ddlTo" runat="server" 
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropFromTo_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="pull-right" style="padding:5px 5px 5px 5px">
                                            <!-- From Drop Down-->
                                            <asp:DropDownList ID="ddlFrom" runat="server" 
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropFromTo_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="pull-right" style="padding:5px 5px 5px 5px">
                                            <!-- Time Frame -->
                                            <asp:DropDownList ID="ddlFilterLineChart" runat="server" 
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropYear_SelectedIndexChanged">
                                                <asp:ListItem Value="Yearly">Yearly</asp:ListItem>
                                                <asp:ListItem Value="Quarterly">Quarterly</asp:ListItem>
                                                <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <center>
                                        <div id="Div1" class="chart-legend"></div>
                                    </center>
                                    <div class="box-body chart-responsive">
                                        <div class="box-body chart-responsive">
                                            <div id="chartAppend1" class="chart">
                                                <canvas id="canvas1" style="height:400px"></canvas>
                                            </div>
                                        </div><!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box box-primary">
                                            <div class="box-header" style="background-color:#0079aa;">
                                                <h1 class="box-title">
                                                    <font color="white">Updated Accounts w/ Documents Concern</font><font color="#fcf41b"> &#9632;</font>
                                                </h1>
                                            </div>
                                            <div class="box-body table-responsive">
                                                <asp:GridView ID="GridDocPayStatus1" runat="server" AutoGenerateColumns="False" 
                                                    GridLines="None"  AllowPaging="true"
                                                    BorderStyle="None" Class="table" AllowSorting="True" ShowFooter="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="vc_res_status" HeaderText="Reservation Status" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Updated_Incomplete_Initial_Docs" HeaderText="Incomplete Initial Docs" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Updated_Incomplete_Additional_Docs" HeaderText="Incomplete Add. Docs" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>
                                                     </Columns>
                                                    <RowStyle HorizontalAlign="Center" />
                                                    <FooterStyle Font-Bold="true" BackColor="#E6E6E6" HorizontalAlign="Center" />
                                                    <HeaderStyle BackColor="#ffffff" Font-Bold="True"/>
                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                                </asp:GridView>
                                                <div style="font-weight:bold; background-color:#E6E6E6;">
                                                    <table class="table table-responsive">
                                                      <tr align="center">
                                                        <td align="left" valign="middle" style="width:25%">TOTAL</td>
                                                        <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkDocPayStatus11" runat="server" PostBackUrl="SLedger1.aspx?Filter=1"><asp:Label ID="lblDocPayStatus11" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                        <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkDocPayStatus22" runat="server" PostBackUrl="SLedger1.aspx?Filter=2"><asp:Label ID="lblDocPayStatus22" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                      </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="box box-primary">
                                            <div class="box-header" style="background-color:#0079aa;">
                                                <h1 class="box-title">
                                                    <font color="white">Past Due Accounts w/ Documents Status</font><font color="#d32730"> &#9632;</font>
                                                </h1>
                                            </div>
                                            <div class="box-body table-responsive">
                                                <asp:GridView ID="GridDocPayStatus" runat="server" AutoGenerateColumns="False" 
                                                    GridLines="None"  AllowPaging="true"
                                                    BorderStyle="None" Class="table" AllowSorting="True" ShowFooter="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="vc_res_status" HeaderText="Reservation Status" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Past_Due_Complete_Docs" HeaderText="Complete Docs" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Past_Due_Incomplete_Docs" HeaderText="Incomplete Docs" ItemStyle-Font-Bold="true">
                                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="25%"/>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <RowStyle HorizontalAlign="Center" />
                                                    <FooterStyle Font-Bold="true" BackColor="#E6E6E6" HorizontalAlign="Center" />
                                                    <HeaderStyle BackColor="#ffffff" Font-Bold="True"/>
                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                                </asp:GridView>
                                                <div style="font-weight:bold; background-color:#E6E6E6;">
                                                    <table class="table">
                                                      <tr align="center">
                                                        <td align="left" valign="middle" style="width:25%">TOTAL</td>
                                                        <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkDocPayStatus1" runat="server" PostBackUrl="SLedger1.aspx?Filter=3"><asp:Label ID="lblDocPayStatus1" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                        <td align="left" valign="middle" style="width:25%"><asp:LinkButton ID="lnkDocPayStatus2" runat="server" PostBackUrl="SLedger1.aspx?Filter=4"><asp:Label ID="lblDocPayStatus2" runat="server" Text=""></asp:Label></asp:LinkButton></td>
                                                      </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="main-footer pull-right" style="padding:10px; background-color:#F9F9F9">
                            <strong>Copyright © <a href="http://www.eurotowersintl.com">Euro Towers International, Inc.</a> | <asp:LinkButton ID="lnk" runat="server" OnClick="lnkViewDisclaimer_Click">Disclaimer</asp:LinkButton></strong>
                        </footer>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostbackTrigger ControlID="DropYear" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostbackTrigger ControlID="DropDownListProject" EventName="SelectedIndexChanged" />
                    </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:HiddenField ID="lblP" runat="server"></asp:HiddenField>
        </section>
    </aside>

    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');

            //Load Disclaimer
            if (document.getElementById("<%=disclaimer.ClientID%>").value == "False") {
                $('#modalDisclaimer').modal('show');
            }

        });
    </script>

    <script type="text/javascript">
        //Bar Chart
        var myBar;
        var barChartData;
        var barChartOptions;
        var ctx;
        var aLabels;
        var aDatasets1;
        var aDatasets2;
        var barChartData1;
        var barChartOptions1;

        //Line Chart
//        var myLine;
//        var lineChartData;
//        var lineChartOptions;
//        var lineLabels;
//        var lineDataset;
//        var lineChartData1;
//        var lineChartOptions1;

        //Bar Chart
        function DrawChart() {
            var ddl = document.getElementById("<%=DropDownListProject.ClientID%>");
            var getProject = ddl.options[ddl.selectedIndex].value;
            var ddl1 = document.getElementById("<%=DropYear.ClientID%>");
            var getYear = ddl1.options[ddl1.selectedIndex].value;
            var key = document.getElementById("<%=lblP.ClientID%>").value;
            var jsonData = JSON.stringify({
                projCode: getProject,
                year: getYear,
                key: key
            });
            $.ajax({
                type: "POST",
                url: "/Portal.aspx/getBarChartData",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess_,
                error: OnErrorCall_
            });

            barChartOptions = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                //String - A legend template
                //Boolean - whether to make the chart responsive
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    position: 'top'
                },
                title: {
                    display: false,
                    text: 'Sales & Cancellation Report'
                }
            };

            function OnSuccess_(reponse) {
                var aData = reponse.d;
                aLabels = aData[0];
                aDatasets1 = aData[1];
                aDatasets2 = aData[2];
                barChartData = {
                    labels: aLabels,
                    datasets: [{
                        label: 'Reserved',
                        backgroundColor: "rgba(60,141,188,0.9)",
                        borderColor: "rgba(60,141,188,0.8)",
                        borderWidth: 1,
                        hoverBackgroundColor: "rgba(31,72,97,1)",
                        hoverBorderColor: "rgba(31,72,97,1)",
                        data: aDatasets1
                    }, {
                        label: 'Cancelled',
                        backgroundColor: "rgba(206,16,25,0.9)",
                        borderColor: "rgba(206,16,25,0.8)",
                        borderWidth: 1,
                        hoverBackgroundColor: "rgba(182,14,22,1)",
                        hoverBorderColor: "rgba(182,14,22,1)",
                        data: aDatasets2
                    }]
                };
            }

            function OnErrorCall_(repo) {
                alert("Woops something went wrong, pls try later !");
            }
        }

        window.onload = function () {
            //Bar Chart
            ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(window.ctx, {
                type: 'bar',
                data: window.barChartData,
                options: window.barChartOptions
            });
        };

        //Line Chart
        function LoadLineChart() {
            $('#canvas1').remove();
            $('#chartAppend1').append('<canvas id="canvas1" style="height:400px"></canvas>');

            var key = document.getElementById("<%=lblP.ClientID%>").value;
            var jsonData1 = JSON.stringify({
                key: key
            });
            $.ajax({
                type: "POST",
                url: "/Portal.aspx/getLineChartData",
                data: jsonData1,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess__,
                error: OnErrorCall__
            });

            var lineChartOptions = {
                pointDotRadius: 10,
                bezierCurve: true,
                scaleShowVerticalLines: false,
                scaleGridLineColor: "black",
                legend: {
                    position: 'top'
                },
                title: {
                    display: false,
                    text: 'Commission Foreacast'
                }
            };

            function OnSuccess__(reponse) {
                var lineData = reponse.d;
                var lineLabels = lineData[0];
                var lineDatasets = lineData[1];
                var lineChartData = {
                    labels: lineLabels,
                    datasets: [{
                        label: "Year",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: lineDatasets
                    }]
                };

                //Line Chart
                var ctxl = document.getElementById("canvas1").getContext("2d");
                var myLine = new Chart(ctxl).Line(window.lineChartData, {
                    pointDotRadius: 10,
                    bezierCurve: true,
                    scaleShowVerticalLines: false,
                    scaleGridLineColor: "black"
                });

            }

            function OnErrorCall__(repo) {
                alert("Woops something went wrong, pls try later !");
            }
        };

        //Bar Chart
        function LoadChart() {
            $('#canvas').remove();
            $('#chartAppend').append('<canvas id="canvas" style="height:250px"></canvas>');
            var ddl = document.getElementById("<%=DropDownListProject.ClientID%>");
            var getProject = ddl.options[ddl.selectedIndex].value;
            var ddl1 = document.getElementById("<%=DropYear.ClientID%>");
            var getYear = ddl1.options[ddl1.selectedIndex].value;
            var key = document.getElementById("<%=lblP.ClientID%>").value;
            var jsonData = JSON.stringify({
                projCode: getProject,
                year: getYear,
                key: key
            });
            $.ajax({
                type: "POST",
                url: "/Portal.aspx/getBarChartData",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess_,
                error: OnErrorCall_
            });

            var barChartOptions2 = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                //String - A legend template
                //Boolean - whether to make the chart responsive
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    position: 'top'
                },
                title: {
                    display: false,
                    text: 'Sales & Cancellation Report'
                }
            };

            function OnSuccess_(reponse) {
                var aData1 = reponse.d;
                var aLabels1 = aData1[0];
                var aDatasets11 = aData1[1];
                var aDatasets22 = aData1[2];
                var barChartData2 = {
                    labels: aLabels1,
                    datasets: [{
                        label: 'Reserved',
                        backgroundColor: "rgba(60,141,188,0.9)",
                        borderColor: "rgba(60,141,188,0.8)",
                        borderWidth: 1,
                        hoverBackgroundColor: "rgba(31,72,97,1)",
                        hoverBorderColor: "rgba(31,72,97,1)",
                        data: aDatasets11
                    }, {
                        label: 'Cancelled',
                        backgroundColor: "rgba(206,16,25,0.9)",
                        borderColor: "rgba(206,16,25,0.8)",
                        borderWidth: 1,
                        hoverBackgroundColor: "rgba(182,14,22,1)",
                        hoverBorderColor: "rgba(182,14,22,1)",
                        data: aDatasets22
                    }]
                };

                var ctx1 = document.getElementById("canvas").getContext("2d");
                var myBar1 = new Chart(ctx1, {
                    type: 'bar',
                    data: barChartData2,
                    options: barChartOptions2
                });
            }

            function OnErrorCall_(repo) {
                alert("Woops something went wrong, pls try later !");
            }

            //Line Chart
            $('#canvas1').remove();
            $('#chartAppend1').append('<canvas id="canvas1" style="height:400px"></canvas>');

            var filterLineChart = document.getElementById("<%=ddlFilterLineChart.ClientID%>");
            var getFiler = filterLineChart.options[filterLineChart.selectedIndex].value;
            var filterFrom = document.getElementById("<%=ddlFrom.ClientID%>");
            var getFilerFrom = filterFrom.options[filterFrom.selectedIndex].value;
            var filterTo = document.getElementById("<%=ddlTo.ClientID%>");
            var getFilerTo = filterTo.options[filterTo.selectedIndex].value;
            var jsonData1 = JSON.stringify({
                filter: getFiler,
                key: key,
                from: getFilerFrom,
                to: getFilerTo
            });

            $.ajax({
                type: "POST",
                url: "/Portal.aspx/getLineChartData",
                data: jsonData1,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess__,
                error: OnErrorCall__
            });

            var lineChartOptions = {
                bezierCurve: true,
                scaleShowVerticalLines: false,
                scaleGridLineColor: "black",
                legend: {
                    position: 'top'
                },
                title: {
                    display: false,
                    text: 'Commission Foreacast'
                }
            };

            function OnSuccess__(reponse) {
                var lineData = reponse.d;
                var lineLabels = lineData[0];
                var lineDatasets = lineData[1];
                var lineChartData = {
                    labels: lineLabels,
                    datasets: [{
                        label: "Amount",
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'round',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'round',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        pointHitRadius: 10,
                        data: lineDatasets
                    }]
                };

                //Line Chart
                var ctxl = document.getElementById("canvas1").getContext("2d");

                var myLine = new Chart(ctxl, {
                    type: 'line',
                    data: lineChartData,
                    options: { 
                        pointDotRadius: 10,
                        bezierCurve: true,
                        scaleShowVerticalLines: true,
                        scaleGridLineColor: "black",
                        legend: {
                            display: false
                        },
                        tooltips: {
                           mode: 'label',
                           label: 'mylabel',
                           callbacks: {
                               label: function(tooltipItem, data) {
                                   return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); } }
                           },
                        scales: {
                                xAxes: [{
                                  ticks: {}
                                }],
                                yAxes: [{
                                  ticks: {
                                    beginAtZero: false,
           	                        // Return an empty string to draw the tick line but hide the tick label
           	                        // Return `null` or `undefined` to hide the tick line entirely
           	                        userCallback: function(value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end
                                        value = value.toString();
                                        value = value.split(/(?=(?:...)*$)/);
                
                                        // Convert the array to a string and format the output
                                        value = value.join(',');
                                        return 'P' + value;
            	                        }
                                  }
                                }]
                              }
                    }
                });

            }

            function OnErrorCall__(repo) {
                alert("Woops something went wrong, pls try later !");
            }
        };

        window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
            //alert("Error occured: " + errorMsg);
            return false;
        }

        </script>
</asp:Content>
