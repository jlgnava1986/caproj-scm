﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CAPROJ
{
    public partial class SupplierList : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        private string _sortDirection;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadSupplierList(txtFilter.Text);
                LoadReport();
            }
        }

        public void LoadSupplierList(string filter)
        {
            string SQL = "select * from dbo.md_supplier_master_data where ('" + filter + "'='') or ('" + filter + "'<>'' and (supplier_name like '%' + '" + filter + "' +'%' or address like '%' + '" + filter + "' +'%' or contact_no like '%' + '" + filter + "' +'%' or discount like '%' + '" + filter + "' + '%' or product_name like '%' + '" + filter + "' + '%' or lead_time like '%' + '" + filter + "' + '%' or price like '%' + '" + filter + "' + '%' or past_orders like '%' + '" + filter + "' + '%'))";
            GridSupplierList.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSupplierList.DataSource = dt;
                GridSupplierList.DataBind();
                GridSupplierList.Visible = true;
                ViewState["currentTable"] = dt;
            }
        }

        protected void GridSupplierList_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(ViewState["currentTable"]);
            {
                string SortDir = string.Empty;
                if (dir == System.Web.UI.WebControls.SortDirection.Ascending)
                {
                    dir = System.Web.UI.WebControls.SortDirection.Descending;
                    SortDir = "Desc";
                }
                else
                {
                    dir = System.Web.UI.WebControls.SortDirection.Ascending;
                    SortDir = "Asc";
                }
                DataView sortedView = new DataView(dt);
                sortedView.Sort = e.SortExpression + " " + SortDir;
                GridSupplierList.DataSource = sortedView;
                GridSupplierList.DataBind();
            }
        }

        public System.Web.UI.WebControls.SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = System.Web.UI.WebControls.SortDirection.Ascending;
                }
                return (System.Web.UI.WebControls.SortDirection)ViewState["dirState"];
            }

            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void LoadReport()
        {
            ReportDocument reportviewer = new ReportDocument();
            FormulaFieldDefinitions pubFormulaFields;
            FormulaFieldDefinition pubFormulaField;

            ConnectionInfo Connection_Info = new ConnectionInfo();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            Tables CrTables;

            string[] strFormulaParam = { "printedby" };
            string[] strFormulaValue = { "'" + Session["fullname"].ToString() + "'" };
            reportviewer.Load(Server.MapPath("Report/SupplierList.rpt"));

            Connection_Info.ServerName = "LAPTOP-AI3BIIL8\\ALYSSA";
            Connection_Info.DatabaseName = "CAPROJ";
            Connection_Info.UserID = "sa";
            Connection_Info.Password = "abc@123";
            CrTables = reportviewer.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = Connection_Info;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            DataTable dtMain = (DataTable)ViewState["currentTable"];
            reportviewer.Database.Tables[0].SetDataSource(dtMain);

            CrystalReportViewer1.ReportSource = reportviewer;

            pubFormulaFields = reportviewer.DataDefinition.FormulaFields;
            if (pubFormulaFields.Count > 0 && strFormulaParam != null)
            {
                for (int i = 0; (i <= (strFormulaParam.Length - 1)); i++)
                {
                    if ((strFormulaParam[i].Trim().Length > 0))
                    {
                        reportviewer.DataDefinition.FormulaFields[strFormulaParam[i]].Text = strFormulaValue[i];
                    }
                }
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadSupplierList(txtFilter.Text);
            LoadReport();
        }
    }
}