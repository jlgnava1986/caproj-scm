﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="GoodsReturn.aspx.cs" Inherits="CAPROJ.GoodsReturn" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Goods Return
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <%--HEADER--%>
                                <div class="col-md-12">
                    <div id="printArea">
                        <div class="row">
                            <div class="col-md-10">
                                &nbsp;
                            </div>
                            <div class="col-md-2">
                                <small>Date Return</small>
                                <asp:TextBox runat="server" id="txtReturn" class="form-control" placeholder="Date Return" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>
                                  <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <small>Purchase Order #</small>
                                    <asp:DropDownList ID="dropPO" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                                </div>                                
                            </div>
                            <div class="col-md-5">
                               <div class="form-group">
                                    <small>Supplier Name</small>
                                    <asp:TextBox runat="server" id="txtSupplierName" class="form-control" placeholder="Supplier Name" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <small>Date Order</small>
                                <asp:TextBox runat="server" id="txtDateOrder" class="form-control" placeholder="Date Order" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <small>Date Received</small>
                                <asp:TextBox runat="server" id="txtDateReceived" class="form-control" placeholder="Date Received" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="col-md-12">
                                  
                                <%--END HEADER--%>

                                <%--GRIDVIEW--%>
                                 <div class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridGoodsReturn" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true" ShowFooter="true"
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Item Name">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="item_name" runat="server" class="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Quantity">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="quantity" runat="server" class="form-control" TextMode="Number" AutoPostBack="true" OnTextChanged="quantity_TextChanged"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Unit Price">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="unit_price" runat="server" class="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Discount">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="discount" runat="server" class="form-control" TextMode="Number"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tax">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="tax" runat="server" class="form-control" TextMode="Number"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    Total Payment Due:
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="total" runat="server" class="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="Right" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="totalPaymentDue" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                       <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="white" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                                          <div id="tblGoodsReturn" class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridGoodsReturnViewing" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true" ShowFooter="true"      
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="cnt" HeaderText="#">
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="item_no" HeaderText="Item No.">
                                                <HeaderStyle HorizontalAlign="Left" Width="40px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="item_name" HeaderText="Item Name">
                                                <HeaderStyle HorizontalAlign="left" Width="100px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="quantity" HeaderText="Quantity">
                                                <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="unit_price" HeaderText="Unit Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="left" Width="50px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="discount" HeaderText="Discount" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                <HeaderStyle HorizontalAlign="left" Width="50px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="tax" HeaderText="Tax">
                                                <HeaderStyle HorizontalAlign="left" Width="50px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="total" HeaderText="Total">
                                                <HeaderStyle HorizontalAlign="left" Width="80px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        </div>
                              <%--END GRIDVIEW--%>

                            </div>
                        </div>
                        <%--SAVE BUTTON--%>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtRemarks" runat="server" class="form-control" placeholder="Remarks" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-block btn-danger" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                            </div>
                            <div class="col-md-3">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-block btn-danger" BackColor="#b01717" OnClick="btnCancel_Click"></asp:Button>
                            </div>
                        </div>

                        <%--END SAVE BUTTON--%>

                    </div>
                </ContentTemplate>
                <Triggers>
                    
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
</asp:Content>

