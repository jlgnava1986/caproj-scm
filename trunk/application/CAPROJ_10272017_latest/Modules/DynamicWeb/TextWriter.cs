using System;

/// <summary>
/// Summary description for TextWriter.
/// </summary>
public class TextWriter
{
	public TextWriter()
	{
	}
	
	static public string MakeParagraph(string s)
	{
		return "<p>" + s + "</p>";
	}

    static public string MakeIFrame(string src, string style, string frameborder)
    {
        return "<iframe src='"+ src + "' style='" + style + "' frameborder='" + frameborder +  "' />";
    }

    static public string MakeLine(string s)
	{
		return s + "<br />";
	}

	static public string Span(string strClass, string strText)
	{
		return "<span class=\"" + strClass + "\">" + strText + "</span>";
	}

	static public string Div(string strClass, string strText)
	{
		return "<div class=\"" + strClass + "\">" + strText + "</div>";
	}

	static public string MakeH1Text(string s)
	{
		return "<h1>" + s + "</h1>";
	}

	static public string MakeH2Text(string s)
	{
		return "<h2>" + s + "</h2>";
	}

	static public string MakeH3Text(string s)
	{
		return "<h3>" + s + "</h3>";
	}

	static public string MakeH4Text(string s)
	{
		return "<h4>" + s + "</h4>";
	}

	static public string MakeH5Text(string s)
	{
		return "<h5>" + s + "</h5>";
	}

	static public string MakeH6Text(string s)
	{
		return "<h6>" + s + "</h6>";
	}

	static public string MakeItalicText(string s)
	{
		return "<i>" + s + "</i>";
	}

	static public string MakeBoldText(string s)
	{
		return "<b>" + s + "</b>";
	}

    static public string MakeMenu(string strClass, string strText, string strLink, string strIcon, Boolean hasSubMenu)
    {
        if (!hasSubMenu)
        {
            return @"<li class='" + strClass + "'>" +
                        "<a href='/" + strLink + "'>" +
                            "<i class='" + strIcon + "'></i><span>" + strText + "</span>" +
                        "</a>"
                    + "</li>";
        }
        else
        {
            return @"<li class='" + strClass + "' style='display:block'>" +
                        "<a href='/" + strLink + "'>" +
                            "<i class='" + strIcon + "'></i><span>" + strText + "</span><i class='fa fa-angle-left pull-right'></i>" +
                        "</a>" +
                        "<ul class='treeview-menu'>";
        }
    }

    static public string MakeSubMenu(string strClass, string strText, string strLink, string strIcon)
    {
        return @"<li class=" + strClass + ">" +
                    "<a href='/" + strLink + "'>" +
                        "<i class='" + strIcon + "'></i>" + strText + "<small class='badge pull-right bg-red'></small>" +
                    "</a>"
                + "</li>";
    }

} // end of class declaration

