﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class CustomerList : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadCustomerList(txtFilter.Text);
            }
        }

        public void LoadCustomerList(String filter)
        {
            string SQL;
            SQL = "select * from dbo.md_customer_master_data where ('" + filter + "'='') or ('" + filter + "'<>'' and (" + DropDownList1.SelectedValue + " like '%' + '" + filter + "' +'%'))";
            GridCustomerList.DataSource = null;
            GridCustomerList.DataBind();
            _SQL.getGrid(SQL, GridCustomerList);
            GridCustomerList.Visible = true;
            ViewState["currentTable"] = (DataTable)(GridCustomerList.DataSource);
        }

        protected void GridSupplierList_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(ViewState["currentTable"]);
            {
                string SortDir = string.Empty;
                if (dir == SortDirection.Ascending)
                {
                    dir = SortDirection.Descending;
                    SortDir = "Desc";
                }
                else
                {
                    dir = SortDirection.Ascending;
                    SortDir = "Asc";
                }
                DataView sortedView = new DataView(dt);
                sortedView.Sort = e.SortExpression + " " + SortDir;
                GridCustomerList.DataSource = sortedView;
                GridCustomerList.DataBind();
            }
        }

        public SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }

            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadCustomerList(txtFilter.Text);
        }
    }
}