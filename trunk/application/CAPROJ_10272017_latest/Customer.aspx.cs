﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data;
using System.Data.SqlClient;

namespace CAPROJ
{
    public partial class Customer : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadCustomers();
            }


        }
        public void LoadCustomers()
        {
            _SQL.getDropdown("Select * from md_customer", "customer_id", "customer_name", DropDownList1);
        }
        public void LoadCustomerData()
        {

            //Load Customer
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_customer where Customer_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();

                CustID.Text = dr["Customer_ID"].ToString();
                CustomerID.Text = dr["Customer_ID"].ToString();
                CustomerName.Text = dr["Customer_name"].ToString().Trim();
                Session["customerName"] = dr["Customer_name"].ToString().Trim();
                Type.Text = dr["type"].ToString().Trim();
                Address.Text = dr["address"].ToString().Trim();
                City.Text = dr["city"].ToString().Trim();
                Province.Text = dr["province"].ToString().Trim();
                ZipCode.Text = dr["zipcode"].ToString().Trim();
                Email.Text = dr["email"].ToString().Trim();
                ContactPerson.Text = dr["contact_person"].ToString().Trim();
                Telephone.Text = dr["telephone_no"].ToString().Trim();
                Mobile.Text = dr["mobile_no"].ToString().Trim();

                //Disable the controls
                CustomerName.ReadOnly = true;
                Type.ReadOnly = true;
                Address.ReadOnly = true;
                City.ReadOnly = true;
                Province.ReadOnly = true;
                ZipCode.ReadOnly = true;
                Email.ReadOnly = true;
                ContactPerson.ReadOnly = true;
                Telephone.ReadOnly = true;
                Mobile.ReadOnly = true;

                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();
            //Load Customer Payment
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr1 = _SQL.ExecuteReader("select * from md_customer_payment where Customer_ID=@value");
            if (dr1.HasRows)
            {
                dr1.Read();
                PaymentOption.Text = dr1["payment_option"].ToString();
                PaymentTerms.Text = dr1["payment_terms"].ToString().Trim();
                Discount.Text = dr1["discount"].ToString().Trim();

                //Disable the controls
                PaymentOption.ReadOnly = true;
                PaymentTerms.ReadOnly = true;
                Discount.ReadOnly = true;
            }
            _SQL.Close();

          
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCustomerData();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            CustomerName.ReadOnly = false;
            Type.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            ContactPerson.ReadOnly = false;
            Telephone.ReadOnly = false;
            Mobile.ReadOnly = false;
            PaymentOption.ReadOnly = false;
            PaymentTerms.ReadOnly = false;
            Discount.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                VerifyCustomerIfExisting();
                if (Session["isexisting"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Customer Already Exists!');", true);
                }
                else
                {
                    _SQL.Open();
                    //Customer data
                    _SQL.AddParameters("Customer_ID", CustomerID.Text.Trim());
                    _SQL.AddParameters("Customer_name", CustomerName.Text.Trim());
                    _SQL.AddParameters("type", Type.Text.Trim());
                    _SQL.AddParameters("address", Address.Text.Trim());
                    _SQL.AddParameters("city", City.Text.Trim());
                    _SQL.AddParameters("province", Province.Text.Trim());
                    _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                    _SQL.AddParameters("email", Email.Text.Trim());
                    _SQL.AddParameters("contact_person", ContactPerson.Text.Trim());
                    _SQL.AddParameters("telephone", Telephone.Text.Trim());
                    _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                    string query = @"INSERT INTO md_Customer(Customer_name,type,address,city,province,zipcode,email,contact_person,telephone_no,mobile_no)
                                       VALUES(@Customer_name,@type,@address,@city,@province,@zipcode,@email,@contact_person,@telephone,@mobileno);";

                    //Customer payment
                    _SQL.AddParameters("payment_option", PaymentOption.Text.Trim());
                    _SQL.AddParameters("payment_terms", PaymentTerms.Text.Trim());
                    _SQL.AddParameters("discount", Discount.Text.Trim());
                    query = query + @"INSERT INTO md_Customer_payment(payment_option,payment_terms,discount)
                                  VALUES(@payment_option,@payment_terms,@discount);";

                    _SQL.ExecuteNonQuery(query);
                    _SQL.Close();


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Customer Successfully Added!'); window.location='" + Request.ApplicationPath + "CustomerList.aspx';", true);
                }
              
            }
            else
            {
                VerifyCustomerIfExistingForEdit();
                if (Session["isexistingforedit"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Customer Already Exists!');", true);
                }
                else
                {
                    _SQL.Open();
                    _SQL.AddParameters("Customer_ID", CustomerID.Text.Trim());
                    _SQL.AddParameters("Customer_name", CustomerName.Text.Trim());
                    _SQL.AddParameters("type", Type.Text.Trim());
                    _SQL.AddParameters("address", Address.Text.Trim());
                    _SQL.AddParameters("city", City.Text.Trim());
                    _SQL.AddParameters("province", Province.Text.Trim());
                    _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                    _SQL.AddParameters("email", Email.Text.Trim());
                    _SQL.AddParameters("contact_person", ContactPerson.Text.Trim());
                    _SQL.AddParameters("telephone", Telephone.Text.Trim());
                    _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                    _SQL.ExecuteNonQuery(@"UPDATE md_Customer SET Customer_name=@Customer_name,type=@type,
                                           address=@address,city=@city,province=@province,zipcode=@zipcode,email=@email,contact_person=@contact_person,
                                           telephone_no=@telephone,mobile_no=@mobileno WHERE Customer_ID=@Customer_ID");
                    _SQL.Close();

                    //Customer payment
                    _SQL.Open();
                    _SQL.AddParameters("Customer_ID", CustomerID.Text.Trim());
                    _SQL.AddParameters("payment_option", PaymentOption.Text.Trim());
                    _SQL.AddParameters("payment_terms", PaymentTerms.Text.Trim());
                    _SQL.AddParameters("discount", Discount.Text.Trim());
                    _SQL.ExecuteNonQuery(@"UPDATE md_Customer_payment SET payment_option=@payment_option,payment_terms=@payment_terms,
                                           discount=@discount WHERE Customer_ID=@Customer_ID");
                    _SQL.Close();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Customer Successfully Updated!'); window.location='" + Request.ApplicationPath + "Customer.aspx';", true);
            }
              
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_Customer WHERE Customer_ID='" + CustomerID.Text + "';";
            query = query + "DELETE FROM md_Customer_payment WHERE Customer_ID='" + CustomerID.Text + "';";
            query = query + "DELETE FROM md_Customer_products WHERE Customer_ID = '" + CustomerID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Customer Successfully Deleted!'); window.location='" + Request.ApplicationPath + "Customer.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Customer.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            CustomerID.Text = "";
            CustomerName.Text = "";
            Type.Text = "";
            Address.Text = "";
            City.Text = "";
            Province.Text = "";
            ZipCode.Text = "";
            Email.Text = "";
            ContactPerson.Text = "";
            Telephone.Text = "";
            Mobile.Text = "";
            PaymentOption.Text = "";
            PaymentTerms.Text = "";
            Discount.Text = "";

            //Enable the controls
            CustomerName.ReadOnly = false; 
           Type.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            ContactPerson.ReadOnly = false;
            Telephone.ReadOnly = false;
            Mobile.ReadOnly = false;
            PaymentOption.ReadOnly = false;
            PaymentTerms.ReadOnly = false;
            Discount.ReadOnly = false;
            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }
        public void VerifyCustomerIfExisting()
        {
            _SQL.Open();
            Session["isexisting"] = _SQL.ExecuteScalar("Select dbo.VerifyCustomerIfExisting('" + CustomerName.Text + "')");

        }
        public void VerifyCustomerIfExistingForEdit()
        {
            _SQL.Open();
            Session["isexistingforedit"] = _SQL.ExecuteScalar("Select dbo.VerifyCustomerIfExistingForEdit('" + CustomerName.Text + "','" + Session["customerName"].ToString().Trim() + "')");

        }
    }

}