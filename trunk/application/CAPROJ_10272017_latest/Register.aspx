﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="CAPROJ.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="SHORTCUT ICON" href="images/favicon.ico"/>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="CssLogin/style.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/jQuery-2.2.0.min.js" type="text/javascript"></script>
     <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/bootstrap.js" type="text/javascript"></script>
        <script src="JScript/bootstrap.min.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JQuery-v1.4.4.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <link href="bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet"/>
</head>
    <body class="bg-white" style="background-color:white">
   <center>
      <div class="box" style="width: 600px; height:750px; align-content: center" >
    
         <center style="padding-top:20px">
            <img src="images/uk_logo.jpg" width="50%" height="50%"/>
            <br />
            <font color="#737373" size="10px" style="text-shadow: 0px 2px 2px gray">Admin Registration</font>
         </center>
         <div>
            <section class="main" >
               <form id="RegForm" class="form-1" runat="server">
                  <div id="fldFName" runat="server">
                     <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                     <p class="field">
                        <asp:TextBox ID="FirstName1" runat="server" placeholder ="First Name"></asp:TextBox>
                        <i class="icon-edit icon-large"></i>
                     </p>
                  </div>
                  <div>
                     <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                     <br />
                     <div id="fldLName" runat="server">
                        <p class="field">
                           <asp:TextBox ID="LastName1" runat="server" placeholder ="Last Name"></asp:TextBox>
                           <i class="icon-edit icon-large"></i>
                        </p>
                     </div>
                     <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                     <br />
                     <div id="fldDepartment" runat="server">
                        <p class="field">
                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                <asp:ListItem Value="Department">Department</asp:ListItem>
                                <asp:ListItem Value="Purchasing">Purchasing</asp:ListItem>
                                <asp:ListItem Value="Sales">Sales</asp:ListItem>
                                <asp:ListItem Value="Inventory">Inventory</asp:ListItem>
                                <asp:ListItem Value="Warehouse">Warehouse</asp:ListItem>
                                <asp:ListItem Value="Production">Production</asp:ListItem>
                                <asp:ListItem Value="Accounting">Accounting</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                     </div>
                     <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
                     <br />
                     <div id="fldUser" runat="server">
                        <p class="field">
                           <asp:TextBox ID="UserName1" runat="server" placeholder ="Username"></asp:TextBox>
                           <i class="icon-user icon-large"></i>
                        </p>
                     </div>
                     <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                     <br />
                     <div id="fldPassword" runat="server">
                        <p class="field">
                           <asp:TextBox ID="Password1" runat="server" placeholder ="Password" TextMode="Password"></asp:TextBox>
                           <i class="icon-lock icon-large"></i>
                        </p>
                     </div>
                      <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                     <div id="Div3" runat="server">
                        <p class="field">
                           
                           <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" ></asp:DropDownList>
                        </p>
                     </div>
                     <div>
                        <asp:Button ID="Register1" runat="server" Text="Register" OnClick="Register1_Click" class="btn btn-block btn-danger" BackColor="#b01717"/>
                     </div>
                     <div>
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Already have an account?</asp:LinkButton>
                     </div>
                  </div>
               </form>
            </section>
         </div>
        
      </div>
   </center>
</body>

</html>
