﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Configuration;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.Security;
//using System.Web.UI.HtmlControls;
//using System.Data.SqlClient;
//using CommonLib;
//using CAPROJ.Modules;
//using System.IO;
//using System.Text;

//namespace CAPROJ
//{
//    public partial class GoodsReturn : System.Web.UI.Page
//    {
//        SQLProcedures _SQL = new SQLProcedures();
//        PublicProperties PublicProperty = new PublicProperties();
//        DataTable dtCurrentTable;

//        protected void Page_Load(object sender, EventArgs e)
//        {
//            if (!Page.IsPostBack)
//            {
//                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
//                {
//                    Response.Redirect("AccessDenied.aspx");
//                }
//                //lahat ng active PO's, iloload natin sa dropPO
//                _SQL.getDropdown("select PurchaseOrder_ID from md_purchase_order_header where status='active' and PurchaseOrder_ID not in(select GoodsReturn_ID from md_goods_return_detail)", "GoodsReturn_ID", "GoodsReturn_ID", dropPO);
//                //add css lang
//                dropPO.CssClass = "form-control";
//                txtSupplierName.CssClass = "form-control";
//                txtRemarks.CssClass = "form-control";
//                //kung more than 1 ang PO, load natin ang details nung PO
//                if (dropPO.Items.Count > 0)
//                {
//                    LoadPurchaseOrderDetails();
//                }


//                //    //lahat ng active PO's, iloload natin sa dropPO
//                //    _SQL.getDropdown("select PurchaseOrder_ID from md_purchase_order_header where status ='active'", "PurchaseOrder_ID", "PurchaseOrder_ID", dropPO);
//                //    //add css lang
//                //    dropPO.CssClass = "form-control";
//                //    txtSupplierName.CssClass = "form-control";
//                //    txtRemarks.CssClass = "form-control";
//                //    //kung more than 1 ang PO, load natin ang details nung PO
//                //    if (dropPO.Items.Count > 0)
//                //    {
//                //        LoadPurchaseOrderDetails();
//                //    }
//                //}

//            }
//        }
//        //eto yung nagloload ng PO details dun sa gridview
//        public void LoadPurchaseOrderDetails()
//        {
//            //kinuha lang natin kung ano yung selected na item sa dropPO
//            string PurchaseOrder_ID = dropPO.SelectedValue.ToString().Trim();

//            //load header
//            //display lang ng supplier name, delivery date & date order
//            _SQL.Open();
//            _SQL.AddParameters("purchase_id", PurchaseOrder_ID);
//            SqlDataReader dr = _SQL.ExecuteReader("select a.*, b.supplier_name,c.date_received from md_purchase_order_header a inner join md_supplier b on a.Supplier_ID = b.Supplier_ID inner join md_goods_receipt_header c on a.PurchaseOrder_ID = c.PurchaseOrder_ID where a.PurchaseOrder_ID=@purchase_id");
//            if (dr.HasRows)
//            {
//                dr.Read();
//                txtSupplierName.Text = dr["supplier_name"].ToString().Trim();
//                //format lang yung date 
//                DateTime date_order = Convert.ToDateTime(dr["date_order"].ToString().Trim());
//                txtDateOrder.Text = date_order.ToString("MM/dd/yyyy");
//                DateTime date_received = Convert.ToDateTime(dr["date_received"].ToString().Trim());
//                txtDateReceived.Text = date_received.ToString("MM/dd/yyyy");
//                txtRemarks.Text = dr["remarks"].ToString().Trim();
//            }
//            _SQL.Close();

//            //load details
//            //display list of items on gridview based on the selected PO
//            string SQL;
//            SQL = "select * from view_po_for_goods_return where PurchaseOrder_ID ='" + PurchaseOrder_ID + "'";
//            GridGoodsReturn.DataSource = null;
//            GridGoodsReturn.DataBind();
//            _SQL.getGrid(SQL, GridGoodsReturn);
//            DataTable dt = (DataTable)GridGoodsReturn.DataSource;
//            if (dt != null)
//            {
//                DataTable dt1 = new DataTable();
//                DataRow dr1 = null;
//                //column names
//                dt1.Columns.Add(new DataColumn("item_name", typeof(string)));
//                dt1.Columns.Add(new DataColumn("quantity", typeof(int)));
//                dt1.Columns.Add(new DataColumn("unit_price", typeof(double)));
//                dt1.Columns.Add(new DataColumn("discount", typeof(int)));
//                dt1.Columns.Add(new DataColumn("tax", typeof(int)));
//                dt1.Columns.Add(new DataColumn("total", typeof(double)));
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    //Insert the rows on Datatable
//                    //add values
//                    dr1 = dt1.NewRow();
//                    dr1["item_name"] = dt.Rows[i]["product_name"].ToString().Trim();
//                    dr1["quantity"] = dt.Rows[i]["quantity"].ToString().Trim();
//                    dr1["unit_price"] = dt.Rows[i]["unit_price"].ToString().Trim();
//                    dr1["discount"] = dt.Rows[i]["discount"].ToString().Trim();
//                    dr1["tax"] = dt.Rows[i]["tax"].ToString().Trim();
//                    dr1["total"] = dt.Rows[i]["total"].ToString().Trim();
//                    dt1.Rows.Add(dr1);

//                    ViewState["CurrentTable"] = dt1;
//                    GridGoodsReturn.DataSource = dt1;
//                    GridGoodsReturn.DataBind();
//                }
//                //display the data on the gridview
//                DisplayTheData();
//            }
//        }

//        private void DisplayTheData()
//        {
//            int rowIndex = 0;
//            if (ViewState["CurrentTable"] != null)
//            {
//                DataTable dt = (DataTable)ViewState["CurrentTable"];
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        TextBox box1 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[0].FindControl("item_name");
//                        TextBox box2 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[1].FindControl("quantity");
//                        TextBox box3 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[2].FindControl("unit_price");
//                        TextBox box4 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[3].FindControl("discount");
//                        TextBox box5 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[4].FindControl("tax");
//                        TextBox box6 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[5].FindControl("total");

//                        box1.Text = dt.Rows[i]["item_name"].ToString();
//                        box1.ReadOnly = true;
//                        box2.Text = dt.Rows[i]["quantity"].ToString();
//                        double unit_price = Convert.ToDouble(dt.Rows[i]["unit_price"].ToString());
//                        box3.Text = unit_price.ToString("N");
//                        box3.ReadOnly = true;
//                        box4.Text = dt.Rows[i]["discount"].ToString();
//                        box4.ReadOnly = true;
//                        box5.Text = dt.Rows[i]["tax"].ToString();
//                        box5.ReadOnly = true;
//                        double total = Convert.ToDouble(dt.Rows[i]["total"].ToString());
//                        box6.Text = total.ToString("N");
//                        box6.ReadOnly = true;
//                        rowIndex++;
//                    }
//                }

//                //Display total footer
//                //add total amount on footer
//                //compute total amount on the footer
//                TextBox txtTotalAmountDue = (TextBox)GridGoodsReturn.FooterRow.FindControl("totalPaymentDue");
//                double totalAmountDue = 0;
//                for (int i = 0; i < GridGoodsReturn.Rows.Count; i++)
//                {
//                    TextBox amount = (TextBox)GridGoodsReturn.Rows[i].Cells[5].FindControl("total");
//                    totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
//                }
//                txtTotalAmountDue.Enabled = false;
//                txtTotalAmountDue.CssClass = "form-control";
//                txtTotalAmountDue.Text = totalAmountDue.ToString("N");
//            }
//        }

//        //everytime na magpapalit ka ng selection sa PO, iloload niya yung function na to
//        protected void dropPO_SelectedIndexChanged1(object sender, EventArgs e)
//        {
//            LoadPurchaseOrderDetails();
//        }

//        //everytime na magchachanged yung value ng quantity, irurun niya yung function na to
//        protected void quantity_TextChanged(object sender, EventArgs e)
//        {
//            //kinukuha lang natin kung ano yung row na nagchanged ang value
//            TextBox txtQuantity = (TextBox)sender;
//            GridViewRow grdrDropDownRow = ((GridViewRow)txtQuantity.Parent.Parent);
//            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");

//            //start computation
//            //kinukuha yung value ng discount
//            TextBox txtDiscount = (TextBox)grdrDropDownRow.FindControl("discount");
//            TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
//            //compute total amount
//            double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
//            txtTotal.Text = total.ToString("N");

//            //add total amount on footer
//            //display total amoun on footer
//            TextBox txtTotalAmountDue = (TextBox)GridGoodsReturn.FooterRow.FindControl("totalPaymentDue");
//            double totalAmountDue = 0;
//            for (int i = 0; i < GridGoodsReturn.Rows.Count; i++)
//            {
//                TextBox amount = (TextBox)GridGoodsReturn.Rows[i].Cells[5].FindControl("total");
//                totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
//            }
//            txtTotalAmountDue.Text = totalAmountDue.ToString("N");
//        }

//        //same computation kagaya nung sa PO
//        public double ComputeTotalPerRow(double quantity, double price, double discountPercentage)
//        {
//            double total = 0;
//            double origPrice = 0;
//            double discountAmount = 0;
//            double discountedPrice = 0;
//            double taxAmount = 0;

//            origPrice = quantity * price;
//            discountAmount = origPrice * (discountPercentage / 100);
//            discountedPrice = origPrice - discountAmount;
//            taxAmount = discountedPrice * .12;
//            total = discountedPrice + taxAmount;

//            return total;
//        }

//        protected void btnSave_Click(object sender, EventArgs e)
//        {
//            //kinukuha lang dito yung values dun sa gridview
//            UpdateDataBeforeSaving();

//            //insert header
//            //tapos dito, ang naiba lang is yung table, before: md_goods_receipt ngayon: md_goods_return
//            _SQL.Open();
//            _SQL.AddParameters("po_id", dropPO.SelectedValue.ToString().Trim());
//            _SQL.AddParameters("date_received", txtDateReceived.Text);
//            _SQL.ExecuteNonQuery("insert into md_goods_return_header(PurchaseOrder_ID,date_received)values(@po_id,@date_received)");
//            _SQL.Close();

//            //insert details
//            //same with the details, md_goods_receipt_detail naging md_goods_return_detail
//            DataTable dt = (DataTable)ViewState["CurrentTable"];
//            string query = "";

//            foreach (DataRow row in dt.Rows)
//            {
//                if (row["quantity"].ToString() != "0")
//                {
//                    //dito nag iinsert sa goods_return_detail
//                    query = query + "INSERT INTO md_goods_return_detail(GoodsReturn_ID,product_name,quantity)VALUES((select max(GoodsReturn_ID) from md_goods_return_header),'" + row["item_name"].ToString() + "','" + row["quantity"].ToString() + "');";
//                    //insert spillage
//                    //dito naman, since nireturn siya, dapat marecord siya sa Spillage sa Raw Materials
//                    query = query + "INSERT INTO md_spillage_raw_materials(Material_ID,quantity,date)VALUES((select Material_ID from md_raw_materials where material_name='" + row["item_name"].ToString() + "'),'" + row["quantity"].ToString() + "','" + Convert.ToDateTime(txtReturn.Text) + "');";
//                }

//                //insert/update raw materials
//                //and then dito, magmaminus tayo sa inventory ng Raw Materials since nireturn nga siya so dapat mabawasan ang inventory natin
//                _SQL.Open();
//                SqlDataReader dr = _SQL.ExecuteReader("select cnt=isnull(count(Material_ID),0) from md_raw_materials where material_name='" + row["item_name"].ToString() + "'");
//                if (dr.HasRows)
//                {
//                    dr.Read();
//                    int count = 0;
//                    count = Convert.ToInt32(dr[0].ToString());
//                    if (count == 0)
//                    {
//                        query = query + "INSERT INTO md_raw_materials(material_name,category)VALUES('" + row["item_name"].ToString() + "','');";
//                        query = query + "INSERT INTO md_raw_materials_list(Material_ID,onhand_quantity,max_quantity)VALUES((select max(Material_ID) from md_raw_materials),'" + row["quantity"].ToString() + "',0);";
//                    }
//                    else
//                    {
//                        //total quantity = onhandquantity - inputted quantity on the textbox(quantity)
//                        query = query + "UPDATE md_raw_materials_list SET onhand_quantity=onhand_quantity - '" + row["quantity"].ToString() + "' where Material_ID=(select Material_ID from md_raw_materials where material_name='" + row["item_name"].ToString() + "');";
//                    }
//                }
//                _SQL.Close();
//            }

//            _SQL.Open();
//            _SQL.ExecuteNonQuery(query);
//            _SQL.Close();
//            //message box
//            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Goods Return Successfully Submitted!'); window.location='" + Request.ApplicationPath + "GoodsReturn.aspx';", true);

//            //questions? gets po
//            //sige, test natin
//        }

//        public void UpdateDataBeforeSaving()
//        {
//            //Save the updated value of textbox to ViewState["CurrentTable"]
//            if (GridGoodsReturn.Rows.Count > 0)
//            {
//                int rowIndex = 0;
//                if (ViewState["CurrentTable"] != null)
//                {
//                    DataTable dt = (DataTable)ViewState["CurrentTable"];
//                    DataRow drCurrentRow = null;
//                    DataTable dtNewTable = new DataTable();
//                    dtNewTable.Columns.Add(new DataColumn("item_name", typeof(string)));
//                    dtNewTable.Columns.Add(new DataColumn("quantity", typeof(int)));
//                    dtNewTable.Columns.Add(new DataColumn("unit_price", typeof(double)));
//                    dtNewTable.Columns.Add(new DataColumn("discount", typeof(int)));
//                    dtNewTable.Columns.Add(new DataColumn("tax", typeof(int)));
//                    dtNewTable.Columns.Add(new DataColumn("total", typeof(double)));
//                    if (dt.Rows.Count > 0)
//                    {
//                        for (int i = 1; i <= dt.Rows.Count; i++)
//                        {
//                            TextBox box1 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[0].FindControl("item_name");
//                            TextBox box2 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[1].FindControl("quantity");
//                            TextBox box3 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[2].FindControl("unit_price");
//                            TextBox box4 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[3].FindControl("discount");
//                            TextBox box5 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[4].FindControl("tax");
//                            TextBox box6 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[5].FindControl("total");

//                            drCurrentRow = dtNewTable.NewRow();

//                            drCurrentRow["item_name"] = box1.Text;
//                            drCurrentRow["quantity"] = box2.Text;
//                            drCurrentRow["unit_price"] = box3.Text;
//                            drCurrentRow["discount"] = box4.Text;
//                            drCurrentRow["tax"] = box4.Text;
//                            drCurrentRow["total"] = box4.Text;
//                            dtNewTable.Rows.Add(drCurrentRow);

//                            rowIndex++;
//                        }
//                        ViewState["CurrentTable"] = dtNewTable;
//                    }
//                }
//            }
//        }

//        public void GetPO_Status()
//        {
//            int totalPO = 0;
//            int totalGR = 0;

//            //get total quantity of Goods Receipt
//            _SQL.Open();
//            SqlDataReader dr = _SQL.ExecuteReader("select totatGR=isnull(sum(a.quantity),0) from md_goods_receipt_detail a inner join md_goods_receipt_header b on a.GoodsReceipt_ID = b.GoodsReceipt_ID where b.PurchaseOrder_ID='" + dropPO.SelectedValue.ToString().Trim() + "'");
//            if (dr.HasRows)
//            {
//                dr.Read();
//                totalGR = Convert.ToInt32(dr[0].ToString());
//            }
//            _SQL.Close();

//            //get total quantity of Purchase Order
//            _SQL.Open();
//            string query = "select totatPO=isnull(sum(quantity),0) from md_purchase_order_detail where PurchaseOrder_ID='" + dropPO.SelectedValue.ToString().Trim() + "'";
//            SqlDataReader dr1 = _SQL.ExecuteReader(query);
//            if (dr1.HasRows)
//            {
//                dr1.Read();
//                totalPO = Convert.ToInt32(dr1[0].ToString());
//            }
//            _SQL.Close();

//            //Compare
//            if (totalPO != totalGR)
//            {
//                Session["status"] = "active";
//            }
//            else
//            {
//                Session["status"] = "delivered";
//            }
//        }

//        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            LoadPurchaseOrderDetails();
//        }

//        protected void btnCancel_Click(object sender, EventArgs e)
//        {
//            Response.Redirect("GoodsReceipt.aspx");
//        }
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class GoodsReturn : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                //lahat ng active PO's, iloload natin sa dropPO
                _SQL.getDropdown("select PurchaseOrder_ID from md_purchase_order_header where status='delivered'", "PurchaseOrder_ID", "PurchaseOrder_ID", dropPO);
                //add css lang
                dropPO.CssClass = "form-control";
                txtSupplierName.CssClass = "form-control";
                txtRemarks.CssClass = "form-control";
                //kung more than 1 ang PO, load natin ang details nung PO
                if (dropPO.Items.Count > 0)
                {
                    LoadPurchaseOrderDetails();
                }
            }

        }
        //eto yung nagloload ng PO details dun sa gridview
        public void LoadPurchaseOrderDetails()
        {
            //kinuha lang natin kung ano yung selected na item sa dropPO
            string PurchaseOrder_ID = dropPO.SelectedValue.ToString().Trim();

            //load header
            //display lang ng supplier name, delivery date & date order
            _SQL.Open();
            _SQL.AddParameters("purchase_id", PurchaseOrder_ID);
            SqlDataReader dr = _SQL.ExecuteReader("select a.*, b.supplier_name,c.date_received from md_purchase_order_header a inner join md_supplier b on a.Supplier_ID = b.Supplier_ID inner join md_goods_receipt_header c on a.PurchaseOrder_ID = c.PurchaseOrder_ID where a.PurchaseOrder_ID=@purchase_id");
            if (dr.HasRows)
            {
                dr.Read();
                txtSupplierName.Text = dr["supplier_name"].ToString().Trim();
                //format lang yung date 
                DateTime date_order = Convert.ToDateTime(dr["date_order"].ToString().Trim());
                txtDateOrder.Text = date_order.ToString("MM/dd/yyyy");
                DateTime date_received = Convert.ToDateTime(dr["date_received"].ToString().Trim());
                txtDateReceived.Text = date_received.ToString("MM/dd/yyyy");
                txtRemarks.Text = dr["remarks"].ToString().Trim();
            }
            _SQL.Close();

            //load details
            //display list of items on gridview based on the selected PO
            string SQL;
            SQL = "select * from view_po_for_goods_return where PurchaseOrder_ID ='" + PurchaseOrder_ID + "'";
            GridGoodsReturn.DataSource = null;
            GridGoodsReturn.DataBind();
            _SQL.getGrid(SQL, GridGoodsReturn);
            DataTable dt = (DataTable)GridGoodsReturn.DataSource;
            if (dt != null)
            {
                DataTable dt1 = new DataTable();
                DataRow dr1 = null;
                //column names
                dt1.Columns.Add(new DataColumn("item_name", typeof(string)));
                dt1.Columns.Add(new DataColumn("quantity", typeof(int)));
                dt1.Columns.Add(new DataColumn("unit_price", typeof(double)));
                dt1.Columns.Add(new DataColumn("discount", typeof(int)));
                dt1.Columns.Add(new DataColumn("tax", typeof(int)));
                dt1.Columns.Add(new DataColumn("total", typeof(double)));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Insert the rows on Datatable
                    //add values
                    dr1 = dt1.NewRow();
                    dr1["item_name"] = dt.Rows[i]["product_name"].ToString().Trim();
                    dr1["quantity"] = dt.Rows[i]["quantity"].ToString().Trim();
                    dr1["unit_price"] = dt.Rows[i]["unit_price"].ToString().Trim();
                    dr1["discount"] = dt.Rows[i]["discount"].ToString().Trim();
                    dr1["tax"] = dt.Rows[i]["tax"].ToString().Trim();
                    dr1["total"] = dt.Rows[i]["total"].ToString().Trim();
                    dt1.Rows.Add(dr1);

                    ViewState["CurrentTable"] = dt1;
                    GridGoodsReturn.DataSource = dt1;
                    GridGoodsReturn.DataBind();
                }
                //display the data on the gridview
                DisplayTheData();
            }
        }

        private void DisplayTheData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[0].FindControl("item_name");
                        TextBox box2 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[1].FindControl("quantity");
                        TextBox box3 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[2].FindControl("unit_price");
                        TextBox box4 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[3].FindControl("discount");
                        TextBox box5 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[4].FindControl("tax");
                        TextBox box6 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[5].FindControl("total");

                        box1.Text = dt.Rows[i]["item_name"].ToString();
                        box1.ReadOnly = true;
                        box2.Text = dt.Rows[i]["quantity"].ToString();
                        double unit_price = Convert.ToDouble(dt.Rows[i]["unit_price"].ToString());
                        box3.Text = unit_price.ToString("N");
                        box3.ReadOnly = true;
                        box4.Text = dt.Rows[i]["discount"].ToString();
                        box4.ReadOnly = true;
                        box5.Text = dt.Rows[i]["tax"].ToString();
                        box5.ReadOnly = true;
                        double total = Convert.ToDouble(dt.Rows[i]["total"].ToString());
                        box6.Text = total.ToString("N");
                        box6.ReadOnly = true;
                        rowIndex++;
                    }
                }

                //Display total footer
                //add total amount on footer
                //compute total amount on the footer
                TextBox txtTotalAmountDue = (TextBox)GridGoodsReturn.FooterRow.FindControl("totalPaymentDue");
                double totalAmountDue = 0;
                for (int i = 0; i < GridGoodsReturn.Rows.Count; i++)
                {
                    TextBox amount = (TextBox)GridGoodsReturn.Rows[i].Cells[5].FindControl("total");
                    totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
                }
                txtTotalAmountDue.Enabled = false;
                txtTotalAmountDue.CssClass = "form-control";
                txtTotalAmountDue.Text = totalAmountDue.ToString("N");
            }
        }

        //everytime na magpapalit ka ng selection sa PO, iloload niya yung function na to
        protected void dropPO_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadPurchaseOrderDetails();
        }

        //everytime na magchachanged yung value ng quantity, irurun niya yung function na to
        protected void quantity_TextChanged(object sender, EventArgs e)
        {
            //kinukuha lang natin kung ano yung row na nagchanged ang value
            TextBox txtQuantity = (TextBox)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)txtQuantity.Parent.Parent);
            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");

            //start computation
            //kinukuha yung value ng discount
            TextBox txtDiscount = (TextBox)grdrDropDownRow.FindControl("discount");
            TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
            //compute total amount
            double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
            txtTotal.Text = total.ToString("N");

            //add total amount on footer
            //display total amoun on footer
            TextBox txtTotalAmountDue = (TextBox)GridGoodsReturn.FooterRow.FindControl("totalPaymentDue");
            double totalAmountDue = 0;
            for (int i = 0; i < GridGoodsReturn.Rows.Count; i++)
            {
                TextBox amount = (TextBox)GridGoodsReturn.Rows[i].Cells[5].FindControl("total");
                totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
            }
            txtTotalAmountDue.Text = totalAmountDue.ToString("N");
        }

        //same computation kagaya nung sa PO
        public double ComputeTotalPerRow(double quantity, double price, double discountPercentage)
        {
            double total = 0;
            double origPrice = 0;
            double discountAmount = 0;
            double discountedPrice = 0;
            double taxAmount = 0;

            origPrice = quantity * price;
            discountAmount = origPrice * (discountPercentage / 100);
            discountedPrice = origPrice - discountAmount;
            taxAmount = discountedPrice * .12;
            total = discountedPrice + taxAmount;

            return total;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //kinukuha lang dito yung values dun sa gridview
            UpdateDataBeforeSaving();

            //insert header
            //tapos dito, ang naiba lang is yung table, before: md_goods_receipt ngayon: md_goods_return
            _SQL.Open();
            _SQL.AddParameters("po_id", dropPO.SelectedValue.ToString().Trim());
            _SQL.AddParameters("date_received", txtDateReceived.Text);
            _SQL.ExecuteNonQuery("insert into md_goods_return_header(PurchaseOrder_ID,date_received)values(@po_id,@date_received)");
            _SQL.Close();

            //insert details
            //same with the details, md_goods_receipt_detail naging md_goods_return_detail
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            string query = "";

            foreach (DataRow row in dt.Rows)
            {
                if (row["quantity"].ToString() != "0")
                {
                    //dito nag iinsert sa goods_return_detail
                    query = query + "INSERT INTO md_goods_return_detail(GoodsReturn_ID,product_name,quantity)VALUES((select max(GoodsReturn_ID) from md_goods_return_header),'" + row["item_name"].ToString() + "','" + row["quantity"].ToString() + "');";
                    //insert spillage
                    //dito naman, since nireturn siya, dapat marecord siya sa Spillage sa Raw Materials
                    query = query + "INSERT INTO md_spillage_raw_materials(Material_ID,quantity,date)VALUES((select Material_ID from md_raw_materials where material_name='" + row["item_name"].ToString() + "'),'" + row["quantity"].ToString() + "','" + Convert.ToDateTime(txtReturn.Text) + "');";
                }

                //insert/update raw materials
                //and then dito, magmaminus tayo sa inventory ng Raw Materials since nireturn nga siya so dapat mabawasan ang inventory natin
                _SQL.Open();
                SqlDataReader dr = _SQL.ExecuteReader("select cnt=isnull(count(Material_ID),0) from md_raw_materials where material_name='" + row["item_name"].ToString() + "'");
                if (dr.HasRows)
                {
                    dr.Read();
                    int count = 0;
                    count = Convert.ToInt32(dr[0].ToString());
                    if (count == 0)
                    {
                        query = query + "INSERT INTO md_raw_materials(material_name,category)VALUES('" + row["item_name"].ToString() + "','');";
                        query = query + "INSERT INTO md_raw_materials_list(Material_ID,onhand_quantity,max_quantity)VALUES((select max(Material_ID) from md_raw_materials),'" + row["quantity"].ToString() + "',0);";
                    }
                    else
                    {
                        //total quantity = onhandquantity - inputted quantity on the textbox(quantity)
                        query = query + "UPDATE md_raw_materials_list SET onhand_quantity=onhand_quantity - '" + row["quantity"].ToString() + "' where Material_ID=(select Material_ID from md_raw_materials where material_name='" + row["item_name"].ToString() + "');";
                    }
                }
                _SQL.Close();
            }

            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            //message box
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Goods Return Successfully Submitted!'); window.location='" + Request.ApplicationPath + "GoodsReturn.aspx';", true);

            //questions? gets po
            //sige, test natin
        }

        public void UpdateDataBeforeSaving()
        {
            //Save the updated value of textbox to ViewState["CurrentTable"]
            if (GridGoodsReturn.Rows.Count > 0)
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    DataTable dtNewTable = new DataTable();
                    dtNewTable.Columns.Add(new DataColumn("item_name", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("quantity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("unit_price", typeof(double)));
                    dtNewTable.Columns.Add(new DataColumn("discount", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("tax", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("total", typeof(double)));
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[0].FindControl("item_name");
                            TextBox box2 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[1].FindControl("quantity");
                            TextBox box3 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[2].FindControl("unit_price");
                            TextBox box4 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[3].FindControl("discount");
                            TextBox box5 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[4].FindControl("tax");
                            TextBox box6 = (TextBox)GridGoodsReturn.Rows[rowIndex].Cells[5].FindControl("total");

                            drCurrentRow = dtNewTable.NewRow();

                            drCurrentRow["item_name"] = box1.Text;
                            drCurrentRow["quantity"] = box2.Text;
                            drCurrentRow["unit_price"] = box3.Text;
                            drCurrentRow["discount"] = box4.Text;
                            drCurrentRow["tax"] = box4.Text;
                            drCurrentRow["total"] = box4.Text;
                            dtNewTable.Rows.Add(drCurrentRow);

                            rowIndex++;
                        }
                        ViewState["CurrentTable"] = dtNewTable;
                    }
                }
            }
        }

        public void GetPO_Status()
        {
            int totalPO = 0;
            int totalGR = 0;

            //get total quantity of Goods Receipt
            _SQL.Open();
            SqlDataReader dr = _SQL.ExecuteReader("select totatGR=isnull(sum(a.quantity),0) from md_goods_receipt_detail a inner join md_goods_receipt_header b on a.GoodsReceipt_ID = b.GoodsReceipt_ID where b.PurchaseOrder_ID='" + dropPO.SelectedValue.ToString().Trim() + "'");
            if (dr.HasRows)
            {
                dr.Read();
                totalGR = Convert.ToInt32(dr[0].ToString());
            }
            _SQL.Close();

            //get total quantity of Purchase Order
            _SQL.Open();
            string query = "select totatPO=isnull(sum(quantity),0) from md_purchase_order_detail where PurchaseOrder_ID='" + dropPO.SelectedValue.ToString().Trim() + "'";
            SqlDataReader dr1 = _SQL.ExecuteReader(query);
            if (dr1.HasRows)
            {
                dr1.Read();
                totalPO = Convert.ToInt32(dr1[0].ToString());
            }
            _SQL.Close();

            //Compare
            if (totalPO != totalGR)
            {
                Session["status"] = "active";
            }
            else
            {
                Session["status"] = "delivered";
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPurchaseOrderDetails();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("GoodsReceiptv2.aspx");
        }
    }
}