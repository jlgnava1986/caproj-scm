﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="UserAccounts.aspx.cs" Inherits="ETIIOnlineServices.UserAccounts" Debug="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                User Accounts
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
            <div class="col-xs-12 no-padding">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="row">
                            <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 10px 15px">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding:10px 15px 10px 15px">
                                <asp:Button ID="btnFilter" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-info pull-right" CommandName="Action"
                                    Text='Search' OnCommand="FilterGridview"/>
                            </div>
                            <div class="pull-right col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:10px 15px 10px 15px">
                                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-success pull-right" CommandName="Action"
                                    Text='Add User' OnCommand="btnAdd_Command"/>
                                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                    Text='Back' OnCommand="btnBack_Command"/>
                            </div>
                        </div>
                        <asp:Panel ID="panelMain" runat="server">
                            <div class="box-body table-responsive no-padding">
                                <asp:GridView ID="Grid" runat="server" AutoGenerateColumns="False"
                                    GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="True" EmptyDataText="No record(s) found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                    OnPageIndexChanging="GridAccounts_PageIndexChanging" PageSize="10" onRowCommand="grdListUsers_RowCommand"
                                    BorderStyle="None" Class="table" AllowSorting="True" ShowFooter="true">
                                    <Columns>
                                            <asp:BoundField DataField="ch_user_id" HeaderText="User ID">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_user_name" HeaderText="Name">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_email_address" HeaderText="Email Address">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_cellphone_no" HeaderText="Phone No.">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="vc_group_name" HeaderText="Group">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="bt_activated" HeaderText="Activated">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="bt_isactive" HeaderText="Active">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info"
                                                        CommandName="edit-something" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Edit User">
                                                        <i class="fa fa-edit"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-danger"
                                                        CommandName="delete-user" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Delete User">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkChangePass" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-success" CommandName="Action"
                                                        OnCommand="lnkChangePassword_Click" ToolTip="Change Password">
                                                        <i class="fa fa-lock"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    </Columns>
                                    
                                    <RowStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Bold="true" />
                                    <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                    <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                    <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                </asp:GridView>
                            </div>

                            <div class="modal" id="modalChangePassword" data-backdrop="" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button id="btnClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Change Password - <asp:Label ID="lblUserID" runat="server" Text=""></asp:Label></h4>
                                        </div>
                                            <div class="modal-body">                                    
                                                <div class="form-group">
                                                    <div class="row" style="padding:10px 10px 10px 10px">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center">
                                                            <h5><asp:Label ID="lblNotif" runat="server" Text=""></asp:Label></h5>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <small>New Password</small>
                                                            <asp:TextBox ID="txtNewPass" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True" TargetControlID="txtNewPass" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" FilterMode="ValidChars" ValidChars="!@#$%^&*()_+-={}[]:;\<>,.?/"></ajaxToolkit:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <small>Reenter New Password</small>
                                                            <asp:TextBox ID="txtReenterNewPass" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True" TargetControlID="txtReenterNewPass" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" FilterMode="ValidChars" ValidChars="!@#$%^&*()_+-={}[]:;\<>,.?/"></ajaxToolkit:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <small><font color="white">_</font></small>
                                                            <asp:Button ID="btnChangePass" runat="server" CausesValidation="False" 
                                                                CssClass="btn btn-flat btn-success btn-block" OnClick="btnChangePass_Click"
                                                                Text='Change Password'/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal" id="modalDelete" data-backdrop="" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button id="btnClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Delete Account</h4>
                                        </div>
                                            <div class="modal-body">                                    
                                                <center><h4 class="modal-title">Are you sure you want to delete this account? <b><asp:Label ID="lblDeleteUser" runat="server" Text=""></asp:Label></b></h4></center>
                                                <br/>
                                                <div class="pull-right col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:Button ID="btnNo" runat="server" CausesValidation="False" 
                                                        CssClass="btn btn-flat btn-danger btn-block" data-dismiss="modal"
                                                        Text='No'/>
                                                </div>
                                                <div class="pull-right col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                                    <asp:Button ID="btnYes" runat="server" CausesValidation="False" OnClick="btnDelete_Click"
                                                        CssClass="btn btn-flat btn-success btn-block"
                                                        Text='Yes'/>
                                                </div>
                                                <br/><br/>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelEditAccount" runat="server">
                            <hr/>
                            <h4 class="modal-title">Update User Account - <asp:Label ID="lblUpdateUser" runat="server" Text=""></asp:Label></h4>
                            <div class="form-group">
                                <div class="row" style="padding:10px 10px 10px 10px">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>User ID</small>
                                        <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Name</small>
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Email Address</small>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Phone No.</small>
                                        <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>User Group</small>
                                        <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                        <small>Activated</small>
                                        <asp:CheckBox ID="chkActivated" runat="server" CssClass="form-control"></asp:CheckBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                        <small>Active</small>
                                        <asp:CheckBox ID="chkActive" runat="server" CssClass="form-control"></asp:CheckBox>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <small><font color="white">_</font></small>
                                        <asp:Button ID="btnUpdate" runat="server" CausesValidation="False" 
                                            CssClass="btn btn-flat btn-success btn-block"
                                            Text='Update' OnCommand="btnUpdate_Command"/>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <script type="text/javascript" src="js/validator/jquery-validate.js"></script>
	                    <script type="text/javascript">
		                    $('form').validate({
			                    onChange : true,
			                    eachValidField : function() {

				                    $(this).closest('div').removeClass('error').addClass('success');
			                    },
			                    eachInvalidField : function() {

				                    $(this).closest('div').removeClass('success').addClass('error');
			                    }
		                    });
	                    </script>
                        <asp:Panel ID="panelAddNewAccount" runat="server">
                            <hr/>
                            <h4 class="modal-title">Add New User</h4>
                            <div class="form-group">
                                <div class="row" style="padding:10px 10px 10px 10px">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>User ID</small>
                                        <asp:TextBox ID="txtNewUserID" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Name</small>
                                        <asp:TextBox ID="txtNewName" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Email Address</small>
                                        <asp:TextBox ID="txtNewEmail" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Phone No.</small>
                                        <asp:TextBox ID="txtNewPhone" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Password</small>
                                        <asp:TextBox ID="txtNewPassword_" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <small>Confirm Password</small>
                                        <asp:TextBox ID="txtConfirmNewPassword_" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                        <small>User Group</small>
                                        <asp:DropDownList ID="ddlNewUserGroup" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlNewUserGroup_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                        <small>Agent ID</small>
                                        <asp:TextBox ID="txtNewEntityID" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                        <small>Activated</small>
                                        <asp:CheckBox ID="chkNewActivated" runat="server" CssClass="form-control"></asp:CheckBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                        <small>Active</small>
                                        <asp:CheckBox ID="chkNewActive" runat="server" CssClass="form-control"></asp:CheckBox>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <small><font color="white">_</font></small>
                                        <asp:Button ID="btnAddUser" runat="server" CausesValidation="False" 
                                            CssClass="btn btn-flat btn-success btn-block"
                                            Text='Add User' OnCommand="btnAddUser_Command"/>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Grid" EventName="RowCommand" />
                            <asp:AsyncPostbackTrigger ControlID="btnChangePass" EventName="Click" />
                            <asp:AsyncPostbackTrigger ControlID="ddlNewUserGroup" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
            </div>
        </section>
    </aside>


    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active').find('li').css({ 'display': 'block' });
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');

<%--            $(document.getElementById("<%=btnChangePass.ClientID%>")).click(function () {
                $('#modalChangePassword').modal('hide');
            });--%>
        });
    </script>

    <script type="text/javascript">
        $(function () {
            "use strict";

            // AREA CHART
            var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666, item2: 2666 },
                        { y: '2011 Q2', item1: 2778, item2: 2294 },
                        { y: '2011 Q3', item1: 4912, item2: 1969 },
                        { y: '2011 Q4', item1: 3767, item2: 3597 },
                        { y: '2012 Q1', item1: 6810, item2: 1914 },
                        { y: '2012 Q2', item1: 5670, item2: 4293 },
                        { y: '2012 Q3', item1: 4820, item2: 3795 },
                        { y: '2012 Q4', item1: 15073, item2: 5967 },
                        { y: '2013 Q1', item1: 10687, item2: 4460 },
                        { y: '2013 Q2', item1: 8432, item2: 5713 }
                    ],
                xkey: 'y',
                ykeys: ['item1', 'item2'],
                labels: ['Item 1', 'Item 2'],
                lineColors: ['#a0d0e0', '#3c8dbc'],
                hideHover: 'auto'
            });

            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666 },
                        { y: '2011 Q2', item1: 2778 },
                        { y: '2011 Q3', item1: 4912 },
                        { y: '2011 Q4', item1: 3767 },
                        { y: '2012 Q1', item1: 6810 },
                        { y: '2012 Q2', item1: 5670 },
                        { y: '2012 Q3', item1: 4820 },
                        { y: '2012 Q4', item1: 15073 },
                        { y: '2013 Q1', item1: 10687 },
                        { y: '2013 Q2', item1: 8432 }
                    ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Item 1'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });

            //DONUT CHART
            var donut = new Morris.Donut({
                element: 'sales-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#0079AA"],
                data: [
                        { label: "Download Sales", value: 12 },
                        { label: "In-Store Sales", value: 30 },
                        { label: "Mail-Order Sales", value: 20 }
                    ],
                hideHover: 'auto'
            });

            //BAR CHART
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                        { y: '2006', a: 100, b: 90 },
                        { y: '2007', a: 75, b: 65 },
                        { y: '2008', a: 50, b: 40 },
                        { y: '2009', a: 75, b: 65 },
                        { y: '2010', a: 50, b: 40 },
                        { y: '2011', a: 75, b: 65 },
                        { y: '2012', a: 100, b: 90 }
                    ],
                barColors: ['#0079AA', '#f56954'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['CPU', 'DISK'],
                hideHover: 'auto'
            });
        });
        </script>
</asp:Content>
