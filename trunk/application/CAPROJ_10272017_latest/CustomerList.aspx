﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="CAPROJ.CustomerList" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Customer Master Data List
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                  <div class="row" style="padding-right:5px;padding-bottom:10px;">
                                      <div class="col-md-3">
                                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="customer_name">Customer Name</asp:ListItem>
                                            <asp:ListItem Value="address">Address</asp:ListItem>
                                            <asp:ListItem Value="contact_no">Contact Number</asp:ListItem>
                                            <asp:ListItem Value="discount">Discount</asp:ListItem>
                                            <asp:ListItem Value="revenue">Revenue</asp:ListItem>
                                            <asp:ListItem Value="past_orders">Past Orders</asp:ListItem>
                                            <asp:ListItem Value="date_pastorders">Date Ordered</asp:ListItem>
                                          </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtFilter" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="FromDate" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="ToDate" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnFilter" Text='Filter' BackColor="#b01717" OnClick="btnFilter_Click"></asp:Button>
                                    </div>
                                </div>
                                <div id="tblCustomerList" class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridCustomerList" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true" OnSorting="GridSupplierList_Sorting"                                        
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="customer_name" HeaderText="Customer Name" SortExpression="customer_name">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="address" HeaderText="Address" SortExpression="address">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="contact_no" HeaderText="Contact No" SortExpression="contact_no">
                                                <HeaderStyle HorizontalAlign="left" Width="100px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="discount" HeaderText="Discount" SortExpression="discount">
                                                <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="revenue" HeaderText="Revenue" SortExpression="revenue">
                                                <HeaderStyle HorizontalAlign="left" Width="100px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="past_orders" HeaderText="Past Orders" SortExpression="past_orders">
                                                <HeaderStyle HorizontalAlign="left" Width="200px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnPrint" Text='Print' OnClientClick="printContent('tblCustomerList')" BackColor="#b01717"></asp:Button>
                            </div>
                        </div>
                    </div>        
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
    <script type="text/javascript">
        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>
</asp:Content>
