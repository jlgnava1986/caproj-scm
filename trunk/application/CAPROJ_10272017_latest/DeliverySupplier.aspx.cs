﻿using CAPROJ.Modules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class DeliverySupplier : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                loaddelivery_supplier();
            }
        }
        public void loaddelivery_supplier()
        {
            string SQL = "select * from dbo.view_delivery_supplier";
            GridSupplierDelivery.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSupplierDelivery.DataSource = dt;
                GridSupplierDelivery.DataBind();
                ViewState["ViewingTable"] = dt;
            }
        }
        protected void GridSupplierDelivery_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(ViewState["ViewingTable"]);
            {
                string SortDir = string.Empty;
                if (dir == System.Web.UI.WebControls.SortDirection.Ascending)
                {
                    dir = System.Web.UI.WebControls.SortDirection.Descending;
                    SortDir = "Desc";
                }
                else
                {
                    dir = System.Web.UI.WebControls.SortDirection.Ascending;
                    SortDir = "Asc";
                }
                DataView sortedView = new DataView(dt);
                sortedView.Sort = e.SortExpression + " " + SortDir;
                GridSupplierDelivery.DataSource = sortedView;
                GridSupplierDelivery.DataBind();
            }
        }

        public System.Web.UI.WebControls.SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = System.Web.UI.WebControls.SortDirection.Ascending;
                }
                return (System.Web.UI.WebControls.SortDirection)ViewState["dirState"];
            }

            set
            {
                ViewState["dirState"] = value;
            }
        }
    }
}