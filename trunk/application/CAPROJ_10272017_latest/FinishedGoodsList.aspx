﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="FinishedGoodsList.aspx.cs" Inherits="CAPROJ.FinishedGoodsList" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Finished Goods List
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                  <asp:GridView ID="GridFGList" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"                                       
                                        BorderStyle="None" Class="table" AllowSorting="True" OnSorting="GridFGList_Sorting">
                                        <Columns>
                                            <asp:BoundField DataField="FinishedGoods_ID" HeaderText="Finished Goods No." SortExpression="FinishedGoods_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="finished_goods_name" HeaderText="Finished Goods Name" SortExpression="finished_goods_name">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="onhand_quantity" HeaderText="On-hand quantity" SortExpression="onhand_quantity">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="max_quantity" HeaderText="Max Quantity" SortExpression="max_quantity">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="lead_time" HeaderText="Lead Time" SortExpression="lead_time">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="reorder_level" HeaderText="Re-order Level" SortExpression="reorder_level">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                    </asp:GridView>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnPrint" Text='Print' OnClientClick="Print()" BackColor="#b01717"></asp:Button>
                            </div>
                        </div>
                    </div>   
                     <div id="dvReport" class="col-md-12" style="display:none;">
                        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                            ToolPanelView="None" PrintMode="Pdf"></CR:CrystalReportViewer>
                    </div>       
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPrint" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
      <%--Source: https://www.aspsnippets.com/Articles/Print-Crystal-Report-on-Client-Side-on-Button-Click-using-JavaScript-in-ASPNet.aspx --%>
    <script type="text/javascript">  
        function Print() {  
            var dvReport = document.getElementById("dvReport");  
            var frame1 = dvReport.getElementsByTagName("iframe")[0];  
            if (navigator.appName.indexOf("Internet Explorer") != -1 || navigator.appVersion.indexOf("Trident") != -1) {  
                frame1.name = frame1.id;  
                window.frames[frame1.id].focus();  
                window.frames[frame1.id].print();  
            } else {  
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;  
                frameDoc.print();  
            }  
        }  
    </script>
</asp:Content>

