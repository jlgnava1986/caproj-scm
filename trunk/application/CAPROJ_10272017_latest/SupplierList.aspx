﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SupplierList.aspx.cs" Inherits="CAPROJ.SupplierList" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Supplier Master Data List
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row" style="padding-right:5px;padding-bottom:10px;">
                                    <div class="col-md-7">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtFilter" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnFilter" Text='Filter' BackColor="#b01717" OnClick="btnFilter_Click"></asp:Button>
                                    </div>
                                </div>
                                <div id="tblSupplierList" class="box-body table-responsive no-padding">
                                    <asp:GridView ID="GridSupplierList" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true" OnSorting="GridSupplierList_Sorting"                                        
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="supplier_name" HeaderText="Supplier Name" SortExpression="supplier_name">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="address" HeaderText="Address" SortExpression="address">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="contact_no" HeaderText="Contact Number" SortExpression="contact_no">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="product_name" HeaderText="Products" SortExpression="product_name">
                                                <HeaderStyle HorizontalAlign="left" Width="200px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="lead_time" HeaderText="Lead Time" SortExpression="lead_time">
                                                <HeaderStyle HorizontalAlign="left" Width="200px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="price" HeaderText="Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}" SortExpression="price">
                                                <HeaderStyle HorizontalAlign="left" Width="100px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="past_orders" HeaderText="Past Orders">
                                                <HeaderStyle HorizontalAlign="left" Width="150px" />
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnPrint" Text='Print' OnClientClick="Print()" BackColor="#b01717"></asp:Button>
                            </div>
                        </div>
                    </div>
                    <div id="dvReport" class="col-md-12" style="display:none;">
                        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                            ToolPanelView="None" PrintMode="Pdf"></CR:CrystalReportViewer>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPrint" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>

    <%--Source: https://www.aspsnippets.com/Articles/Print-Crystal-Report-on-Client-Side-on-Button-Click-using-JavaScript-in-ASPNet.aspx --%>
    <script type="text/javascript">  
        function Print() {  
            var dvReport = document.getElementById("dvReport");  
            var frame1 = dvReport.getElementsByTagName("iframe")[0];  
            if (navigator.appName.indexOf("Internet Explorer") != -1 || navigator.appVersion.indexOf("Trident") != -1) {  
                frame1.name = frame1.id;  
                window.frames[frame1.id].focus();  
                window.frames[frame1.id].print();  
            } else {  
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;  
                frameDoc.print();  
            }  
        }  
    </script>  
</asp:Content>
