﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ETIIOnlineServices.Default" Debug="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ETII - Sellers Portal</title>
    <link rel="SHORTCUT ICON" href="images/favicon.ico"/>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="CssLogin/style.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
<%--        <script src="JScript/bootstrap.js" type="text/javascript"></script>
        <script src="JScript/bootstrap.min.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JQuery-v1.4.4.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <link href="bootstrap-3.2.0-dist/css/bootstrap.css" rel=Stylesheet>--%>
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/jQuery-2.2.0.min.js" type="text/javascript"></script>
</head>
<body style="overflow:hidden">
    <div class="wrapper"   >
           
            <center style="padding-top:20px">
                <img src="images/etii-logo-low-res.png" width="216px" height="216px">
                <br/><br/><br/>
                <font color="#545454" size="10px" style="text-shadow: 0px 2px 2px gray">Sellers Portal</font>
            </center>

                <section class="main" >
                    <form id="Form1" class="form-1" runat="server">
                    <div  id="fldUser" runat="server">
				        <p class="field">
					        <asp:TextBox runat="server" ID="txtUser" placeholder="Username" />
					        <i class="icon-user icon-large"></i>
				        </p>
                    </div>
                    <div  id="fldPass" runat="server">
				        <p class="field">
					        <asp:TextBox runat="server" ID="txtPass" TextMode="Password" placeholder="Password" />
					        <i class="icon-lock icon-large"></i>
				        </p>
                    </div>
                    <div  id="fldSubmit" runat="server">
				        <p class="submit">
                            <button runat="server" type="submit" onserverclick="submit_Click" ID="btnLogin"><i class="icon-arrow-right icon-large"></i></button>
				        </p>
                    </div>
                    <center>
                        <asp:Button runat="server" class="btn btn-block btn-primary" ID="btnForgotPass" 
                            Text='Forgot Password ?' onclick="btnForgotPass_Click1"></asp:Button>
                        <asp:Label ID="lblNotification" runat="server" Text="" ForeColor="#0096ff" style="font-size:large;"></asp:Label>
                    </center>
                    <div class="input-group">
                        <asp:TextBox ID="txtUserID" runat="server" class="form-control" placeholder="User ID" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtEmailAdd" runat="server" class="form-control" placeholder="Email Address" Visible="false"></asp:TextBox>
                        <asp:Button ID="btnReset" runat="server" CausesValidation="False"
                                        CssClass="btn btn-primary pull-left" CommandName="Action"
                                        Text='Reset Password' onclick="btnReset_Click" Visible="false"/>
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                        CssClass="btn btn-danger pull-right" CommandName="Action"
                                        Text='Cancel' Visible="false" onclick="btnCancel_Click"/>
                    </div>
                </form>
                </section>
            <br/><br/>
            <div class="push"></div>
    </div>
    <div class="footer">
        <div style="background:rgb(ff,0,0); background:rgba(0,0,0,.50); padding-top:5px; padding-bottom:5px;">
            <center>
                <p><font color="white">Copyright © Euro Towers International, Inc.</font><br/>
                <font color="white">All Rights Reserved.</font>
                <font color="white">Version 1.0</font>
                </p>
            </center>
        </div>
    </div>
</body>
</html>
