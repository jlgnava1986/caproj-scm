﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SpillageFinishedGoods.aspx.cs" Inherits="CAPROJ.SpillageFG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Spillage - Finished Goods
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                 <!-- MAIN CONTENT -->
                                <%--GRIDVIEW VIEWING--%>
                                <div id="tblSpillageRM" class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridSpillageFG" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="false"                                        
                                                BorderStyle="None" Class="table" AllowSorting="True">
                                                <Columns>
                                                    <asp:BoundField DataField="FinishedGoods_ID" HeaderText="Finished Goods #" SortExpression="FinishedGoods_ID">
                                                        <HeaderStyle HorizontalAlign="Left" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="finished_goods_name" HeaderText="Finished Goods Name" SortExpression="finished_goods_name">
                                                        <HeaderStyle HorizontalAlign="left" Width="150px" />
                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="quantity" HeaderText="Quantity" SortExpression="quantity">
                                                        <HeaderStyle HorizontalAlign="left" Width="50px" />
                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
                                                        <HeaderStyle HorizontalAlign="left" Width="50px" />
                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="#CCCC99" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>


                                <%--END GRIDVIEW VIEWING--%>
                            </div>
                        </div>
                    </div>        
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>

    </asp:Content>