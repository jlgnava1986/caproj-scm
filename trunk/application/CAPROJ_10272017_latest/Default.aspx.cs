﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data.SqlClient;



namespace CAPROJ
{
    public partial class Default : System.Web.UI.Page    {
        SQLProcedures _SQL = new SQLProcedures();

        PublicProperties PublicProperty = new PublicProperties();
        WebFunctions.WebFunctions func = new WebFunctions.WebFunctions();


        protected void Page_Load(object sender, EventArgs e)
        {
            lblNotification.Text = PublicProperty.validMessage;
        }
        protected void Login1_Click(object sender, EventArgs e)
        {
            Label2.Text = "";
            Label3.Text = "";
            if (Username.Text == "")
            {
                Label2.Text = "Username is required to login";
            }
            if (Password.Text == "")
            {
                Label3.Text = "Password is required to login";
            }
            if (Username.Text != "" && Password.Text != "")
            {
                _SQL.Open();
                _SQL.AddParameters("userID", Username.Text);
                _SQL.AddParameters("Password", func.EncryptData(Password.Text,"", "", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256));
                SqlDataReader dr = _SQL.ExecuteReader("select a.ch_group_access,b.vc_default_page,a.username,a.password,a.first_name,a.last_name,a.department from md_administration a inner join md_menu_group b on a.ch_group_access=b.ch_group_access where a.username=@userID and a.password=@password and bt_activated='Active'");
                if (dr.HasRows)
                {
                    dr.Read();
                    Session["ch_group_access"] = dr["ch_group_access"].ToString().Trim();
                    Session["username"] = dr["username"].ToString().Trim();
                    Session["fullname"] = dr["first_name"].ToString().Trim() + " " + dr["last_name"].ToString().Trim();
                    Session["password"] = dr["password"].ToString().Trim();
                    Session["default_page"] = dr["vc_default_page"].ToString().Trim();
                    RedirectUser(dr["username"].ToString().Trim());
                }
                else
                {
                    lblNotification.ForeColor = System.Drawing.Color.Red;
                    lblNotification.Text = "Username and Password is incorrect!";
                }
                _SQL.Close();

            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }

        public void RedirectUser(string userID)
        {
            SQLProcedures cn = new SQLProcedures();
            cn.Open();
            cn.AddParameters("groupID", Session["ch_group_access"].ToString());
            PublicProperty.validAllowedMenu = cn.FillDataTable("select ch_menu_link from md_menus where ch_menu_link<>'#' and isnull(rtrim(ch_menu_link),'')<>'' and ch_group_access=@groupID and bt_isVisible=1 union all select ch_submenu_link from md_sub_menus where ch_submenu_link<>'#' and isnull(rtrim(ch_submenu_link),'')<>'' and ch_group_access=@groupID and bt_isVisible=1");
            cn.Close();
            Response.Redirect(Session["default_page"].ToString().Trim(), false);
        }
    }
}