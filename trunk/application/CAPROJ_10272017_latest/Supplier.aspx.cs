﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;
using System.Data;
using System.Data.SqlClient;

namespace CAPROJ
{
    public partial class Supplier : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadSuppliers();
            }

                
        }
        public void LoadSuppliers()
        {
            _SQL.getDropdown("Select * from md_supplier", "supplier_id","supplier_name", DropDownList1);
        }
        public void LoadSupplierData()
        {

                        //Load Supplier
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_supplier where Supplier_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();

                SuppID.Text = dr["Supplier_ID"].ToString();
                SupplierID.Text = dr["Supplier_ID"].ToString();
                SupplierName.Text = dr["supplier_name"].ToString().Trim();
                Session["supplierName"]= dr["supplier_name"].ToString().Trim();
                Category.Text = dr["category"].ToString().Trim();
                BusParType.Text = dr["business_partner_type"].ToString().Trim();
                Address.Text = dr["address"].ToString().Trim();
                City.Text = dr["city"].ToString().Trim();
                Province.Text = dr["province"].ToString().Trim();
                ZipCode.Text = dr["zipcode"].ToString().Trim();
                Email.Text = dr["email"].ToString().Trim();
                ContactPerson.Text = dr["contact_person"].ToString().Trim();
                Mobile.Text = dr["mobile_no"].ToString().Trim();

                //Disable the controls
                SupplierName.ReadOnly = true;
                Category.ReadOnly = true;
                BusParType.ReadOnly = true;
                Address.ReadOnly = true;
                City.ReadOnly = true;
                Province.ReadOnly = true;
                ZipCode.ReadOnly = true;
                Email.ReadOnly = true;
                ContactPerson.ReadOnly = true;
                Mobile.ReadOnly = true;

                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();
            //Load Supplier Payment
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList1.SelectedValue.ToString());
            SqlDataReader dr1 = _SQL.ExecuteReader("select * from md_supplier_payment where Supplier_ID=@value");
            if (dr1.HasRows)
            {
                dr1.Read();
                PaymentOption.Text = dr1["payment_option"].ToString();
                PaymentTerms.Text = dr1["payment_terms"].ToString().Trim();
                Discount.Text = dr1["discount"].ToString().Trim();

                //Disable the controls
                PaymentOption.ReadOnly = true;
                PaymentTerms.ReadOnly = true;
                Discount.ReadOnly = true;
            }
            _SQL.Close();

            //Load Supplier Products
            string SQL = "select * from md_supplier_products where Supplier_ID='" + DropDownList1.SelectedValue.ToString() + "'";
            GridSupplierList.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSupplierList.DataSource = dt;
                GridSupplierList.DataBind();
                //Show Gridview for viewing of products
                GridSupplierList.Visible = true;
                //Hide Gridview for adding new products
                //GridSupplierList.Visible = false;
                //Set datatable for editing purposes
                ViewState["ViewingTable"] = dt;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSupplierData();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SupplierName.ReadOnly = false;
            Category.ReadOnly = false;
            BusParType.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            ContactPerson.ReadOnly = false;
            Mobile.ReadOnly = false;
            PaymentOption.ReadOnly = false;
            PaymentTerms.ReadOnly = false;
            Discount.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)

        {
            
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                VerifySupplierIfExisting();
                if (Session["isexisting"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Supplier Already Exists!');", true);
                }
                else
                {
                    _SQL.Open();
                    //supplier data
                    _SQL.AddParameters("Supplier_ID", SupplierID.Text.Trim());
                    _SQL.AddParameters("supplier_name", SupplierName.Text.Trim());
                    _SQL.AddParameters("category", Category.Text.Trim());
                    _SQL.AddParameters("partner_type", BusParType.Text.Trim());
                    _SQL.AddParameters("address", Address.Text.Trim());
                    _SQL.AddParameters("city", City.Text.Trim());
                    _SQL.AddParameters("province", Province.Text.Trim());
                    _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                    _SQL.AddParameters("email", Email.Text.Trim());
                    _SQL.AddParameters("contact_person", ContactPerson.Text.Trim());
                    _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                    string query = @"INSERT INTO md_supplier(supplier_name,category,business_partner_type,address,city,province,zipcode,email,contact_person,mobile_no)
                                    VALUES(@supplier_name,@category,@partner_type,@address,@city,@province,@zipcode,@email,@contact_person,@mobileno);";

                    //supplier payment
                    _SQL.AddParameters("payment_option", PaymentOption.Text.Trim());
                    _SQL.AddParameters("payment_terms", PaymentTerms.Text.Trim());
                    _SQL.AddParameters("discount", Discount.Text.Trim());
                    query = query + @"INSERT INTO md_supplier_payment(payment_option,payment_terms,discount)
                                VALUES(@payment_option,@payment_terms,@discount);";

                    _SQL.ExecuteNonQuery(query);
                    _SQL.Close();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Supplier Successfully Added!'); window.location='" + Request.ApplicationPath + "SupplierList.aspx';", true);

                }
            }
            else
            {
                VerifySupplierIfExistingForEdit();
                if (Session["isexistingforedit"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Supplier Already Exists!');", true);
                }
                else
                {
                    _SQL.Open();
                    _SQL.AddParameters("Supplier_ID", SupplierID.Text.Trim());
                    _SQL.AddParameters("supplier_name", SupplierName.Text.Trim());
                    _SQL.AddParameters("category", Category.Text.Trim());
                    _SQL.AddParameters("partner_type", BusParType.Text.Trim());
                    _SQL.AddParameters("address", Address.Text.Trim());
                    _SQL.AddParameters("city", City.Text.Trim());
                    _SQL.AddParameters("province", Province.Text.Trim());
                    _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                    _SQL.AddParameters("email", Email.Text.Trim());
                    _SQL.AddParameters("contact_person", ContactPerson.Text.Trim());
                    _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                    _SQL.ExecuteNonQuery(@"UPDATE md_supplier SET supplier_name=@supplier_name,category=@category,business_partner_type=@partner_type,
                                        address=@address,city=@city,province=@province,zipcode=@zipcode,email=@email,contact_person=@contact_person
                                ,mobile_no=@mobileno WHERE Supplier_ID=@Supplier_ID");
                    _SQL.Close();

                    //Supplier payment
                    _SQL.Open();
                    _SQL.AddParameters("Supplier_ID", SupplierID.Text.Trim());
                    _SQL.AddParameters("payment_option", PaymentOption.Text.Trim());
                    _SQL.AddParameters("payment_terms", PaymentTerms.Text.Trim());
                    _SQL.AddParameters("discount", Discount.Text.Trim());
                    _SQL.ExecuteNonQuery(@"UPDATE md_supplier_payment SET payment_option=@payment_option,payment_terms=@payment_terms,
                                        discount=@discount WHERE Supplier_ID=@Supplier_ID");
                    _SQL.Close();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Supplier Successfully Updated!'); window.location='" + Request.ApplicationPath + "Supplier.aspx';", true);
            }
               
            
           
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_supplier WHERE Supplier_ID='" + SupplierID.Text + "';";
            query = query + "DELETE FROM md_supplier_payment WHERE Supplier_ID='" + SupplierID.Text + "';";
            query = query + "DELETE FROM md_supplier_products WHERE Supplier_ID = '" + SupplierID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Supplier Successfully Deleted!'); window.location='" + Request.ApplicationPath + "Supplier.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Supplier.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            SupplierID.Text = "";
            SupplierName.Text = "";
            Category.Text = "";
            BusParType.Text = "";
            Address.Text = "";
            City.Text = "";
            Province.Text = "";
            ZipCode.Text = "";
            Email.Text = "";
            ContactPerson.Text = "";
            Mobile.Text = "";
            PaymentOption.Text = "";
            PaymentTerms.Text = "";
            Discount.Text = "";

            //Enable the controls
            SupplierName.ReadOnly = false;
            Category.ReadOnly = false;
            BusParType.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            ContactPerson.ReadOnly = false;
            Mobile.ReadOnly = false;
            PaymentOption.ReadOnly = false;
            PaymentTerms.ReadOnly = false;
            Discount.ReadOnly = false;
            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }
        public void VerifySupplierIfExisting()
        {
            _SQL.Open();
            Session["isexisting"] = _SQL.ExecuteScalar("Select dbo.VerifySupplierIfExisting('" + SupplierName.Text + "')");
            
        }
        public void VerifySupplierIfExistingForEdit()
        {
            _SQL.Open();
            Session["isexistingforedit"] = _SQL.ExecuteScalar("Select dbo.VerifySupplierIfExistingForEdit('" + SupplierName.Text + "','" + Session["supplierName"].ToString().Trim() + "')");

        }
    }
  
}