﻿using CAPROJ.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CAPROJ
{
    public partial class Driver : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadDrivers();
            }


        }
        public void LoadDrivers()
        {
            _SQL.getDropdown("Select * from md_driver", "driver_id", "driver_name", DropDownList2);
        }
        public void LoadDriverData()
        {

            //Load Supplier
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList2.SelectedValue.ToString());
            SqlDataReader dr = _SQL.ExecuteReader("select * from md_driver where Driver_ID=@value");
            if (dr.HasRows)
            {
                dr.Read();

                DriverrID.Text = dr["Driver_ID"].ToString();
                DriverID.Text= dr["Driver_ID"].ToString();
                DriverName.Text = dr["driver_name"].ToString();
                Address.Text = dr["address"].ToString().Trim();
                City.Text = dr["city"].ToString().Trim();
                Province.Text = dr["province"].ToString().Trim();
                ZipCode.Text = dr["zipcode"].ToString().Trim();
                Email.Text = dr["email"].ToString().Trim();
                Mobile.Text = dr["mobile_no"].ToString().Trim();

                //Disable the controls
                DriverName.ReadOnly = true;
                Address.ReadOnly = true;
                City.ReadOnly = true;
                Province.ReadOnly = true;
                ZipCode.ReadOnly = true;
                Email.ReadOnly = true;
                Mobile.ReadOnly = true;

                //Disable Save Button, Enable Edit Button, Hide Cancel Button, Enable Delete Button
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnCancel.Visible = false;
                btnDelete.Enabled = true;
                btnDelete.CssClass = "btn btn-block btn-danger";
            }
            _SQL.Close();
            //Load Supplier Payment
            _SQL.Open();
            _SQL.AddParameters("value", DropDownList2.SelectedValue.ToString());
            SqlDataReader dr1 = _SQL.ExecuteReader("select * from md_driver_delivery where Driver_ID=@value");
            if (dr1.HasRows)
            {
                dr1.Read();
                PlateNo.Text = dr1["plate_no"].ToString();
                CarBrand.Text = dr1["car_brand"].ToString();
                CarColor.Text = dr1["car_color"].ToString();
                //Disable the controls
                PlateNo.ReadOnly = true;
                CarBrand.ReadOnly = true;
                CarColor.ReadOnly = true;
            }
            _SQL.Close();

         
            
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDriverData();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DriverName.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            PlateNo.ReadOnly = false;
            CarBrand.ReadOnly = false;
            CarColor.ReadOnly = false;
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Edit";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["VerifyIfAddorEdit"].ToString() == "Add")
            {
                _SQL.Open();
                //supplier data
                _SQL.AddParameters("Driver_ID", DriverID.Text.Trim());
                _SQL.AddParameters("driver_name", DriverName.Text.Trim());
                _SQL.AddParameters("address", Address.Text.Trim());
                _SQL.AddParameters("city", City.Text.Trim());
                _SQL.AddParameters("province", Province.Text.Trim());
                _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                _SQL.AddParameters("email", Email.Text.Trim());
                _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                string query = @"INSERT INTO md_driver(driver_name,address,city,province,zipcode,email,mobile_no)
                                       VALUES(@driver_name,@address,@city,@province,@zipcode,@email,@mobileno);";

                //supplier payment
                _SQL.AddParameters("plate_no", PlateNo.Text.Trim());
                _SQL.AddParameters("car_brand", CarBrand.Text.Trim());
                _SQL.AddParameters("car_color", CarColor.Text.Trim());
                query = query + @"INSERT INTO md_driver_delivery(plate_no,car_brand,car_color)
                                  VALUES(@plate_no, @car_brand, @car_color);";

                _SQL.ExecuteNonQuery(query);
                _SQL.Close();


                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Driver Successfully Added!'); window.location='" + Request.ApplicationPath + "Driver.aspx';", true);
            }
            else
            {
                _SQL.Open();
                _SQL.AddParameters("Driver_ID", DriverID.Text.Trim());
                _SQL.AddParameters("driver_name", DriverName.Text.Trim());
                _SQL.AddParameters("address", Address.Text.Trim());
                _SQL.AddParameters("city", City.Text.Trim());
                _SQL.AddParameters("province", Province.Text.Trim());
                _SQL.AddParameters("zipcode", ZipCode.Text.Trim());
                _SQL.AddParameters("email", Email.Text.Trim());
                _SQL.AddParameters("mobileno", Mobile.Text.Trim());
                _SQL.ExecuteNonQuery(@"UPDATE md_driver SET driver_name=@driver_name,address=@address,city=@city,province=@province,zipcode=@zipcode,email=@email,
                                           mobile_no=@mobileno WHERE Driver_ID=@Driver_ID");
                _SQL.Close();

                //Supplier payment
                _SQL.Open();
                _SQL.AddParameters("Driver_ID", DriverID.Text.Trim());
                _SQL.AddParameters("plate_no", PlateNo.Text.Trim());
                _SQL.AddParameters("car_brand", CarBrand.Text.Trim());
                _SQL.AddParameters("car_color", CarColor.Text.Trim());
                _SQL.ExecuteNonQuery(@"UPDATE md_driver_delivery SET plate_no=@plate_no, car_brand=@car_brand, car_color=@car_color WHERE Driver_ID=@Driver_ID");
                _SQL.Close();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Driver Successfully Updated!'); window.location='" + Request.ApplicationPath + "Driver.aspx';", true);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _SQL.Open();
            string query = "DELETE FROM md_driver WHERE Driver_ID='" + DriverID.Text + "';";
            query = query + "DELETE FROM md_driver_delivery WHERE Driver_ID='" + DriverID.Text + "';";
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Driver Successfully Deleted!'); window.location='" + Request.ApplicationPath + "Driver.aspx';", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Driver.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear textbox values
            DriverID.Text = "";
            DriverName.Text = "";
            Address.Text = "";
            City.Text = "";
            Province.Text = "";
            ZipCode.Text = "";
            Email.Text = "";
            Mobile.Text = "";
            PlateNo.Text = "";
            CarBrand.Text = "";
            CarColor.Text = "";

            //Enable the controls
            DriverName.ReadOnly = false;
            Address.ReadOnly = false;
            City.ReadOnly = false;
            Province.ReadOnly = false;
            ZipCode.ReadOnly = false;
            Email.ReadOnly = false;
            Mobile.ReadOnly = false;
            PlateNo.ReadOnly = false;
            CarBrand.ReadOnly = false;
            CarColor.ReadOnly = false;

            //Enable buttons
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnDelete.Enabled = false;
            btnDelete.CssClass = "btn btn-block btn-danger";
            btnCancel.Enabled = true;
            btnCancel.CssClass = "btn btn-block btn-danger";

            Session["VerifyIfAddorEdit"] = "Add";
        }
    }

}