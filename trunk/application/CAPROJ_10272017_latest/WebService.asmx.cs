﻿using CAPROJ.Modules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CAPROJ
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        public class Notification
        {
            public string EntityID { get; set; }
         
        }

        [WebMethod]
     
        public string HelloWorld(string entity_id_param)
        {
            //  var notification = new Notification
            //  {
            //      EntityID = "hello! " 
            //  };

            //   return JsonConvert.SerializeObject(chk);
             string entity_id = entity_id_param;
            SQLProcedures cn = new SQLProcedures();
            try
            {

                cn.Open();
                cn.ClearParameters();

                cn.AddParameters("entity_id", entity_id);
                // cn cn.AddParameters("userid", Session["user_id"].ToString());.AddParameters("userid", Session["user_id"].ToString());

                string query = "UPDATE tf_notification SET  bt_isread = 1 WHERE ch_entity_id =@entity_id ; ";

                cn.ExecuteNonQuery(query);
                //  Response.Redirect("NewsFeed.aspx");

                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                cn.Close();

            }


            //   return GetNewsFeedDa

          //  return "Hello World";
        }





        [WebMethod]
        public string loadNotification(string entity_id_param)
        {
            string entity_id = entity_id_param;
            SQLProcedures cn = new SQLProcedures();
            try
            {
                // Timer1.Enabled = false;
              //  Literal litNotifCount = new Literal();
              //  LiteralControl litNotifMsg = new LiteralControl();
                cn.Open();
                cn.ClearParameters();

                cn.AddParameters("agent_id", entity_id);

                string query = "SELECT * FROM tf_notification WHERE ch_entity_id = @agent_id and bt_isread=0 ; ";

                DataTable dt = cn.FillDataTable(query);

                //   if (notif_count.Text != dt.Rows.Count.ToString()) {
                if (dt.Rows.Count > 0)
                {


                    //  notif_count.Text = dt.Rows.Count.ToString();
                    //  notif_count.CssClass = "label label-danger ";
                    // notif_count.Style.Add("position", "absolute");
                    // notif_count.Style.Add("top", "0");
                    // notif_count.Style.Add("right", "0");
                    // plcnotif_msg.Controls.Add(new LiteralControl("</br>"));

                    //  litNotifCount.Text = "You have " + dt.Rows.Count.ToString() + " notifications";
                    //  plcnotif_count.Controls.Add(litNotifCount);
                    //  litNotifMsg.Text = "";
                    String notifmsg = "";
                    foreach (DataRow dr in dt.Rows)
                    {


                        notifmsg += "<li style='margin-bottom:10px;' ><a href = '" + dr["tx_url"] + "' > " +
                      "<table><tr><td><i class='fa fa-comments text-aqua' ></i></td><td style='width:100%'><p > " + dr["vc_notification_msg"] + "</p><small class='pull-right' style=' padding-right:5px'><i class='fa fa-clock'></i>" + TimeAgo(Convert.ToDateTime(dr["sd_date_created"])) + "</small> </td></tr></table></a> </li>";


                        //    litNotifMsg.Text += "<li style='margin-bottom:30px'> "+
                        //     "<table><tr><td><i class='fa fa-comments text-aqua pull-left' style='display:inline !important;'></i></td><td><p style='display:inline !important; vertical-align: text-top'> " + dr["vc_notification_msg"]  + "</p></td></tr></table> </li>";

                    }
                    //litNotifMsg.Text = Server.HtmlDecode(litNotifMsg.Text);
                    //  plcnotif_msg.Controls.Add(litNotifMsg);
                    return notifmsg;
                }
                else
                {
                    return "";
                }
                //   }

            }
            catch (Exception ex)
            {
                return ex.ToString();
               // Response.Write(ex.ToString());
            }
            finally
            {
                cn.Close();

            }

        }

        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("about {0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("about {0} {1} ago",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("about {0} {1} ago",
                span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("about {0} {1} ago",
                span.Hours, span.Hours == 1 ? "hour" : "hours");
            if (span.Minutes > 0)
                return String.Format("about {0} {1} ago",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("about {0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return string.Empty;
        }

        public string  gettest()
        {
         
            return "s";
        }

    }
}
