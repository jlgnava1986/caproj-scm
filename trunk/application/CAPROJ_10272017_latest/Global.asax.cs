﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Configuration;
using CAPROJ.Modules;

namespace CAPROJ
{
    public class Global : System.Web.HttpApplication
    {
        PublicProperties PublicProperty = new PublicProperties();
        protected void Application_Start(object sender, EventArgs e)
        {
            //SqlDependency.Start(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpException))
            {
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength"))
                    return;

                Server.Transfer("Default.aspx");
            }

            // Clear the error from the server
            Server.ClearError();
            PublicProperty.validMessage = "Something went wrong! Please try again in a few minutes." + exc.Message;
            Response.Redirect("Default.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            //SqlDependency.Stop(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            //SqlDependency.Stop(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
        }
    }
}