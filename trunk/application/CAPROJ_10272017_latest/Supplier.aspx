﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Supplier.aspx.cs" Inherits="CAPROJ.Supplier" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Dashboard_Portal" runat="server">
   <aside class="right-side">
      <section class="content-header">
         <h1>
            Supplier Master Data
         </h1>
      </section>
      <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
               <div class="row">
                  <div class="col-md-12">
                     <%-- 1 column is equal to 12 so if its 6 its only half of the page --%>
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#Gen" data-toggle="tab">General</a></li>
                           <li><a href="#Pay" data-toggle="tab">Payment</a></li>
                           <li><a href="#Prod" data-toggle="tab">Products</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="Gen">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="row">
                                       <div class="col-md-2">
                                          <div class="form-group">
                                             Supplier ID: 
                                             <asp:Label ID="SupplierID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                        <div class="col-md-6">
                                           <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                                       </div>
                                         <div class="col-md-4">
                                           
                                       </div>
                                       
                                    </div>
                                     </br>
                                     <br />
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <small>
                                                     Supplier Name
                                                 </small>
                                                <asp:TextBox ID="SupplierName" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>Category</small>
                                                <asp:TextBox ID="Category" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Business Partner Type</small>
                                                 <asp:TextBox ID="BusParType" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>Address</small>
                                                 <asp:TextBox ID="Address" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>City</small>
                                                 <asp:TextBox ID="City" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Province</small>
                                                 <asp:TextBox ID="Province" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                         </div>
                                             <div class="col-md-6">
                                             <small>
                                                 Zip Code
                                             </small>
                                                <asp:TextBox ID="ZipCode" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                 <br />
                                                <small> Email</small>
                                                 
                                                 <asp:TextBox ID="Email" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                                 <br />
                                                 <small>Contact Person</small>
                                                 
                                                 <asp:TextBox ID="ContactPerson" runat="server" CssClass="form-control"></asp:TextBox><br />
                                              
                                                 <small>Mobile Phone</small>
                                                 
                                                 <asp:TextBox ID="Mobile" runat="server" CssClass="form-control"></asp:TextBox>
                                             </div>
                                     </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="Pay">
                              <div class="row">
                                 <div class="col-md-12">
                                     <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             Supplier ID: 
                                             <asp:Label ID="SuppID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                       
                                     
                                    </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                
                                                 <small>Payment Option</small>
                                                <asp:TextBox ID="PaymentOption" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Payment Terms</small>
                                                 <asp:TextBox ID="PaymentTerms" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small>Discount</small>
                                                 <asp:TextBox ID="Discount" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                         </div>
                                             <div class="col-md-6">
                                         
                                             </div>
                                    </div>
                                 </div>
                              </div>
                          
                           <div class="tab-pane" id="Prod">
                              <div class="row">
                                 <div class="col-md-12">
                                     <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             Supplier ID: 
                                             <asp:Label ID="SupplierID2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                      
                                    </div>
                                     Sample
                                   <asp:GridView ID="GridSupplierList" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None"  AllowPaging="true"                                       
                                        BorderStyle="None" Class="table" AllowSorting="True">
                                        <Columns>
                                            <asp:BoundField DataField="product_name" HeaderText="Product Name" SortExpression="product_name">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="lead_time" HeaderText="Lead Time" SortExpression="lead_time">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                    </asp:GridView>
                                 </div>
                              </div>
                           </div>
                        </div>
                         
                     </div>
                      <div class="row">
                             <div class="col-md-2">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-block btn-danger" Text="Cancel" BackColor="#b01717" OnClick="btnCancel_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnAdd" runat="server" class="btn btn-block btn-danger" Text="Add" BackColor="#b01717" OnClick="btnAdd_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnEdit" runat="server" class="btn btn-block btn-danger" Text="Edit" BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnDelete" runat="server" class="btn btn-block btn-danger" Text="Delete" BackColor="#b01717" OnClick="btnDelete_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnSave" runat="server" class="btn btn-block btn-danger" Text="Save" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                             </div>
                         </div>
                  </div>
               </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
         </asp:UpdatePanel>
      </section>
   </aside>
</asp:Content>