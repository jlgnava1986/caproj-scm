﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Forms.aspx.cs" Inherits="ETIIOnlineServices.Forms" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Downloadable Forms</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
            <asp:Panel ID="panelUploadFile" runat="server">
              <div class="col-xs-12 no-padding">
                    <div class="box-header">
                        <h4 class="box-title">Add New File</h4>
                    </div>
                    <div class="row" style="padding:10px 10px 10px 10px">                                    
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" 
                                CssClass="btn btn-flat btn-success btn-block" CausesValidation="false" 
                                onclick="btnUpload_Click"/>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center">
                            <h5><asp:Label ID="lblNotifProfilePhoto" runat="server" Text=""></asp:Label></h5>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-padding"><hr/></div>
            </asp:Panel>
            <div class="col-xs-12 no-padding">
                <%--<div class="box" style="padding:10px 10px 10px 10px">--%>
                    <%--<div class="box-header">--%>
                        <h4>Please click the image to download.</h4>
                    <%--</div>--%>
                    <div class="row">
                        <asp:PlaceHolder id="dloadableForms" runat="server"></asp:PlaceHolder>
                    </div>
                <%--</div>--%>        
            </div>
        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</asp:Content>

