using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

	/// <summary>
	/// Summary description for StarLiteral.
	/// </summary>
	public class MyLiteral : Literal
	{
		public MyLiteral(string strText)
		{
			Text = strText;
		}

		public void AddParagraph(string strText)
		{
			Text += TextWriter.MakeParagraph(strText);
		}
	}
