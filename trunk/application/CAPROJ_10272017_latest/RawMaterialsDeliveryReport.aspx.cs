﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CAPROJ
{
    public partial class RawMaterialsDeliveryReport : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadScheduleForViewing(txtFilter.Text);
                LoadReport();
                FromDate.CssClass = "form-control";
                ToDate.CssClass = "form-control";
            }
        }

        public void LoadScheduleForViewing(string filter)
        {
            string SQL = "";
            if (dropColumns.SelectedValue == "date_received")
            {
                SQL = "select * from dbo.view_delivery_reports_raw_materials where cast(date_received as date) between '" + FromDate.Text + "' and '" + ToDate.Text + "'";
            }
            else
            {
                SQL = "select * from dbo.view_delivery_reports_raw_materials where ('" + filter + "'='') or ('" + filter + "'<>'' and (" + dropColumns.SelectedValue.ToString() + " like '%' + '" + filter + "' +'%'))";
            }
            GridScheduleViewing.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridScheduleViewing.DataSource = dt;
                GridScheduleViewing.DataBind();
                GridScheduleViewing.Visible = true;
                ViewState["ViewingTable"] = dt;
            }
        }

        protected void GridScheduleViewing_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(ViewState["ViewingTable"]);
            {
                string SortDir = string.Empty;
                if (dir == System.Web.UI.WebControls.SortDirection.Ascending)
                {
                    dir = System.Web.UI.WebControls.SortDirection.Descending;
                    SortDir = "Desc";
                }
                else
                {
                    dir = System.Web.UI.WebControls.SortDirection.Ascending;
                    SortDir = "Asc";
                }
                DataView sortedView = new DataView(dt);
                sortedView.Sort = e.SortExpression + " " + SortDir;
                GridScheduleViewing.DataSource = sortedView;
                GridScheduleViewing.DataBind();
            }
        }

        public System.Web.UI.WebControls.SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = System.Web.UI.WebControls.SortDirection.Ascending;
                }
                return (System.Web.UI.WebControls.SortDirection)ViewState["dirState"];
            }

            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void LoadReport()
        {
            ReportDocument reportviewer = new ReportDocument();
            FormulaFieldDefinitions pubFormulaFields;
            FormulaFieldDefinition pubFormulaField;

            ConnectionInfo Connection_Info = new ConnectionInfo();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            Tables CrTables;

            string[] strFormulaParam = { "printedby" };
            string[] strFormulaValue = { "'" + Session["fullname"].ToString() + "'" };
            reportviewer.Load(Server.MapPath("Report/RawMaterialsDeliveryReport.rpt"));

            Connection_Info.ServerName = "LAPTOP-AI3BIIL8\\ALYSSA";
            Connection_Info.DatabaseName = "CAPROJ";
            Connection_Info.UserID = "sa";
            Connection_Info.Password = "abc@123";
            CrTables = reportviewer.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = Connection_Info;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            DataTable dtMain = (DataTable)ViewState["ViewingTable"];
            reportviewer.Database.Tables[0].SetDataSource(dtMain);

            CrystalReportViewer1.ReportSource = reportviewer;

            pubFormulaFields = reportviewer.DataDefinition.FormulaFields;
            if (pubFormulaFields.Count > 0 && strFormulaParam != null)
            {
                for (int i = 0; (i <= (strFormulaParam.Length - 1)); i++)
                {
                    if ((strFormulaParam[i].Trim().Length > 0))
                    {
                        reportviewer.DataDefinition.FormulaFields[strFormulaParam[i]].Text = strFormulaValue[i];
                    }
                }
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadScheduleForViewing(txtFilter.Text);
            LoadReport();
        }

        protected void dropColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropColumns.SelectedValue == "date_received")
            {
                txtFilter.Enabled = false;
                txtFilter.CssClass = "form-control";

                FromDate.Enabled = true;
                FromDate.CssClass = "form-control";
                ToDate.Enabled = true;
                ToDate.CssClass = "form-control";
            }
            else
            {
                txtFilter.Enabled = true;
                txtFilter.CssClass = "form-control";

                FromDate.Enabled = false;
                FromDate.CssClass = "form-control";
                ToDate.Enabled = false;
                ToDate.CssClass = "form-control";
            }

        }
    }
}