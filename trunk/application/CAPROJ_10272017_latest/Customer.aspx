﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="CAPROJ.Customer" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Dashboard_Portal" runat="server">
   <aside class="right-side">
      <section class="content-header">
         <h1>
            Customer Master Data
         </h1>
      </section>
      <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
               <div class="row">
                  <div class="col-md-12">
                     <%-- 1 column is equal to 12 so if its 6 its only half of the page --%>
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#Gen" data-toggle="tab">General</a></li>
                           <li><a href="#Pay" data-toggle="tab">Payment</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="Gen">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="row">
                                       <div class="col-md-2">
                                          <div class="form-group">
                                             Customer ID: 
                                             <asp:Label ID="CustomerID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                        <div class="col-md-6">
                                           <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                                       </div>
                                         <div class="col-md-4">
                                           
                                       </div>
                                       
                                    </div>
                                     </br>
                                     <br />
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <small>
                                                     Customer Name
                                                 </small>
                                                <asp:TextBox ID="CustomerName" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>Type</small>
                                                <asp:TextBox ID="Type" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <br />
                                                 <small>Address</small>
                                                 <asp:TextBox ID="Address" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                 <small>City</small>
                                                 <asp:TextBox ID="City" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Province</small>
                                                 <asp:TextBox ID="Province" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                         </div>
                                             <div class="col-md-6">
                                             <small>
                                                 Zip Code
                                             </small>
                                                <asp:TextBox ID="ZipCode" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                 <br />
                                                <small>Email</small>
                                                 
                                                 <asp:TextBox ID="Email" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                                 <br />
                                                 <small>Contact Person</small>
                                                 
                                                 <asp:TextBox ID="ContactPerson" runat="server" CssClass="form-control"></asp:TextBox><br />
                                                <small> Telephone</small>
                                                 
                                                 <asp:TextBox ID="Telephone" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox><br />
                                                 <small>Mobile Phone</small>
                                                 
                                                 <asp:TextBox ID="Mobile" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                             </div>
                                     </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="Pay">
                              <div class="row">
                                 <div class="col-md-12">
                                     <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             Customer ID: 
                                             <asp:Label ID="CustID" runat="server" Text="" Font-Bold="true"></asp:Label>
                                          </div>
                                       </div>
                                       
                                     
                                    </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                
                                                 <small>Payment Option</small>
                                                <asp:TextBox ID="PaymentOption" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small> Payment Terms</small>
                                                 <asp:TextBox ID="PaymentTerms" runat="server" CssClass="form-control"></asp:TextBox>
                                                 <br />
                                                <small>Discount</small>
                                                 <asp:TextBox ID="Discount" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                         </div>
                                             <div class="col-md-6">
                                         
                                             </div>
                                    </div>
                                 </div>
                              </div>
                          
                        </div>
                         
                     </div>
                      <div class="row">
                             <div class="col-md-2">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-block btn-danger" Text="Cancel" BackColor="#b01717" OnClick="btnCancel_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnAdd" runat="server" class="btn btn-block btn-danger" Text="Add" BackColor="#b01717" OnClick="btnAdd_Click"> </asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnEdit" runat="server" class="btn btn-block btn-danger" Text="Edit" BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnDelete" runat="server" class="btn btn-block btn-danger" Text="Delete" BackColor="#b01717" OnClick="btnDelete_Click"></asp:Button>
                             </div>
                             <div class="col-md-2">
                                 <asp:Button ID="btnSave" runat="server" class="btn btn-block btn-danger" Text="Save" BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                             </div>
                         </div>
                  </div>
               </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
         </asp:UpdatePanel>
      </section>
   </aside>
</asp:Content>