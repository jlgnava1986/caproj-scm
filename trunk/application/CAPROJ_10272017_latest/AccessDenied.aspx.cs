﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.Net.Mail;
using System.Net;
using System.Web.Mail;
using System.Web.Caching;

namespace CAPROJ
{
    public partial class AccessDenied : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        WebFunctions.WebFunctions func = new WebFunctions.WebFunctions();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
        }

        protected void lnkBacktoLoginPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}