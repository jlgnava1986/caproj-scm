﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.SqlClient;
using System.Threading;
using CAPROJ.Modules;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;

namespace CAPROJ
{
    public partial class Main : System.Web.UI.MasterPage
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        Literal lit = new Literal();
        Literal litSub = new Literal();

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            if (!Page.IsPostBack)
            {
                if (Session["username"] == null) 
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                else
                {
                    SqlDataReader rd = _SQL.getRead("SELECT fullname=first_name + ' ' + last_name,first_name,last_name,department=rtrim(department),vc_group_name FROM md_administration a inner join md_menu_group b on a.ch_group_access=b.ch_group_access WHERE username='" + Session["username"].ToString() + "'");
                    if (rd.HasRows)
                    {
                        rd.Read();
                        Session["validFullName"] = rd[0].ToString();
                        Session["validFirstName"] = rd[1].ToString();
                        Session["validLastName"] = rd[2].ToString();
                        Session["validDepartment"] = rd[3].ToString();
                        Session["groupAccess"] = rd[4].ToString();
                        lblAccess.Text = Session["groupAccess"].ToString();

                        profile_photo.ImageUrl = "img/avatar2.png";

                        lblUserAlias.Text = Session["validFullName"].ToString();
                        LoadMenuItems();
                    }
                }
              
            }
        }

        private void SQL_EventState(ConnectionState state)
        {
            Thread.Sleep(0);
        }

        public void LoadMenuItems()
        {
            string isActive = "''";
            string isActiveSub = "''";
            try
            {
                _SQL.Open();
                _SQL.AddParameters("groupID", Session["ch_group_access"].ToString());
                DataTable dt = _SQL.FillDataTable("select * from md_menus where ch_group_access=@groupID and bt_isVisible=1 order by int_menu_index");
                DataTable dtSub = _SQL.FillDataTable("select a.* from md_sub_menus a inner join md_menus b on a.ch_menu_id = b.ch_menu_id where a.ch_group_access=@groupID and a.bt_isVisible=1 order by b.int_menu_index,a.int_submenu_index");
                DataTable dtSubFiltered;
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToBoolean(dr["bt_hasSubMenu"])) { isActive = "treeview active"; } else { isActive = "''"; }
                        lit.Text += TextWriter.MakeMenu(isActive, dr["ch_menu_name"].ToString().Trim(), dr["ch_menu_link"].ToString().Trim(), dr["ch_menu_icon"].ToString().Trim(), Convert.ToBoolean(dr["bt_hasSubMenu"]));

                        litSub.Text = "";
                        if (Convert.ToBoolean(dr["bt_hasSubMenu"]))
                        {
                            dtSubFiltered = dtSub.AsEnumerable().Where(row => row.Field<String>("ch_menu_id") == dr["ch_menu_id"].ToString().Trim()).OrderBy(row => row.Field<int>("int_submenu_index")).CopyToDataTable();
                            foreach (DataRow drSub in dtSubFiltered.Rows)
                            {
                                //if (Convert.ToBoolean(drSub["bt_isactive"])) { isActiveSub = "active"; } else { isActiveSub = ""; }
                                litSub.Text += TextWriter.MakeSubMenu(isActiveSub, drSub["ch_submenu_name"].ToString().Trim(), drSub["ch_submenu_link"].ToString().Trim(), drSub["ch_submenu_icon"].ToString().Trim());
                            }
                            litSub.Text += "</ul></li>";
                        }
                        lit.Text += litSub.Text;
                    }
                    LocalPlaceHolder.Controls.Add(lit);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _SQL.Close();
            }
        }

        public void SetActiveMenu()
        { 
        
        }
        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Session["username"] = null;
            Response.Redirect("Default.aspx");
        }

        public string DeviceType() 
        {
            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex OS = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex device = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string device_info = string.Empty;
            if (OS.IsMatch(userAgent))
            {
                device_info = OS.Match(userAgent).Groups[0].Value;
            }
            if (device.IsMatch(userAgent.Substring(0, 4)))
            {
                device_info += device.Match(userAgent).Groups[0].Value;
            }
            if (!string.IsNullOrEmpty(device_info))
            {
                return "Mobile";
            }
            else
            {
                return "Desktop";
            }
        }
    }
}