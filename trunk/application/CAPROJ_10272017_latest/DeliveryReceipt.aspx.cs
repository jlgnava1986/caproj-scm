﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class DeliveryReceipt : System.Web.UI.Page
    {

        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
               
                _SQL.getDropdown("select SalesOrder_ID from md_sales_order_header where status ='active'", "SalesOrder_ID", "SalesOrder_ID", dropSO);
                dropSO.CssClass = "form-control";
                txtCustomerName.CssClass = "form-control";
                txtRemarks.CssClass = "form-control";
                if (dropSO.Items.Count > 0)
                {
                    LoadSalesOrderDetails();
                }
               
            }

        }
        public void LoadSalesOrderDetails()
        {
            string SalesOrder_ID = dropSO.SelectedValue.ToString().Trim();

            _SQL.Open();
            _SQL.AddParameters("sales_id", SalesOrder_ID);
            SqlDataReader dr = _SQL.ExecuteReader("select a.*, b.customer_name from md_sales_order_header a inner join md_customer b on a.Customer_ID = b.Customer_ID where SalesOrder_ID=@sales_id");
            if (dr.HasRows)
            {
                dr.Read();
                txtCustomerName.Text = dr["customer_name"].ToString().Trim();
                DateTime delivery_date = Convert.ToDateTime(dr["delivery_date"].ToString().Trim());
                txtDeliveryDate.Text = delivery_date.ToString("MM/dd/yyyy");
                DateTime date_order = Convert.ToDateTime(dr["date_order"].ToString().Trim());
                txtDateOrder.Text = date_order.ToString("MM/dd/yyyy");
                txtRemarks.Text = dr["remarks"].ToString().Trim();

            }
            _SQL.Close();

         
            string SQL;
            SQL = "select * from view_so_for_delivery_receipt where SalesOrder_ID ='" + SalesOrder_ID + "'";
            GridDeliveryReceipt.DataSource = null;
            GridDeliveryReceipt.DataBind();
            _SQL.getGrid(SQL, GridDeliveryReceipt);
            DataTable dt = (DataTable)GridDeliveryReceipt.DataSource;
            if (dt != null)
            {
                DataTable dt1 = new DataTable();
                DataRow dr1 = null;
              
                dt1.Columns.Add(new DataColumn("product_name", typeof(string)));
                dt1.Columns.Add(new DataColumn("quantity", typeof(int)));
                dt1.Columns.Add(new DataColumn("remaining_quantity", typeof(int)));
                dt1.Columns.Add(new DataColumn("ordered_quantity", typeof(int)));
                dt1.Columns.Add(new DataColumn("unit_price", typeof(double)));
                dt1.Columns.Add(new DataColumn("discount", typeof(int)));
                dt1.Columns.Add(new DataColumn("tax", typeof(int)));
                dt1.Columns.Add(new DataColumn("total", typeof(double)));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Insert the rows on Datatable
                    //add values
                    dr1 = dt1.NewRow();
                    dr1["product_name"] = dt.Rows[i]["product_name"].ToString().Trim();
                    dr1["quantity"] = dt.Rows[i]["quantity"].ToString().Trim();
                    dr1["remaining_quantity"] = dt.Rows[i]["remaining_quantity"].ToString().Trim();
                    dr1["ordered_quantity"] = dt.Rows[i]["ordered_quantity"].ToString().Trim();
                    dr1["unit_price"] = dt.Rows[i]["unit_price"].ToString().Trim();
                    dr1["discount"] = dt.Rows[i]["discount"].ToString().Trim();
                    dr1["tax"] = dt.Rows[i]["tax"].ToString().Trim();
                    dr1["total"] = dt.Rows[i]["total"].ToString().Trim();
                    dt1.Rows.Add(dr1);

                    ViewState["CurrentTable"] = dt1;
                    GridDeliveryReceipt.DataSource = dt1;
                    GridDeliveryReceipt.DataBind();
                }
                //display the data on the gridview
                DisplayTheData();
            }
        }

        private void DisplayTheData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[0].FindControl("product_name");
                        TextBox box2 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[1].FindControl("quantity");
                        TextBox boxN = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[2].FindControl("remaining_quantity");
                        TextBox boxO = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[3].FindControl("ordered_quantity");
                        TextBox box3 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[4].FindControl("unit_price");
                        TextBox box4 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[5].FindControl("discount");
                        TextBox box5 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[6].FindControl("tax");
                        TextBox box6 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[7].FindControl("total");

                        box1.Text = dt.Rows[i]["product_name"].ToString();
                        box1.ReadOnly = true;
                        box2.Text = dt.Rows[i]["quantity"].ToString();
                        boxN.ReadOnly = true;
                        boxN.Text = dt.Rows[i]["remaining_quantity"].ToString();
                        boxO.ReadOnly = true;
                        boxO.Text = dt.Rows[i]["ordered_quantity"].ToString();
                        double unit_price = Convert.ToDouble(dt.Rows[i]["unit_price"].ToString());
                        box3.Text = unit_price.ToString("N");
                        box3.ReadOnly = true;
                        box4.Text = dt.Rows[i]["discount"].ToString();
                        box4.ReadOnly = true;
                        box5.Text = dt.Rows[i]["tax"].ToString();
                        box5.ReadOnly = true;
                        double total = Convert.ToDouble(dt.Rows[i]["total"].ToString());
                        box6.Text = total.ToString("N");
                        box6.ReadOnly = true;
                        rowIndex++;
                    }
                }

                //Display total footer
                //add total amount on footer
                //compute total amount on the footer
                TextBox txtTotalAmountDue = (TextBox)GridDeliveryReceipt.FooterRow.FindControl("totalPaymentDue");
                double totalAmountDue = 0;
                for (int i = 0; i < GridDeliveryReceipt.Rows.Count; i++)
                {
                    TextBox amount = (TextBox)GridDeliveryReceipt.Rows[i].Cells[7].FindControl("total");
                    totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
                }
                txtTotalAmountDue.Enabled = false;
                txtTotalAmountDue.CssClass = "form-control";
                txtTotalAmountDue.Text = totalAmountDue.ToString("N");
            }
        }

        protected void dropSO_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSalesOrderDetails();
        }

        protected void quantity_TextChanged(object sender, EventArgs e)
        {
            //kinukuha lang natin kung ano yung row na nagchanged ang value
            TextBox txtQuantity = (TextBox)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)txtQuantity.Parent.Parent);
            TextBox unitPrice = (TextBox)grdrDropDownRow.FindControl("unit_price");
            TextBox remainingQuantity = (TextBox)grdrDropDownRow.FindControl("remaining_quantity");
            TextBox orderedQuantity = (TextBox)grdrDropDownRow.FindControl("ordered_quantity");

            if (Convert.ToInt32(txtQuantity.Text) > Convert.ToInt32(orderedQuantity.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Invalid Amount!');", true);
            }
            else
            {
                //start computation
                //kinukuha yung value ng discount
                TextBox txtDiscount = (TextBox)grdrDropDownRow.FindControl("discount");
                TextBox txtTotal = (TextBox)grdrDropDownRow.FindControl("total");
                remainingQuantity.Text = (Convert.ToInt32(orderedQuantity.Text) - Convert.ToInt32(txtQuantity.Text)).ToString();
                //compute total amount
                double total = ComputeTotalPerRow(Convert.ToDouble(txtQuantity.Text), Convert.ToDouble(unitPrice.Text), Convert.ToDouble(txtDiscount.Text));
                txtTotal.Text = total.ToString("N");

                //add total amount on footer
                //display total amoun on footer
                TextBox txtTotalAmountDue = (TextBox)GridDeliveryReceipt.FooterRow.FindControl("totalPaymentDue");
                double totalAmountDue = 0;
                for (int i = 0; i < GridDeliveryReceipt.Rows.Count; i++)
                {
                    TextBox amount = (TextBox)GridDeliveryReceipt.Rows[i].Cells[7].FindControl("total");
                    totalAmountDue = totalAmountDue + Convert.ToDouble(amount.Text);
                }
                txtTotalAmountDue.Text = totalAmountDue.ToString("N");
            }
        }

        public double ComputeTotalPerRow(double quantity, double price, double discountPercentage)
        {
            double total = 0;
            double origPrice = 0;
            double discountAmount = 0;
            double discountedPrice = 0;
            double taxAmount = 0;

            origPrice = quantity * price;
            discountAmount = origPrice * (discountPercentage / 100);
            discountedPrice = origPrice - discountAmount;
            taxAmount = discountedPrice * .12;
            total = discountedPrice + taxAmount;

            return total;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateDataBeforeSaving();

            //insert header
            _SQL.Open();
            _SQL.AddParameters("so_id", dropSO.SelectedValue.ToString().Trim());
            _SQL.AddParameters("date_received", txtDateReceived.Text);
            _SQL.ExecuteNonQuery("insert into md_delivery_receipt_header(SalesOrder_ID,date_received)values(@so_id,@date_received)");
            _SQL.Close();

            //insert details
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            string query = "";

            foreach (DataRow row in dt.Rows)
            {
                if (row["quantity"].ToString() != "0")
                {
                    query = query + "INSERT INTO md_delivery_receipt_detail_v2(DeliveryReceipt_ID,FinishedGoods_ID,quantity)VALUES((select max(DeliveryReceipt_ID) from md_delivery_receipt_header),'" + row["product_name"].ToString() + "','" + row["quantity"].ToString() + "');";
                }
                _SQL.Open();
                SqlDataReader dr = _SQL.ExecuteReader("select cnt=isnull(count(FinishedGoods_ID),0) from md_finished_goods where finished_goods_name='" + row["product_name"].ToString() + "'");
                if (dr.HasRows)
                {
                    dr.Read();
                    int count = 0;
                    count = Convert.ToInt32(dr[0].ToString());
                    if (count == 0)
                    {
                        query = query + "INSERT INTO md_finished_goods(finished_goods_name,category,unit_price,lead_time)VALUES('" + row["product_name"].ToString() + "','','0','');";
                        query = query + "INSERT INTO md_finished_goods_list(FinishedGoods_ID,onhand_quantity,max_quantity)VALUES((select max(FinishedGoods_ID) from md_finished_goods),'" + row["quantity"].ToString() + "',0);";
                    }
                    else
                    {
                        query = query + "UPDATE md_finished_goods_list SET onhand_quantity=onhand_quantity + '" + row["quantity"].ToString() + "' where FinishedGoods_ID=(select FinishedGoods_ID from md_finished_goods where finished_goods_name='" + row["product_name"].ToString() + "');";
                    }
                }
                _SQL.Close();
            }

            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();

            GetSO_Status();
            if (Session["status"].ToString().Trim() == "delivered")
            {
                //update SO status
                _SQL.Open();
                _SQL.AddParameters("so_id", dropSO.SelectedValue.ToString().Trim());
                _SQL.ExecuteNonQuery("update md_sales_order_header set status='delivered' where SalesOrder_ID=@so_id");
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Delivery Receipt Successfully Submitted!'); window.location='" + Request.ApplicationPath + "DeliveryReceipt.aspx';", true);
        }



    public void UpdateDataBeforeSaving()
        {
            //Save the updated value of textbox to ViewState["CurrentTable"]
            if (GridDeliveryReceipt.Rows.Count > 0)
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    DataTable dtNewTable = new DataTable();
                    dtNewTable.Columns.Add(new DataColumn("product_name", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("quantity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("remaining_quantity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("ordered_quantity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("unit_price", typeof(double)));
                    dtNewTable.Columns.Add(new DataColumn("discount", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("tax", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("total", typeof(double)));
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[0].FindControl("product_name");
                            TextBox box2 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[1].FindControl("quantity");
                            TextBox boxN = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[2].FindControl("remaining_quantity");
                            TextBox boxO = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[3].FindControl("ordered_quantity");
                            TextBox box3 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[4].FindControl("unit_price");
                            TextBox box4 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[5].FindControl("discount");
                            TextBox box5 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[6].FindControl("tax");
                            TextBox box6 = (TextBox)GridDeliveryReceipt.Rows[rowIndex].Cells[7].FindControl("total");

                            drCurrentRow = dtNewTable.NewRow();

                            drCurrentRow["product_name"] = box1.Text;
                            drCurrentRow["quantity"] = box2.Text;
                            drCurrentRow["remaining_quantity"] = boxN.Text;
                            drCurrentRow["ordered_quantity"] = boxO.Text;
                            drCurrentRow["unit_price"] = box3.Text;
                            drCurrentRow["discount"] = box4.Text;
                            drCurrentRow["tax"] = box4.Text;
                            drCurrentRow["total"] = box4.Text;
                            dtNewTable.Rows.Add(drCurrentRow);

                            rowIndex++;
                        }
                        ViewState["CurrentTable"] = dtNewTable;
                    }
                }
            }
        }
        public void GetSO_Status()
        {
            int totalSO = 0;
            int totalDR = 0;

            //get total quantity of Delivery Receipt
            _SQL.Open();
            SqlDataReader dr = _SQL.ExecuteReader("select totatDR=isnull(sum(a.quantity),0) from md_delivery_receipt_detail_v2 a inner join md_delivery_receipt_header b on a.DeliveryReceipt_ID = b.DeliveryReceipt_ID where b.SalesOrder_ID='" + dropSO.SelectedValue.ToString().Trim() + "'");
            if (dr.HasRows)
            {
                dr.Read();
                totalDR = Convert.ToInt32(dr[0].ToString());
            }
            _SQL.Close();

            //get total quantity of Purchase Order
            _SQL.Open();
            string query = "select totatSO=isnull(sum(quantity),0) from md_sales_order_detail where SalesOrder_ID='" + dropSO.SelectedValue.ToString().Trim() + "'";
            SqlDataReader dr1 = _SQL.ExecuteReader(query);
            if (dr1.HasRows)
            {
                dr1.Read();
                totalSO = Convert.ToInt32(dr1[0].ToString());
            }
            _SQL.Close();

            //Compare
            if (totalSO != totalDR)
            {
                Session["status"] = "active";
            }
            else
            {
                Session["status"] = "delivered";
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeliveryReceipt.aspx");
        }







    }
}