﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class ListOfUsers : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                //load the users on the viewing gridview
                //dalawa kasi gridview natin, una is for viewing, tapos yung isa is for editing
                //viewing = GridUsersViewing
                //editing = GridUsers
                LoadUsersForViewing();
                //hide muna natin yung GridUsers
                GridUsers.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (GridUsers.Rows.Count > 0)
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    //name ng columns sa GridUsers
                    dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    DataTable dtNewTable = new DataTable();
                    dtNewTable.Columns.Add(new DataColumn("first_name", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("last_name", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("username", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("group_access", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("status", typeof(string)));
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            //get the columns
                            DropDownList dropStatus = (DropDownList)GridUsers.Rows[rowIndex].Cells[4].FindControl("dropStatus");
                            TextBox box1 = (TextBox)GridUsers.Rows[rowIndex].Cells[0].FindControl("first_name");
                            TextBox box2 = (TextBox)GridUsers.Rows[rowIndex].Cells[1].FindControl("last_name");
                            TextBox box3 = (TextBox)GridUsers.Rows[rowIndex].Cells[2].FindControl("username");
                            TextBox box4 = (TextBox)GridUsers.Rows[rowIndex].Cells[3].FindControl("group_access");

                            drCurrentRow = dtNewTable.NewRow();

                            //get the values
                            drCurrentRow["status"] = dropStatus.SelectedValue.ToString();
                            drCurrentRow["first_name"] = box1.Text;
                            drCurrentRow["last_name"] = box2.Text;
                            drCurrentRow["username"] = box3.Text;
                            drCurrentRow["group_access"] = box4.Text;
                            dtNewTable.Rows.Add(drCurrentRow);

                            rowIndex++;
                        }
                        ViewState["CurrentTable"] = dtNewTable;
                        //update the status of the users
                        UpdateUsers();
                    }
                }
                }
            //message box
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Users Successfully Updated!'); window.location='" + Request.ApplicationPath + "ListOfUsers.aspx';", true);
        }
        //add a blank row
        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            //get columns
            dt.Columns.Add(new DataColumn("first_name", typeof(string)));
            dt.Columns.Add(new DataColumn("last_name", typeof(string)));
            dt.Columns.Add(new DataColumn("username", typeof(string)));
            dt.Columns.Add(new DataColumn("group_access", typeof(string)));
            dt.Columns.Add(new DataColumn("status", typeof(string)));
            dr = dt.NewRow();
            //add blank values
            dr["first_name"] = string.Empty;
            dr["last_name"] = string.Empty;
            dr["username"] = string.Empty;
            dr["group_access"] = string.Empty;
            dr["status"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridUsers.DataSource = dt;
            GridUsers.DataBind();
        }

        //set the values to the gridview
        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridUsers.Rows[rowIndex].Cells[4].FindControl("dropStatus");
                        TextBox box1 = (TextBox)GridUsers.Rows[rowIndex].Cells[0].FindControl("first_name");
                        TextBox box2 = (TextBox)GridUsers.Rows[rowIndex].Cells[1].FindControl("last_name");
                        TextBox box3 = (TextBox)GridUsers.Rows[rowIndex].Cells[2].FindControl("username");
                        TextBox box4 = (TextBox)GridUsers.Rows[rowIndex].Cells[3].FindControl("group_access");

                     

                        itemList.SelectedIndex = itemList.Items.IndexOf(itemList.Items.FindByText(dt.Rows[i]["status"].ToString()));
                        box1.Text = dt.Rows[i]["first_name"].ToString().Trim();
                        box2.Text = dt.Rows[i]["last_name"].ToString().Trim();
                        box3.Text = dt.Rows[i]["username"].ToString().Trim();
                        box4.Text = dt.Rows[i]["group_access"].ToString().Trim();

                        box1.CssClass = "form-control";
                        box2.CssClass = "form-control";
                        box3.CssClass = "form-control";
                        box4.CssClass = "form-control";
                        box1.Enabled = false;
                        box2.Enabled = false;
                        box3.Enabled = false;
                        box4.Enabled = false;

                        rowIndex++;
                    }
                }
            }
         }

        //get the value on the GridUsersViewing
        private void SetPreviousDataForViewing()
        {
            if (ViewState["ViewingTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["ViewingTable"];
                if (dt.Rows.Count > 0)
                {
                    DataTable dt1 = new DataTable();
                    DataRow dr = null;
                    dt1.Columns.Add(new DataColumn("first_name", typeof(string)));
                    dt1.Columns.Add(new DataColumn("last_name", typeof(string)));
                    dt1.Columns.Add(new DataColumn("username", typeof(string)));
                    dt1.Columns.Add(new DataColumn("group_access", typeof(string)));
                    dt1.Columns.Add(new DataColumn("status", typeof(string)));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Insert the rows on Datatable
                        dr = dt1.NewRow();
                        dr["first_name"] = dt.Rows[i]["first_name"].ToString().Trim();
                        dr["last_name"] = dt.Rows[i]["last_name"].ToString().Trim();
                        dr["username"] = dt.Rows[i]["username"].ToString().Trim();
                        dr["group_access"] = dt.Rows[i]["group_access"].ToString().Trim();
                        dr["status"] = dt.Rows[i]["status"].ToString().Trim();
                        dt1.Rows.Add(dr);

                        ViewState["CurrentTable"] = dt1;
                        GridUsers.DataSource = dt1;
                        GridUsers.DataBind();
                    }
                    //Display the value on Gridview
                    SetPreviousData();
                }
            }
        }

        public void UpdateUsers()
        {
            string query = "";
            dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            _SQL.Open();

            foreach (DataRow row in dtCurrentTable.Rows)
            {                
                //update the column bt_activated based on the selected status
                query = query + "UPDATE md_administration SET bt_activated='" + row["status"].ToString() + "' WHERE username='" + row["username"].ToString().Trim() + "';";
            }
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
        }


        //load the users on the GridUserViewing
        public void LoadUsersForViewing()
        {
            string SQL = "select a.*,group_access=b.vc_group_name,status=rtrim(bt_activated) from md_administration a inner join md_menu_group b on a.ch_group_access=b.ch_group_access";
            GridUserViewing.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridUserViewing.DataSource = dt;
                GridUserViewing.DataBind();
                GridUserViewing.Visible = true;
                GridUsers.Visible = false;
                ViewState["ViewingTable"] = dt;
                btnCancel.Visible = false;
            }
            else
            {
                GridUserViewing.Visible = false;
                GridUsers.Visible = true;

                //Add blank row on Gridview
                SetInitialRow();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;

            //Hide Gridview for viewing of products
            GridUserViewing.Visible = false;
            //Show Gridview for adding new products
            GridUsers.Visible = true;
            //Assign data to Gridview from Datatable
            SetPreviousDataForViewing();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListOfUsers.aspx");
        }
    }
}