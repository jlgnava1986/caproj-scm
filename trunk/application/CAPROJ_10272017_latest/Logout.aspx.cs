﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CAPROJ
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["username"] = null;
                Session.Clear();
                Session.Contents.RemoveAll();
                Session.Abandon();
                Response.Redirect("Default.aspx");
            }
        }
    }
}