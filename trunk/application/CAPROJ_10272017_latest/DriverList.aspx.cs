﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class DriverList : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadDriverList(txtFilter.Text);
            }
        }

        public void LoadDriverList(string filter)
        {
            string SQL;
            SQL = "select cnt=ROW_NUMBER() OVER(ORDER BY Driver_ID),* from dbo.md_driver where ('" + filter + "'='') or ('" + filter + "'<>'' and (" + DropDownList1.SelectedValue + " like '%' + '" + filter + "' +'%'))";
            GridDriverList.DataSource = null;
            GridDriverList.DataBind();
            _SQL.getGrid(SQL, GridDriverList);
            GridDriverList.Visible = true;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadDriverList(txtFilter.Text);
        }
    }
}