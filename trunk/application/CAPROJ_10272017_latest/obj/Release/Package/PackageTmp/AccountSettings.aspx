﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AccountSettings.aspx.cs" Inherits="ETIIOnlineServices.AccountSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                User Account
                <small>Account Settings</small>
            </h1>
        </section>

        <section class="content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="box">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-header">
                                    <h4 class="box-title">Change Profile Photo</h4>
                                </div>
                                <div class="row" style="padding:10px 10px 10px 10px">                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" 
                                            CssClass="btn btn-flat btn-success btn-block" CausesValidation="false" 
                                            onclick="btnUpload_Click"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center">
                                        <h5><asp:Label ID="lblNotifProfilePhoto" runat="server" Text=""></asp:Label></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12"><hr></div>
                            <div class="col-xs-12">
                                <div class="box-header">
                                    <h4 class="box-title">Change Password</h4>
                                </div>
                                <div class="row" style="padding:10px 10px 10px 10px">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                        <small>Old Password</small>
                                        <asp:TextBox ID="txtOldPass" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                        <small>New Password</small>
                                        <asp:TextBox ID="txtNewPass" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True" TargetControlID="txtNewPass" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" FilterMode="ValidChars" ValidChars="!@#$%^&*()_+-={}[]:;\<>,.?/"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                        <small>Reenter New Password</small>
                                        <asp:TextBox ID="txtReenterNewPass" runat="server" CssClass="form-control" Enabled="true" TextMode="Password"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True" TargetControlID="txtReenterNewPass" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" FilterMode="ValidChars" ValidChars="!@#$%^&*()_+-={}[]:;\<>,.?/"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                        <small><font color="white">_</font></small>
                                        <asp:Button ID="btnChangePass" runat="server" CausesValidation="False" 
                                            CssClass="btn btn-flat btn-success btn-block" OnClick="btnChangePass_Click"
                                            Text='Change Password'/>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center">
                                        <h5><asp:Label ID="lblNotif" runat="server" Text=""></asp:Label></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />
                <asp:AsyncPostbackTrigger ControlID="btnChangePass" EventName="Click" />
            </Triggers>
          </asp:UpdatePanel>
       </section>
    </aside>
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
</asp:Content>

