﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="UnitInventory.aspx.cs" Inherits="ETIIOnlineServices.UnitInventory" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Unit Inventory/Pricelist</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
            <div class="col-xs-12 no-padding">
                <%--<div class="box" style="padding:10px 10px 10px 10px">--%>
                    <!-- Unit Inventory -->
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="row">
                            <div class="pull-left col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 10px 15px">
                                <asp:DropDownList ID="DropDownProject" runat="server" CssClass="form-control" OnSelectedIndexChanged="OnProjectClick" AutoPostBack="true">
                                    <asp:ListItem Selected="True" Value="VRC">Vivaldi Residences Cubao</asp:ListItem>
                                    <asp:ListItem Value="VRD">Vivaldi Residences Davao</asp:ListItem>
                                    <asp:ListItem Value="MRF">Milan Residences Fairview</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 10px 15px">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding:10px 15px 10px 15px">
                                <asp:Button ID="btnFilter" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-info pull-right" CommandName="Action"
                                    Text='Filter' OnCommand="FilterGridview"/>
                            </div>
                            <div class="pull-right col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding:10px 15px 10px 15px">
                                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                    Text='Back' OnCommand="ShowInventorySummary"/>
                            </div>

<%--                                        <asp:Button ID="btnExportPDF" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                Text='Export To PDF'/>--%>
                        </div>
                            <!-- Open Units Summary Per Type-->
                            <div class="box-body table-responsive no-padding">
                                <asp:GridView ID="Grid" runat="server" AutoGenerateColumns="False" 
                                    GridLines="None"  AllowPaging="false"
                                    OnPageIndexChanging="Grid_PageIndexChanging"
                                    BorderStyle="None" Class="table" AllowSorting="True" ShowFooter="true">
                                    <Columns>
                                            <asp:BoundField DataField="ch_unittype_desc" HeaderText="Unit Type">
                                                <HeaderStyle HorizontalAlign="Left" Width="470px" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="int_open" HeaderText="Available Units">
                                                <HeaderStyle HorizontalAlign="Left" Width="430px" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                            </asp:BoundField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewDetails" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                        Text='View Details' OnCommand="LoadUnitInventoryDetailsPerType"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnExportPDF" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-danger" CommandName="Action"
                                                        Text='Download PDF' OnCommand="DownloadPricelist"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    </Columns>
                                    
                                    <RowStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Bold="true" />
                                    <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                    <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                    <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                </asp:GridView>
                                <!-- Open Units Details Per Type-->                            
                                <asp:GridView ID="GridPricelistDetails" runat="server" AutoGenerateColumns="False" 
                                        GridLines="None" Class="table table-responsive" AllowPaging="true"
                                        OnPageIndexChanging="Grid_PageIndexChanging"              
                                        BorderStyle="None" AllowSorting="True"  PageSize="50">
                                        <Columns>
                                                <asp:BoundField DataField="ch_floor_id" HeaderText="Floor">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="nu_unit_number" HeaderText="Unit No.">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="nu_sqm" HeaderText="Area (sqm)">
                                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="ch_unitmodel_desc" HeaderText="Unit Model">
                                                    <HeaderStyle HorizontalAlign="Left" Width="250px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="ch_orientation_desc" HeaderText="Orientation">
                                                    <HeaderStyle HorizontalAlign="Left" Width="250px" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="mo_lp_with_cwtdisc" HeaderText="Gross List Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="mo_discount" HeaderText="Intro Disc" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>
                                                
                                                <asp:BoundField DataField="mo_discountedlp_" HeaderText="Discounted List Price" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:BoundField DataField="ch_unit_id" HeaderText="Unit ID">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                </asp:BoundField>

                                                <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnCompute" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                        Text='Sample Comp.' OnCommand="RedirectSampleComp"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                        <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                        <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                            </div>
<%--                                 <div class="table-container-footer" runat="server" id="rowFoot">
                                      <table class="table table-condensed">
                                            <tr>
                                              <th>TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;71</th>
                                            </tr>
                                      </table>
                                   </div>
--%>                        </ContentTemplate>
                        <Triggers>
<%--                                <asp:AsyncPostBackTrigger ControlID="ctl00$Dashboard_Portal$Grid$ctl02$btnViewDetails" EventName="Click" />--%>
                        </Triggers>
                        </asp:UpdatePanel>
                <%--</div>--%>
            </div>
        </section>
    </aside>

    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active').find('li').css({ 'display': 'block' });
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(function () {
            "use strict";

            // AREA CHART
            var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666, item2: 2666 },
                        { y: '2011 Q2', item1: 2778, item2: 2294 },
                        { y: '2011 Q3', item1: 4912, item2: 1969 },
                        { y: '2011 Q4', item1: 3767, item2: 3597 },
                        { y: '2012 Q1', item1: 6810, item2: 1914 },
                        { y: '2012 Q2', item1: 5670, item2: 4293 },
                        { y: '2012 Q3', item1: 4820, item2: 3795 },
                        { y: '2012 Q4', item1: 15073, item2: 5967 },
                        { y: '2013 Q1', item1: 10687, item2: 4460 },
                        { y: '2013 Q2', item1: 8432, item2: 5713 }
                    ],
                xkey: 'y',
                ykeys: ['item1', 'item2'],
                labels: ['Item 1', 'Item 2'],
                lineColors: ['#a0d0e0', '#3c8dbc'],
                hideHover: 'auto'
            });

            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                        { y: '2011 Q1', item1: 2666 },
                        { y: '2011 Q2', item1: 2778 },
                        { y: '2011 Q3', item1: 4912 },
                        { y: '2011 Q4', item1: 3767 },
                        { y: '2012 Q1', item1: 6810 },
                        { y: '2012 Q2', item1: 5670 },
                        { y: '2012 Q3', item1: 4820 },
                        { y: '2012 Q4', item1: 15073 },
                        { y: '2013 Q1', item1: 10687 },
                        { y: '2013 Q2', item1: 8432 }
                    ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Item 1'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });

            //DONUT CHART
            var donut = new Morris.Donut({
                element: 'sales-chart',
                resize: true,
                colors: ["#3c8dbc", "#f56954", "#0079AA"],
                data: [
                        { label: "Download Sales", value: 12 },
                        { label: "In-Store Sales", value: 30 },
                        { label: "Mail-Order Sales", value: 20 }
                    ],
                hideHover: 'auto'
            });

            //BAR CHART
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                        { y: '2006', a: 100, b: 90 },
                        { y: '2007', a: 75, b: 65 },
                        { y: '2008', a: 50, b: 40 },
                        { y: '2009', a: 75, b: 65 },
                        { y: '2010', a: 50, b: 40 },
                        { y: '2011', a: 75, b: 65 },
                        { y: '2012', a: 100, b: 90 }
                    ],
                barColors: ['#0079AA', '#f56954'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['CPU', 'DISK'],
                hideHover: 'auto'
            });
        });
        </script>
</asp:Content>
