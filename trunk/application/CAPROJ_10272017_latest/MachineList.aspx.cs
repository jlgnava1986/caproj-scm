﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class MachineList : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadMachineList(txtFilter.Text);
            }
        }

        public void LoadMachineList(string filter)
        {
            string SQL;
            SQL = "select cnt=ROW_NUMBER() OVER(ORDER BY Machine_ID),* from dbo.md_machine where ('" + filter + "'='') or ('" + filter + "'<>'' and (" + DropDownList1.SelectedValue + " like '%' + '" + filter + "' +'%'))";
            GridMachineList.DataSource = null;
            GridMachineList.DataBind();
            _SQL.getGrid(SQL, GridMachineList);
            GridMachineList.Visible = true;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            LoadMachineList(txtFilter.Text);
        }
    }
}