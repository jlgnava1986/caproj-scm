﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="NewsFeed.aspx.cs" Inherits="ETIIOnlineServices.NewsFeed" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
               News Feed
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
     <!-- row -->
      
             <!-- Delete Modal -->
<div class="modal" id="modal_remove" >
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove Post</h4>
      </div>
	
				 <input type="hidden" class="form-control" id="post_id"  value="">
      <div class="modal-body">
  
              <div class="box-body">
		     <div class="modal-body">
      </div> 
	  </div>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <button type="button" class="btn btn-danger" id ="btnRemoveList">Remove</button>
      </div>
    </div>
  </div>
</div>




            <% if (Session["group_id"] != null)
                {
                    if (Session["group_id"].ToString() == "005")
                    { %>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" >
                              <ContentTemplate>
              <div class="row">
        <div class="col-md-12 ">
                 
            <div class="box box-info" >

         <div class="box-body" style="background:#d9defc">
                 <div class="row">
                  <div class="col-md-12"  >
                    <div class="form-group" >
                        <asp:textbox runat="server" rows="4" TextMode="multiline" ID="post_message"  class="form-control" />  
                 
                    </div>
                   </div>
                    </div>
                  <div class="row">
         <div class="col-md-3"  >

              
   <div class="fileupload fileupload-new" data-provides="fileupload">
    <span class="btn btn-primary btn-file"><span class="fileupload-new">Upload Image</span>
    <span class="fileupload-exists">Change</span>       <asp:FileUpload ID="FileUpload1" runat="server"  accept="image/*" /></span>
    <span class="fileupload-preview"></span>
    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
       <asp:Label ID="lblNotifProfilePhoto" runat="server" Text=""></asp:Label>
  </div>
                              </div>
   <div class="col-md-4"  >
      
  <!--<label for="usr" class="d-inline">Youtube URL:</label>-->
     <asp:textbox runat="server" class="form-control" id="post_video_url" placeholder="Enter Youtube link here..."/>



</div>
        <div class="col-md-1"  >
                      
 <p>Post by:</p>
</div>
                 <div class="col-md-3">
                      

            <asp:DropDownList id="posted_by"  class="form-control"

                    runat="server">
                    <asp:ListItem Text="Euro Towers" Value="Euro Towers" />
                    <asp:ListItem Text="Vivaldi Residences Davao" Value="Vivaldi Residences Davao" />
                    <asp:ListItem Text="Vivaldi Residences Cubao" Value="Vivaldi Residences Davao" />
                    <asp:ListItem Text="Milan Residenze Fairview" Value="Milan Residenze Fairview" />
           </asp:DropDownList>

                        
    </div>

         <!--    <button class="btn btn-default d-inline"><i class="fa fa-bullhorn"></i></button>
             <button class="btn btn-default d-inline"><i class="fa fa-bullhorn"></i></button>-->
                    
                  <div class="col-md-1"  >
               

           <asp:Button ID="btnsave_post" runat="server" CausesValidation="False"   class="btn btn-success pull-right "  onclick="btnsave_post_Click" text="Post"  PostBackUrl="~/NewsFeed.aspx"/>
                    
                                   
                        </div>
                        </div>
            </div>
                 
            </div>
         
              
             </div>
        </div>        
        </ContentTemplate>
                                   </asp:UpdatePanel>
              <% }
                  } %>

                   <div class="row">
        <div class="col-md-12">
       
       
          <asp:Repeater ID="rptNewsFeed" runat="server">
            <ItemTemplate>

          <!-- The time line -->
          
        <div >
            
          <ul class="timeline" id="news_feed">

         <li id ="feed_body">
           <!--       <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qXlqfyOcxeg" frameborder="0" allowfullscreen></iframe>
                   
                  </div>-->
             </li>


            
          </ul>
            <div style="position: absolute; 
bottom: 0;">
                <img id="loader" alt="" src="img/loading.gif" style="display: none" /></div>
        </div>
                  </ItemTemplate>
        </asp:Repeater>
                
           









        </div>


       
      </div>


                             

        <!-- /.col -->
      <!-- /.row -->

        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="js/jquery.timeago.js" type="text/javascript"></script>
     <script src="js/Autolinker.min.js" type="text/javascript"></script>
     <script src="js/jquery.formatDateTime.js" type="text/javascript"></script>


    <script  type="text/javascript">
        ! function (e) {
            var t = function (t, n) {
                this.$element = e(t), this.type = this.$element.data("uploadtype") || (this.$element.find(".thumbnail").length > 0 ? "image" : "file"), this.$input = this.$element.find(":file");
                if (this.$input.length === 0) return;
                this.name = this.$input.attr("name") || n.name, this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]'), this.$hidden.length === 0 && (this.$hidden = e('<input type="hidden" />'), this.$element.prepend(this.$hidden)), this.$preview = this.$element.find(".fileupload-preview");
                var r = this.$preview.css("height");
                this.$preview.css("display") != "inline" && r != "0px" && r != "none" && this.$preview.css("line-height", r), this.original = {
                    exists: this.$element.hasClass("fileupload-exists"),
                    preview: this.$preview.html(),
                    hiddenVal: this.$hidden.val()
                }, this.$remove = this.$element.find('[data-dismiss="fileupload"]'), this.$element.find('[data-trigger="fileupload"]').on("click.fileupload", e.proxy(this.trigger, this)), this.listen()
            };
            t.prototype = {
                listen: function () {
                    this.$input.on("change.fileupload", e.proxy(this.change, this)), e(this.$input[0].form).on("reset.fileupload", e.proxy(this.reset, this)), this.$remove && this.$remove.on("click.fileupload", e.proxy(this.clear, this))
                },
                change: function (e, t) {
                    if (t === "clear") return;
                    var n = e.target.files !== undefined ? e.target.files[0] : e.target.value ? {
                        name: e.target.value.replace(/^.+\\/, "")
                    } : null;
                    if (!n) {
                        this.clear();
                        return
                    }
                    this.$hidden.val(""), this.$hidden.attr("name", ""), this.$input.attr("name", this.name);
                    if (this.type === "image" && this.$preview.length > 0 && (typeof n.type != "undefined" ? n.type.match("image.*") : n.name.match(/\.(gif|png|jpe?g)$/i)) && typeof FileReader != "undefined") {
                        var r = new FileReader,
                            i = this.$preview,
                            s = this.$element;
                        r.onload = function (e) {
                            i.html('<img src="' + e.target.result + '" ' + (i.css("max-height") != "none" ? 'style="max-height: ' + i.css("max-height") + ';"' : "") + " />"), s.addClass("fileupload-exists").removeClass("fileupload-new")
                        }, r.readAsDataURL(n)
                    } else this.$preview.text(n.name), this.$element.addClass("fileupload-exists").removeClass("fileupload-new")
                },
                clear: function (e) {
                    this.$hidden.val(""), this.$hidden.attr("name", this.name), this.$input.attr("name", "");
                    if (navigator.userAgent.match(/msie/i)) {
                        var t = this.$input.clone(!0);
                        this.$input.after(t), this.$input.remove(), this.$input = t
                    } else this.$input.val("");
                    this.$preview.html(""), this.$element.addClass("fileupload-new").removeClass("fileupload-exists"), e && (this.$input.trigger("change", ["clear"]), e.preventDefault())
                },
                reset: function (e) {
                    this.clear(), this.$hidden.val(this.original.hiddenVal), this.$preview.html(this.original.preview), this.original.exists ? this.$element.addClass("fileupload-exists").removeClass("fileupload-new") : this.$element.addClass("fileupload-new").removeClass("fileupload-exists")
                },
                trigger: function (e) {
                    this.$input.trigger("click"), e.preventDefault()
                }
            }, e.fn.fileupload = function (n) {
                return this.each(function () {
                    var r = e(this),
                        i = r.data("fileupload");
                    i || r.data("fileupload", i = new t(this, n)), typeof n == "string" && i[n]()
                })
            }, e.fn.fileupload.Constructor = t, e(document).on("click.fileupload.data-api", '[data-provides="fileupload"]', function (t) {
                var n = e(this);
                if (n.data("fileupload")) return;
                n.fileupload(n.data());
                var r = e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');
                r.length > 0 && (r.trigger("click.fileupload"), t.preventDefault())
            })
        }(window.jQuery)

    </script>

      <!-- ViewBox -->
        <script src="js/jquery.viewbox.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        var prevdate = "";
        var currentdate = "";
        var isLastPost = true;
        $j(document).ready(function () {



            
            $('#modal_remove').on('show.bs.modal', function (event) { // id of the modal with event
	
                var button = $(event.relatedTarget) // Button that triggered the modal
                var post_id = button.data('post_id') // Extract info from data-* attributes
              //  var item_name = button.data('item_name')

                var modal = $(this)
                modal.find('.modal-body').text("Do you want to remove this post?" )

                $('#modal_remove').find('input#post_id').val(post_id)

            })
	
	
            $( "#btnRemoveList" ).click(function() {
                // Button that triggered the modal
              
                var post_id = $('#post_id').val() // Extract info from data-* attributes
             //   alert(post_id)


                $.post("NewsFeed.aspx", { post_id: post_id })
             //   alert(post_id)
                  .done(function( data ) {
                 //     alert(data)
                     // if(data==""){
                          $('#modal').modal('toggle');
                          window.location.replace("NewsFeed.aspx");

                     // }else{
                     //     alert (data )
                     // }
                  })
                  .fail(function(response) {

                      alert (response.responseText )

                  });

            });
  
  

    
/*
            $("#btn_post").click(function () {
           
            var post_message = $("#post_message").val();
            var post_message = $("#post_video_url").val();
            var post_message = $("#post_image_url").val();
            var post_category =1;
            var post_message = $("#posted_by").val();

                $.ajax({
                    type: "POST",
                    url: "NewsFeed.aspx/GetNewsFeed",
                    data: '{post_message: ' + pageIndex + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });

            });
            */






            function getId(url) {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var match = url.match(regExp);

                if (match && match[2].length == 11) {
                    return match[2];
                } else {
                    return 'error';
                }
            }

         //   var myId = getId('https://youtu.be/PMivT7MJ41M');


            var autolinker = new Autolinker({
                urls: {
                    schemeMatches: true,
                    wwwMatches: true,
                    tldMatches: true
                },
                email: true,
                phone: true,
                mention: 'twitter',
                hashtag: 'facebook',

                stripPrefix: true,
                stripTrailingSlash: true,
                newWindow: true,

                truncate: {
                    length: 50,
                    location: 'smart'
                },

                className: ''
            });

            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
            var pageIndex = 0;
            var pageCount=1;
            GetRecords();



   
           // alert(autolinker.link("http://gregjacobs.github.io/Autolinker.js/examples/live-example/"))
              

    
            $(window).scroll(function () {
                if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                    GetRecords();
                }
            });
            function GetRecords() {
                pageIndex++;
                console.log(pageCount);
                if (pageIndex == 2 || pageIndex <= pageCount) {
                  //  alert(pageIndex)
                    $("#loader").show();
                    $.ajax({
                        type: "POST",
                        url: "NewsFeed.aspx/GetNewsFeed",
                        data: '{pageIndex: ' + pageIndex + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.d);
                        },
                        error: function (response) {
                            alert(response.d);
                        }
                    });
                } else {
                  ///  alert(isLastPost)
                    if (isLastPost == true) {
                       
                        $("#news_feed").append('<li class="time-label"> <span class="bg-olive">No more post to display </span></li>');
                        isLastPost = false;
                    }
                  
                }
            }
            function OnSuccess(response) {
         
                var xmlDoc = $.parseXML(response.d);
          
                var xml = $(xmlDoc);
    
                pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
             
                var customers = xml.find("mf_news_feed");
              
                console.log(customers);




                customers.each(function () {
                    var newsfeed = $(this);


                    var ul = $("#news_feed ul").eq(0).clone(true);
                  
                    currentdate = $.datepicker.formatDate('MM dd, yy', new Date( newsfeed.find("sd_date_created").text()));
                  


                   // alert(dateFormat)
                    var feed = (currentdate!=prevdate ? '<li class="time-label"> <span class="bg-red">' + currentdate +' </span></li> ':'') +
                     ' <li><i class="' + newsfeed.find("vc_post_category_icon").text() + '"></i>' +

                     ' <div class="timeline-item">' + 
                        '<span class="time"><i class="fa fa-clock-o"></i><time class="timeago" style="padding-left:10px" datetime="' + newsfeed.find("sd_date_created").text() + '"> ' + $j.formatDateTime('MM dd, yy g:ii a', new Date(newsfeed.find("sd_date_created").text())) + '</time> </span>  ' +

                        '<h3 class="timeline-header">Posted by <strong>' + newsfeed.find("vc_posted_by").text() + '</strong> ' + ('<%= Session["group_id"].ToString() %>' =='005' ? '<a href="#"  title="Remove" data-toggle="modal" data-target="#modal_remove" data-post_id="' + newsfeed.find("int_post_id").text() + '" > <i class="glyphicon glyphicon-remove text-danger" ></i></a> ' : '') + '</h3> ' +

                       ' <div class="timeline-body"><p class="lead">' + autolinker.link(newsfeed.find("vc_post_message").text()) + ' </br></br> ' + (newsfeed.find("vc_post_video_url").text() != '' && newsfeed.find("vc_post_image_url").text() != '' ? autolinker.link(newsfeed.find("vc_post_video_url").text()) + '</br></br>' : '') + '</p> ' +
                       (newsfeed.find("vc_post_video_url").text() != '' && newsfeed.find("vc_post_image_url").text() == '' ? '    <div class="embed-responsive embed-responsive-16by9"> ' +
                    '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + getId(newsfeed.find("vc_post_video_url").text()) + '" frameborder="0" allowfullscreen></iframe> ' +
                  '</div>' : (newsfeed.find("vc_post_image_url").text() != '' ? '<a href="images/post_photos/' + newsfeed.find("vc_post_image_url").text() + '" class="image-link"  ><img src="images/post_photos/' + newsfeed.find("vc_post_image_url").text() + '" class="img-responsive img-rounded"  alt="Sellers Portal News Feed" style=" max-height:436px ; max-width: 504px;"></a>' : '')) + '</div></div></li>';


                    $("#news_feed").append(feed);
                    cdate = $.datepicker.formatDate('MM dd, yy', new Date());
                    if (currentdate == cdate) {
                        $j("time.timeago").timeago();
                    }
                  
                
                  


                    prevdate = currentdate;
                });
                $("#loader").hide();
                $j('.image-link').viewbox({
                    setTitle: true,
                    margin: 20,
                    resizeDuration: 300,
                    openDuration: 200,
                    closeDuration: 200,
                    closeButton: true,
                    navButtons: false,
                    closeOnSideClick: true,
                    nextOnContentClick: false
                });

                //$('[data-toggle="tooltip"]').tooltip();
            }
        });
    </script>
</asp:Content>

