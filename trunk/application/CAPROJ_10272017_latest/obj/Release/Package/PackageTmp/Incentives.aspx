﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Incentives.aspx.cs" Inherits="ETIIOnlineServices.Incentives" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <!-- Left side column. contains the logo and sidebar -->
<%--    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <br/>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="img/avatar2.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><asp:Label ID="lblUserAlias" runat="server" Text=""></asp:Label></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <ul class="sidebar-menu">
                <asp:PlaceHolder id="LocalPlaceHolder" runat="server"></asp:PlaceHolder>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
--%>
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Commission Ledger</small>
            </h1>
        </section>

        <section class="content">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" role="tablist">
                        <!-- Tab 1 Header -->
                        <li class="active"><a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">Incentives</a></li>
                        <!-- Tab 2 Header -->
<%--                        <li><a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">Adjustments</a></li>--%>
                        <!-- Tab 3 Header -->
<%--                        <li><a href="#tab_3" aria-controls="tab_3" role="tab" data-toggle="tab">Sales/Cancellation</a></li>
                        <li><a href="#tab_4" aria-controls="tab_4" role="tab" data-toggle="tab">Accreditation History</a></li>--%>
                        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- Tab 1 Details Commission -->
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                        <div class="col-xs-12">
                                <div class="box-header">
                                    <h3 class="box-title">Incentives</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead style="background-color:#0079AA; color:White;">
                                            <tr>
                                                <th>Client Name</th>
                                                <th>Unit ID</th>
                                                <th>Rate</th>
                                                <th>TCP</th>
                                                <th>Gross Commission</th>
                                                <th>Released Commission</th>
                                                <th>Balance Commission</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Client Name 1</td>
                                                <td>SAMPLEUNIT 1</td>
                                                <td>1.00</td>
                                                <td>2,358,678.60</td>
                                                <td>23,586.79</td>
                                                <td>19,401.35</td>
                                                <td>2,358.68</td>
                                            </tr>
                                            <tr>
                                                <td>Client Name 2</td>
                                                <td>SAMPLEUNIT 2</td>
                                                <td>1.00</td>
                                                <td>2,358,678.60</td>
                                                <td>23,586.79</td>
                                                <td>19,401.35</td>
                                                <td>2,358.68</td>
                                            </tr>
                                            <tr>
                                                <td>Client Name 3</td>
                                                <td>SAMPLEUNIT 3</td>
                                                <td>1.00</td>
                                                <td>2,358,678.60</td>
                                                <td>23,586.79</td>
                                                <td>19,401.35</td>
                                                <td>2,358.68</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- /.box -->
                        </div>
                    </div>
                        </div>

                        <!-- Tab 2 Details Unit Inventory -->
<%--                        <div class="tab-pane" id="tab_2">

                        </div>--%>

                        <!-- Tab 3 - Availablity Map -->
<%--                        <div class="tab-pane" id="tab_3">

                        </div>

                        <div class="tab-pane" id="tab_4">

                        </div>--%>
                    </div>
                </div>        
            </div>
        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</asp:Content>

