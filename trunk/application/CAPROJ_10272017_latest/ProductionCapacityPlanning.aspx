﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProductionCapacityPlanning.aspx.cs" Inherits="CAPROJ.ProductionCapacityPlanning" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">    
        <section class="content-header">
            <h1>
                Production
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <small>Production No.</small>
                                        <asp:DropDownList ID="dropProduction" runat="server" CssClass="form-control" 
                                            AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="dropFinishedGoods_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <small>Finished Goods Name</small>
                                        <asp:DropDownList ID="dropFinishedGoods" runat="server" CssClass="form-control" 
                                            AutoPostBack="true" AppendDataBoundItems="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <small>Cycle Time</small>
                                        <asp:TextBox ID="txtCycleTime" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Gridview for adding new record -->
                                        <div class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridSchedule" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true" ShowFooter="true"
                                                BorderStyle="None" Class="table" AllowSorting="True">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Raw Materials">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="dropMaterials" runat="server" CssClass="form-control" 
                                                                AppendDataBoundItems="true">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <FooterTemplate>
                                                            <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" CssClass="btn btn-sm btn-info" OnClick="ButtonAdd_Click"/>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="white" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>
                                        <!-- Gridview for viewing records -->
                                        <div class="box-body table-responsive no-padding">
                                            <asp:GridView ID="GridScheduleViewing" runat="server" AutoGenerateColumns="False" 
                                                GridLines="None"  AllowPaging="true" ShowHeaderWhenEmpty="true" ShowHeader="true"
                                                BorderStyle="None" Class="table" AllowSorting="True">
                                                <Columns>
                                                    <asp:BoundField DataField="material_name" HeaderText="Raw Materials">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                                    </asp:BoundField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="white" />
                                                <HeaderStyle BackColor="#b01717" Font-Bold="True" ForeColor="White"/>
                                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                <SortedDescendingHeaderStyle BackColor="#575357" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnCancel" Text='Cancel' BackColor="#b01717" OnClick="btnCancel_Click"></asp:Button>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnAdd" Text='Add' BackColor="#b01717" OnClick="btnAdd_Click"></asp:Button>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnEdit" Text='Edit' BackColor="#b01717" OnClick="btnEdit_Click"></asp:Button>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" class="btn btn-block btn-danger" ID="btnSave" Text='Save' BackColor="#b01717" OnClick="btnSave_Click"></asp:Button>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div> <!-- /.row -->
                    </div>        
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostbackTrigger ControlID="btnEdit" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </aside>
</asp:Content>
