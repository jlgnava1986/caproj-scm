﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccessDenied.aspx.cs" Inherits="CAPROJ.AccessDenied" Debug="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>U&K - UPVC Material</title>
    <link rel="SHORTCUT ICON" href="images/favicon.ico"/>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="CssLogin/style.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/jQuery-2.2.0.min.js" type="text/javascript"></script>
</head>
<body style="overflow:hidden;background-color:white;">
    <div class="wrapper"   >           
        <form id="Form1" runat="server">
            
            <center style="padding-top:20px">
                <img src="images/stop.png" width="25%" height="25%">
                <br/>
                <font color="#545454" size="7px" style="text-shadow: 0px 2px 2px gray">Access Denied/Forbidden</font>
                <br/>
                <font color="#545454" size="4px">
                    The page or resource you were trying to access is absolutely forbidden for some reason
                </font>
                <br/><br/>
                <asp:LinkButton ID="lnkBacktoLoginPage" runat="server" Font-Bold="true" Font-Underline="true" ForeColor="#0099cc" Font-Size="Medium" OnClick="lnkBacktoLoginPage_Click">← Back to Login Page</asp:LinkButton>
            </center>
            <br/>

            <div class="push"></div>
        </form>
    </div>
</body>
</html>
