﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="CAPROJ.Profile" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">    
            <section class="content-header">
                <h1>
                    Update Profile
                </h1>
            </section>

            <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9"> 
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-box pull-left" id="login-box">
                        <div class="form-group">
                             <small>First Name</small>
                              <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <small>Last Name</small>
                            <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox> 
                         </div>
                        <div class="form-group">
                            <small>Department</small>
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                <asp:ListItem Value="Purchasing">Purchasing </asp:ListItem>
                                <asp:ListItem Value="Sales">Sales</asp:ListItem>
                                <asp:ListItem Value="Inventory">Inventory</asp:ListItem>
                                <asp:ListItem Value="Warehouse">Warehouse</asp:ListItem>
                                <asp:ListItem Value="Production">Production</asp:ListItem>
                                <asp:ListItem Value="Accounting">Accounting</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <small>Username</small>
                            <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox> 
                         </div>
                        <div class="form-group">
                            <small>Password</small>
                            <asp:TextBox ID="TextBox5" runat="server" class="form-control" TextMode ="Password"></asp:TextBox> 
                        </div>
                         <div class="form-group">
                            <asp:Label ID="lblmessage" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
                         </div>
                           <div class="form-group">
                                <asp:Button ID="Button1" runat="server" class="btn btn-block" OnClick="Button1_Click"
                                     Text="Update" BackColor="#b01717" ForeColor="White"></asp:Button>
                            </div>

                    </div>
                </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                </Triggers>
                </asp:UpdatePanel>
            </section>
    </aside>
</asp:Content>
