﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class MachineCapacityPlanning : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadScheduleForViewing();
                GridSchedule.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (GridSchedule.Rows.Count > 0)
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    DataTable dtNewTable = new DataTable();
                    dtNewTable.Columns.Add(new DataColumn("Machine_ID", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("machine_name", typeof(string)));
                    dtNewTable.Columns.Add(new DataColumn("design_capacity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("effective_capacity", typeof(int)));
                    dtNewTable.Columns.Add(new DataColumn("machine_status", typeof(string)));
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            DropDownList dropItems = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMachine");
                            TextBox box1 = (TextBox)GridSchedule.Rows[rowIndex].Cells[1].FindControl("machine_name");
                            TextBox box2 = (TextBox)GridSchedule.Rows[rowIndex].Cells[2].FindControl("design_capacity");
                            TextBox box3 = (TextBox)GridSchedule.Rows[rowIndex].Cells[3].FindControl("effective_capacity");
                            TextBox box4 = (TextBox)GridSchedule.Rows[rowIndex].Cells[4].FindControl("machine_status");

                            drCurrentRow = dtNewTable.NewRow();

                            drCurrentRow["Machine_ID"] = dropItems.SelectedItem.Text.ToString();
                            drCurrentRow["machine_name"] = box1.Text;
                            drCurrentRow["design_capacity"] = box2.Text;
                            drCurrentRow["effective_capacity"] = box3.Text;
                            drCurrentRow["machine_status"] = box4.Text;
                            dtNewTable.Rows.Add(drCurrentRow);

                            rowIndex++;
                        }
                        ViewState["CurrentTable"] = dtNewTable;
                        InsertSchedule();
                    }
                }
                }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Machine Successfully Added!'); window.location='" + Request.ApplicationPath + "MachineCapacityPlanning.aspx';", true);
        }
        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Machine_ID", typeof(int)));
            dt.Columns.Add(new DataColumn("machine_name", typeof(string)));
            dt.Columns.Add(new DataColumn("design_capacity", typeof(int)));
            dt.Columns.Add(new DataColumn("effective_capacity", typeof(int)));
            dt.Columns.Add(new DataColumn("machine_status", typeof(string)));
            dr = dt.NewRow();
            dr["Machine_ID"] = 0;
            dr["machine_name"] = string.Empty;
            dr["design_capacity"] = 0;
            dr["effective_capacity"] = 0;
            dr["machine_status"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridSchedule.DataSource = dt;
            GridSchedule.DataBind();

            TextBox machineName = (TextBox)GridSchedule.Rows[0].Cells[1].FindControl("machine_name");
            machineName.Enabled = false;
            machineName.CssClass = "form-control";
            DropDownList dropItemList = (DropDownList)GridSchedule.Rows[0].Cells[1].FindControl("dropMachine");
            dropItemList.CssClass = "form-control";
            _SQL.getDropdown("select * from md_machine", "machine_name", "Machine_ID", dropItemList);
        }

        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMachine");
                        TextBox box1 = (TextBox)GridSchedule.Rows[rowIndex].Cells[1].FindControl("machine_name");
                        TextBox box2 = (TextBox)GridSchedule.Rows[rowIndex].Cells[2].FindControl("design_capacity");
                        TextBox box3 = (TextBox)GridSchedule.Rows[rowIndex].Cells[3].FindControl("effective_capacity");
                        TextBox box4 = (TextBox)GridSchedule.Rows[rowIndex].Cells[4].FindControl("machine_status");

                        drCurrentRow = dtCurrentTable.NewRow();

                        box1.Enabled = false;
                        box1.CssClass = "form-control";

                        dtCurrentTable.Rows[i - 1]["Machine_ID"] = itemList.SelectedItem.Text.ToString().Trim();
                        dtCurrentTable.Rows[i - 1]["machine_name"] = box1.Text;
                        dtCurrentTable.Rows[i - 1]["design_capacity"] = box2.Text;
                        dtCurrentTable.Rows[i - 1]["effective_capacity"] = box3.Text;
                        dtCurrentTable.Rows[i - 1]["machine_status"] = box4.Text;

                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    GridSchedule.DataSource = dtCurrentTable;
                    GridSchedule.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMachine");
                        TextBox box1 = (TextBox)GridSchedule.Rows[rowIndex].Cells[1].FindControl("machine_name");
                        TextBox box2 = (TextBox)GridSchedule.Rows[rowIndex].Cells[2].FindControl("design_capacity");
                        TextBox box3 = (TextBox)GridSchedule.Rows[rowIndex].Cells[3].FindControl("effective_capacity");
                        TextBox box4 = (TextBox)GridSchedule.Rows[rowIndex].Cells[4].FindControl("machine_status");
                        _SQL.getDropdown("select * from md_machine", "machine_name", "Machine_ID", itemList);

                        itemList.SelectedIndex = itemList.Items.IndexOf(itemList.Items.FindByText(dt.Rows[i]["Machine_ID"].ToString()));
                        box1.Text = dt.Rows[i]["machine_name"].ToString().Trim();
                        box1.CssClass = "form-control";
                        box2.Text = dt.Rows[i]["design_capacity"].ToString();
                        box3.Text = dt.Rows[i]["effective_capacity"].ToString();
                        box4.Text = dt.Rows[i]["machine_status"].ToString();
                        rowIndex++;
                    }
                }
            }
         }

        private void SetPreviousDataForViewing()
        {
            if (ViewState["ViewingTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["ViewingTable"];
                if (dt.Rows.Count > 0)
                {
                    DataTable dt1 = new DataTable();
                    DataRow dr = null;
                    dt1.Columns.Add(new DataColumn("Machine_ID", typeof(int)));
                    dt1.Columns.Add(new DataColumn("machine_name", typeof(string)));
                    dt1.Columns.Add(new DataColumn("design_capacity", typeof(int)));
                    dt1.Columns.Add(new DataColumn("effective_capacity", typeof(int)));
                    dt1.Columns.Add(new DataColumn("machine_status", typeof(string)));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Insert the rows on Datatable
                        dr = dt1.NewRow();
                        dr["Machine_ID"] = Convert.ToInt32(dt.Rows[i]["Machine_ID"].ToString().Trim());
                        dr["machine_name"] = dt.Rows[i]["machine_name"];
                        dr["design_capacity"] = Convert.ToInt32(dt.Rows[i]["design_capacity"]);
                        dr["effective_capacity"] = Convert.ToInt32(dt.Rows[i]["effective_capacity"]);
                        dr["machine_status"] = dt.Rows[i]["machine_status"].ToString().Trim();
                        dt1.Rows.Add(dr);

                        ViewState["CurrentTable"] = dt1;
                        GridSchedule.DataSource = dt1;
                        GridSchedule.DataBind();
                    }
                    //Display the value on Gridview
                    SetPreviousData();
                }
            }
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        public void InsertSchedule()
        {
            string query = "";
            dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            _SQL.Open();

            query = "DELETE FROM md_machine_capacity;";
            foreach (DataRow row in dtCurrentTable.Rows)
            {                
                if (row["Machine_ID"].ToString() != "")
                {
                    query = query + "INSERT INTO md_machine_capacity(Machine_ID,design_capacity,effective_capacity,machine_status) VALUES('" + row["Machine_ID"].ToString() + "','" + row["design_capacity"].ToString() + "','" + row["effective_capacity"].ToString() + "','" + row["machine_status"].ToString() + "');";
                }
            }
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();
        }


        //Load Schedule Details for Viewing and Editing
        public void LoadScheduleForViewing()
        {
            //Load Supplier Products
            string SQL = "select a.*,b.machine_name from md_machine_capacity a inner join md_machine b on a.Machine_ID=b.Machine_ID";
            GridScheduleViewing.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridScheduleViewing.DataSource = dt;
                GridScheduleViewing.DataBind();
                GridScheduleViewing.Visible = true;
                GridSchedule.Visible = false;
                ViewState["ViewingTable"] = dt;
                btnCancel.Visible = false;
            }
            else
            {
                GridScheduleViewing.Visible = false;
                GridSchedule.Visible = true;

                //Add blank row on Gridview
                SetInitialRow();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;

            //Hide Gridview for viewing of products
            GridScheduleViewing.Visible = false;
            //Show Gridview for adding new products
            GridSchedule.Visible = true;
            //Assign data to Gridview from Datatable
            SetPreviousDataForViewing();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("MachineCapacityPlanning.aspx");
        }

        protected void dropMachine_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList selected = (DropDownList)sender;
            GridViewRow grdrDropDownRow = ((GridViewRow)selected.Parent.Parent);
            TextBox machineName = (TextBox)grdrDropDownRow.FindControl("machine_name");

            machineName.Text = selected.SelectedValue.ToString().Trim();
        }
    }
}