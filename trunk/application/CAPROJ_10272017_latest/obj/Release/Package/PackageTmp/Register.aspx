﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ETIIOnlineServices.Register" Debug="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>EDMS - Online Services</title>
    <link rel="SHORTCUT ICON" href="images/favicon.ico"/>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="CssLogin/style.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="JScript/bootstrap.js" type="text/javascript"></script>
        <script src="JScript/bootstrap.min.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <script src="JScript/jslink.js" type="text/javascript"></script>
        <script src="JScript/JQuery-v1.4.4.js" type="text/javascript"></script>
        <script src="JScript/JFlash.js" type="text/javascript"></script>
        <link href="bootstrap-3.2.0-dist/css/bootstrap.css" rel=Stylesheet>
</head>
<body class="bg-white">
    <div class="form-box" id="login-box">
        <div class="header bg-blue">Register New Membership</div>
        <form runat="server">
            <div class="body bg-gray">
                <div class="form-group">
                    <center><font color="red" style="font-weight:bold"><asp:Label ID="lblWarning" runat="server"></asp:Label></font></center>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtFullName" class="form-control" placeholder="Full name" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtUserID" class="form-control" placeholder="Username" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtPassword" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtConfirmPassword" class="form-control" placeholder="Retype Password" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtCellNo" class="form-control" placeholder="Phone No."></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtEmailAddress" class="form-control" placeholder="Email Address" AutoPostBack="true" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="footer">
                <asp:button runat="server" ID="btnSubmit" class="btn bg-blue btn-block" 
                    Text="Sign me up" onclick="btnSubmit_Click"></asp:button>
                <center><a href="Default.aspx" class="text-center">I already have a membership</a></center>            
            </div>           
        </form>
    </div>
    <br/></br/>
    <div class="footer">
        <div style="background:rgb(ff,0,0); background:rgba(0,0,0,.50); padding-top:5px; padding-bottom:5px;">
            <center>
                <p><font color="white">Copyright © Euro Towers International, Inc.</font><br/>
                <font color="white">All Rights Reserved.</font></p>
            </center>
        </div>
    </div>
</body>
</html>
