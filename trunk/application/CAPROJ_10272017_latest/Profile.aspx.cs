﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAPROJ.Modules;

namespace CAPROJ
{
    public partial class Profile : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        WebFunctions.WebFunctions func = new WebFunctions.WebFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SqlDataReader rd = _SQL.getRead("SELECT fullname=first_name + ' ' + last_name,first_name,last_name,department=rtrim(department),vc_group_name FROM md_administration a inner join md_menu_group b on a.ch_group_access=b.ch_group_access WHERE username='" + Session["username"].ToString() + "'");
                if (rd.HasRows)
                {
                    rd.Read();
                    Session["validFullName"] = rd[0].ToString();
                    Session["validFirstName"] = rd[1].ToString();
                    Session["validLastName"] = rd[2].ToString();
                    Session["validDepartment"] = rd[3].ToString();
                    Session["groupAccess"] = rd[4].ToString();
                }
                TextBox4.Text = Session["username"].ToString();
                TextBox1.Text = Session["validFirstName"].ToString();
                TextBox2.Text = Session["validLastName"].ToString();
                DropDownList1.SelectedValue = Session["validDepartment"].ToString();

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                lblmessage.Text = "All fields are required!";
            }
            if (TextBox2.Text == "")
            {
                lblmessage.Text = "All fields are required!";
            }
            if (DropDownList1.SelectedValue == "")
            {
                lblmessage.Text = "All fields are required!";
            }
            if (TextBox4.Text == "")
            {
                lblmessage.Text = "All fields are required!";
            }
            if (TextBox5.Text == "")
            {
                lblmessage.Text = "All fields are required!";
            }

            if (TextBox1.Text != "" && TextBox2.Text != "" && DropDownList1.SelectedValue != "" && TextBox4.Text != "" && TextBox5.Text != "" )
            {
                _SQL.Open();
                _SQL.AddParameters("firstname", TextBox1.Text.Trim());
                _SQL.AddParameters("lastname", TextBox2.Text.Trim());
                _SQL.AddParameters("department", DropDownList1.SelectedValue);
                _SQL.AddParameters("userID", TextBox4.Text.Trim());
                _SQL.AddParameters("password", func.EncryptData(TextBox5.Text.Trim(), "", "", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256));
                _SQL.ExecuteNonQuery("UPDATE md_administration SET first_name=@firstname,last_name=@lastname,department=@department,username=@userID,password=@password where username='" + Session["username"].ToString() + "'");
                _SQL.Close();
                Session["username"] = TextBox4.Text.Trim();
                lblmessage.Text = "Your profile is updated!";
                lblmessage.ForeColor = System.Drawing.Color.Blue;
              }
        }
    }
}