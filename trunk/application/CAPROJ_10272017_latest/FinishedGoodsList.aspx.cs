﻿using CAPROJ.Modules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CAPROJ
{
    public partial class FinishedGoodsList : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                loadfinishedgoods_list();
                LoadReport();
            }
        }
        public void loadfinishedgoods_list()
        {
            string SQL = "select * from dbo.md_view_master_data_finished_goods";
            GridFGList.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridFGList.DataSource = dt;
                GridFGList.DataBind();
                ViewState["ViewingTable"] = dt;
            }
        }
        protected void GridFGList_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(ViewState["ViewingTable"]);
            {
                string SortDir = string.Empty;
                if (dir == System.Web.UI.WebControls.SortDirection.Ascending)
                {
                    dir = System.Web.UI.WebControls.SortDirection.Descending;
                    SortDir = "Desc";
                }
                else
                {
                    dir = System.Web.UI.WebControls.SortDirection.Ascending;
                    SortDir = "Asc";
                }
                DataView sortedView = new DataView(dt);
                sortedView.Sort = e.SortExpression + " " + SortDir;
                GridFGList.DataSource = sortedView;
                GridFGList.DataBind();
            }
        }

        public System.Web.UI.WebControls.SortDirection dir
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = System.Web.UI.WebControls.SortDirection.Ascending;
                }
                return (System.Web.UI.WebControls.SortDirection)ViewState["dirState"];
            }

            set
            {
                ViewState["dirState"] = value;
            }
        }
        public void LoadReport()
        {
            ReportDocument reportviewer = new ReportDocument();
            FormulaFieldDefinitions pubFormulaFields;
            FormulaFieldDefinition pubFormulaField;

            ConnectionInfo Connection_Info = new ConnectionInfo();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            Tables CrTables;

            string[] strFormulaParam = { "printedby" };
            string[] strFormulaValue = { "'" + Session["fullname"].ToString() + "'" };
            reportviewer.Load(Server.MapPath("Report/FinishedGoodsList.rpt"));

            Connection_Info.ServerName = "LAPTOP-AI3BIIL8\\ALYSSA";
            Connection_Info.DatabaseName = "CAPROJ";
            Connection_Info.UserID = "sa";
            Connection_Info.Password = "abc@123";
            CrTables = reportviewer.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = Connection_Info;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            foreach (ReportDocument subrep in reportviewer.Subreports)
            {
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in subrep.Database.Tables)
                {
                    table.LogOnInfo.ConnectionInfo = Connection_Info;
                    table.ApplyLogOnInfo(table.LogOnInfo);
                }

            }

            //DataTable dtMain = (DataTable)ViewState["ViewingTable"];
            //reportviewer.Database.Tables[0].SetDataSource(dtMain);

            CrystalReportViewer1.ReportSource = reportviewer;

            pubFormulaFields = reportviewer.DataDefinition.FormulaFields;
            if (pubFormulaFields.Count > 0 && strFormulaParam != null)
            {
                for (int i = 0; (i <= (strFormulaParam.Length - 1)); i++)
                {
                    if ((strFormulaParam[i].Trim().Length > 0))
                    {
                        reportviewer.DataDefinition.FormulaFields[strFormulaParam[i]].Text = strFormulaValue[i];
                    }
                }
            }
        }
    }
}