﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class SpillageRM : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                LoadScheduleForViewing();
            }
        }

        //Load Schedule Details for Viewing and Editing
        public void LoadScheduleForViewing()
        {
            //Load Supplier Products
            string SQL = "select a.*,b.material_name from dbo.md_spillage_raw_materials a inner join md_raw_materials b on a.Material_ID = b.Material_ID";
            GridSpillageRM.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridSpillageRM.DataSource = dt;
                GridSpillageRM.DataBind();
                GridSpillageRM.Visible = true;
              
                ViewState["ViewingTable"] = dt;
                
            }
            
        }

    }
}