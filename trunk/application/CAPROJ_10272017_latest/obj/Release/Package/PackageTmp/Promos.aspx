﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Promos.aspx.cs" Inherits="ETIIOnlineServices.Promos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Promos</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px">
            <div class="col-xs-12 no-padding">
                <div class="box" style="padding:10px 10px 10px 10px">
                    <div class="row">
                        <div class="box-header">
                            <h3 class="box-title">Latest Promos</h3>
                        </div><!-- /.box-header -->
                        <div class="col-xs-12">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                              <!-- Indicators -->
                              <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<%--                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>--%>
                              </ol>

                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <center>
                                      <img src="images/promos/promos-may-16-30-floresdemayo.png" alt="Flores De Mayo">
                                  </center>
<%--                                  <div class="carousel-caption">
                                    <h3><font color="black">Flores De Mayo</font></h3>
                                    <p><font color="black">Flores de Mayo promo from May 16-30, 2015 9:00am - 6:00pm.
Enjoy additional discounts and freebies for outright unit reservations and downpayments</font></p>
                                  </div>--%>
                                </div>

<%--                                <div class="item">
                                  <img src="img_chania2.jpg" alt="Chania">
                                </div>

                                <div class="item">
                                  <img src="img_flower.jpg" alt="Flower">
                                </div>

                                <div class="item">
                                  <img src="img_flower2.jpg" alt="Flower">
                                </div>--%>
                              </div>

                              <!-- Left and right controls -->
                              <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
        </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</asp:Content>

