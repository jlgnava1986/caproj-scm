﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CommonLib;
using CAPROJ.Modules;
using System.IO;
using System.Text;

namespace CAPROJ
{
    public partial class ProductionCapacityPlanning : System.Web.UI.Page
    {
        SQLProcedures _SQL = new SQLProcedures();
        PublicProperties PublicProperty = new PublicProperties();
        DataTable dtCurrentTable;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((PublicProperty.validAllowedMenu != null && PublicProperty.validAllowedMenu.AsEnumerable().Where(row => row.Field<string>("ch_menu_link").Trim() == Request.Url.ToString().Split('/').Last()).ToList().Count == 0) || Session["username"] == null)
                {
                    Response.Redirect("AccessDenied.aspx");
                }
                _SQL.getDropdown("select * from md_production_capacity_header", "Production_ID", "Production_ID", dropProduction);
                LoadProductionForViewing();
                dropFinishedGoods.Enabled = false;
                dropFinishedGoods.CssClass = "form-control";
                txtCycleTime.Enabled = false;
                txtCycleTime.CssClass = "form-control";
                btnEdit.Enabled = true;
                btnEdit.CssClass = "btn btn-block btn-danger";
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-block btn-danger";

                GridSchedule.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (GridSchedule.Rows.Count > 0)
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    DataTable dtNewTable = new DataTable();
                    dtNewTable.Columns.Add(new DataColumn("material_name", typeof(string)));

                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            DropDownList dropItems = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMaterials");

                            drCurrentRow = dtNewTable.NewRow();

                            drCurrentRow["material_name"] = dropItems.SelectedValue.ToString();
                            dtNewTable.Rows.Add(drCurrentRow);

                            rowIndex++;
                        }
                        ViewState["CurrentTable"] = dtNewTable;
                        if (Session["VerifyIfAddOrEdit"].ToString() == "Add")
                        {
                            InsertProduction();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Production Successfully Added!'); window.location='" + Request.ApplicationPath + "ProductionCapacityPlanning.aspx';", true);
                        }
                        else
                        {
                            UpdateProduction();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Production Successfully Updated!'); window.location='" + Request.ApplicationPath + "ProductionCapacityPlanning.aspx';", true);
                        }
                    }
                }
            }
        }
        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("material_name", typeof(string)));
            dr = dt.NewRow();
            dr["material_name"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridSchedule.DataSource = dt;
            GridSchedule.DataBind();

            DropDownList dropItemList = (DropDownList)GridSchedule.Rows[0].Cells[0].FindControl("dropMaterials");
            dropItemList.CssClass = "form-control";
            _SQL.getDropdown("select * from md_raw_materials", "Material_ID", "material_name", dropItemList);
        }

        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMaterials");

                        drCurrentRow = dtCurrentTable.NewRow();

                        dtCurrentTable.Rows[i - 1]["material_name"] = itemList.SelectedValue.ToString().Trim();

                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    GridSchedule.DataSource = dtCurrentTable;
                    GridSchedule.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList itemList = (DropDownList)GridSchedule.Rows[rowIndex].Cells[0].FindControl("dropMaterials");
                        _SQL.getDropdown("select * from md_raw_materials", "Material_ID", "material_name", itemList);

                        if (Session["Viewing"].ToString() == "Add")
                        {
                            itemList.SelectedIndex = itemList.Items.IndexOf(itemList.Items.FindByValue(dt.Rows[i]["material_name"].ToString()));
                        }
                        else
                        {
                            itemList.SelectedIndex = itemList.Items.IndexOf(itemList.Items.FindByText(dt.Rows[i]["material_name"].ToString()));
                        }
                        rowIndex++;
                    }
                }
            }
         }

        private void SetPreviousDataForViewing()
        {
            if (ViewState["ViewingTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["ViewingTable"];
                if (dt.Rows.Count > 0)
                {
                    DataTable dt1 = new DataTable();
                    DataRow dr = null;
                    dt1.Columns.Add(new DataColumn("material_name", typeof(string)));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Insert the rows on Datatable
                        dr = dt1.NewRow();
                        dr["material_name"] = dt.Rows[i]["material_name"];
                        dt1.Rows.Add(dr);

                        ViewState["CurrentTable"] = dt1;
                        GridSchedule.DataSource = dt1;
                        GridSchedule.DataBind();
                    }
                    //Display the value on Gridview
                    SetPreviousData();
                }
            }
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            Session["Viewing"] = "Add";
            AddNewRowToGrid();
        }

        public void InsertProduction()
        {
            string query = "";
            dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            //header
            query = "INSERT INTO md_production_capacity_header(FinishedGoods_ID,cycle_time)VALUES('" + dropFinishedGoods.SelectedValue.ToString() + "','" + txtCycleTime.Text + "');";
            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();

            //detail
            query = "";
            query = "DELETE FROM md_production_capacity_detail where FinishedGoods_ID='" + dropFinishedGoods.SelectedValue.ToString() + "';";
            foreach (DataRow row in dtCurrentTable.Rows)
            {
                if (row["material_name"].ToString() != "")
                {
                    query = query + "INSERT INTO md_production_capacity_detail(Production_ID,FinishedGoods_ID,Material_ID) VALUES((select max(Production_ID) from md_production_capacity_header),'" + dropFinishedGoods.SelectedValue.ToString() + "','" + row["material_name"].ToString() + "');";
                }
            }
            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();

        }

        public void UpdateProduction()
        {
            string query = "";
            dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            //header
            query = "UPDATE md_production_capacity_header SET FinishedGoods_ID='" + dropFinishedGoods.SelectedValue.ToString() + "',cycle_time='" + txtCycleTime.Text + "' WHERE Production_ID='" + dropProduction.SelectedValue.ToString() + "';";
            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();

            //detail
            query = "";
            query = "DELETE FROM md_production_capacity_detail where Production_ID='" + dropProduction.SelectedValue.ToString() + "';";
            foreach (DataRow row in dtCurrentTable.Rows)
            {
                if (row["material_name"].ToString() != "")
                {
                    query = query + "INSERT INTO md_production_capacity_detail(Production_ID,FinishedGoods_ID,Material_ID) VALUES('" + dropProduction.SelectedValue.ToString() + "','" + dropFinishedGoods.SelectedValue.ToString() + "','" + row["material_name"].ToString() + "');";
                }
            }
            _SQL.Open();
            _SQL.ExecuteNonQuery(query);
            _SQL.Close();

        }

        public void LoadProductionForViewing()
        {
            _SQL.getDropdown("select a.*,b.finished_goods_name from md_production_capacity_header a inner join md_finished_goods b on a.FinishedGoods_ID = b.FinishedGoods_ID where a.Production_ID='" + dropProduction.SelectedValue.ToString() + "'","FinishedGoods_ID", "finished_goods_name", dropFinishedGoods);

            _SQL.Open();
            SqlDataReader dr = _SQL.ExecuteReader("select cycle_time from md_production_capacity_header where Production_ID='" + dropProduction.SelectedValue.ToString() + "'");
            if (dr.HasRows)
            {
                dr.Read();
                txtCycleTime.Text = dr[0].ToString().Trim();
            }
            _SQL.Close();

            string SQL = "select * from md_production_capacity_planning where FinishedGoods_ID='" + dropFinishedGoods.SelectedValue.ToString() + "'";
            GridScheduleViewing.DataSource = null;
            DataTable dt = _SQL.FillDataTable(SQL);
            if (dt.Rows.Count > 0)
            {
                GridScheduleViewing.DataSource = dt;
                GridScheduleViewing.DataBind();
                GridScheduleViewing.Visible = true;
                GridSchedule.Visible = false;
                ViewState["ViewingTable"] = dt;
                btnCancel.Visible = false;
            }
            else
            {
                GridScheduleViewing.Visible = false;
                GridSchedule.Visible = true;

                //Add blank row on Gridview
                SetInitialRow();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnCancel.Visible = true;
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            dropProduction.Enabled = false;
            dropProduction.CssClass = "form-control";
            dropFinishedGoods.Enabled = true;
            dropFinishedGoods.CssClass = "form-control";
            txtCycleTime.Enabled = true;
            txtCycleTime.CssClass = "form-control";

            //Hide Gridview for viewing of products
            GridScheduleViewing.Visible = false;
            //Show Gridview for adding new products
            GridSchedule.Visible = true;
            //Assign data to Gridview from Datatable
            Session["Viewing"] = "Edit";
            SetPreviousDataForViewing();

            Session["VerifyIfAddOrEdit"] = "Edit";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductionCapacityPlanning.aspx");
        }

        protected void dropFinishedGoods_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadProductionForViewing();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            _SQL.getDropdown("select * from md_finished_goods", "FinishedGoods_ID", "finished_goods_name", dropFinishedGoods);
            dropProduction.Enabled = false;
            dropProduction.CssClass = "form-control";
            dropFinishedGoods.Enabled = true;
            dropFinishedGoods.CssClass = "form-control";
            btnSave.Enabled = true;
            btnSave.CssClass = "btn btn-block btn-danger";
            btnEdit.Enabled = false;
            btnEdit.CssClass = "btn btn-block btn-danger";
            btnAdd.Enabled = false;
            btnAdd.CssClass = "btn btn-block btn-danger";
            txtCycleTime.Enabled = true;
            txtCycleTime.CssClass = "form-control";
            txtCycleTime.Text = "";
            GridScheduleViewing.Visible = false;
            GridSchedule.Visible = true;
            SetInitialRow();

            Session["VerifyIfAddOrEdit"] = "Add";
        }
    }
}