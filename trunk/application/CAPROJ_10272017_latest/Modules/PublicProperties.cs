﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace CAPROJ.Modules
{
    public class PublicProperties
    {

        static string strMessage;
        public string validMessage
        {
            get { return strMessage; }
            set { strMessage = value; }
        }

        static DataTable dtAllowedMenu;
        public DataTable validAllowedMenu
        {
            get { return dtAllowedMenu; }
            set { dtAllowedMenu = value; }
        }
    }
}