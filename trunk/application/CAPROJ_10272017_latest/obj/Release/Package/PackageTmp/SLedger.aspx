﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SLedger.aspx.cs" Inherits="ETIIOnlineServices.SLedger" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Agent's Portal
                <small>Commissions</small>
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
            <div class="col-xs-12 no-padding">
                <%--<div class="box" style="padding:10px 10px 10px 10px">--%>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="row">
                            <div class="pull-left col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <h3><asp:Label ID="lblHeader" runat="server" Text="Label">Commissions</asp:Label></h3>
                            </div>
                            <div class="pull-left col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 0px 15px">
                                <asp:DropDownList ID="dropFilter" runat="server" CssClass="form-control" OnSelectedIndexChanged="dropFilter_OnSelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Selected="True" Value="0">Processing</asp:ListItem>
                                    <asp:ListItem Value="1">Bank Processing</asp:ListItem>
                                    <asp:ListItem Value="3">For Release</asp:ListItem>
                                    <asp:ListItem Value="4">Released</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="pull-right col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding:10px 15px 10px 15px">
                                    <asp:Button ID="btnBackCommission" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-danger pull-right" CommandName="Action"
                                        Text='Back' OnCommand="ShowCommissionSummary"/>
                            </div>
                        </div>
                        <!-- Commission Summary -->
                        <div class="box-body table-responsive no-padding">
                            <asp:GridView ID="GridCommissionSummary" runat="server" AutoGenerateColumns="False" 
                                GridLines="None"  AllowPaging="true"
                                OnPageIndexChanging="GridCommissionSummary_PageIndexChanging"
                                BorderStyle="None" Class="table" AllowSorting="True">
                                <Columns>
                                    <asp:BoundField DataField="ch_batch_no" HeaderText="Batch No.">
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ch_cdf_no" HeaderText="CDF No.">
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="10"/>
                                    </asp:BoundField>

                                    <asp:BoundField DataField="trans_date" HeaderText="Date" dataformatstring="{0:MM/dd/yyyy}">
                                        <HeaderStyle HorizontalAlign="left" Width="150px" />
                                        <HeaderStyle HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Remarks" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                    </asp:BoundField>

                                    <asp:BoundField DataField="netcomm" HeaderText="Total Amount" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                        <HeaderStyle HorizontalAlign="left" Width="150px" />
                                        <HeaderStyle HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:TemplateField ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnViewDetailsComm" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-info" CommandName="Action"
                                                Text='View Details' OnCommand="LoadCommissionDetails"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle HorizontalAlign="Center" />
                                <FooterStyle BackColor="#CCCC99" />
                                <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                <SortedDescendingHeaderStyle BackColor="#575357" />
                            </asp:GridView>
                        </div>
                        <!-- Commission Details -->
                        <div class="box-body table-responsive no-padding">
                            <asp:GridView ID="GridCommissionDetails" runat="server" AutoGenerateColumns="False" 
                                    GridLines="None"  AllowPaging="true"
                                    OnPageIndexChanging="GridCommissionDetails_PageIndexChanging"              
                                    BorderStyle="None" Class="table" AllowSorting="True" PagerStyle-BackColor="#F7F7DE">
                                    <Columns>
                                        <asp:BoundField DataField="RowNum" HeaderText="">
                                            <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="Small" ForeColor="Red"/>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="ch_floor_id" HeaderText="Floor">
                                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="nu_unit_number" HeaderText="Unit No.">
                                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="dpinstallmentno" HeaderText="Installment No.">
                                            <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="vc_org_name" HeaderText="Client Name">
                                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="netcomm" HeaderText="Gross Amount" DataFormatString="{0:#,##0.00;(#,##0.00);0}">
                                            <HeaderStyle HorizontalAlign="left" Width="150px" />
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Center" />
                                    <FooterStyle BackColor="#CCCC99" />
                                    <HeaderStyle BackColor="#0079AA" Font-Bold="True" ForeColor="White"/>
                                    <PagerStyle CssClass="paging" BackColor="#F7F7DE" HorizontalAlign="Center"/>
                                    <PagerSettings  Mode="NumericFirstLast" FirstPageText="First" PreviousPageText="&laquo;" NextPageText="Next" LastPageText="&raquo;" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                </asp:GridView>
                        </div>
                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                            <ProgressTemplate>
                                <div class="divWaiting">            
                                <center>
                                    <div style="width:40%; height:40%;">
                                            Please wait...
                                        <div class="progress progress-striped active" width="40%" height="40%">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                                    </div>
                                </center>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>                           
                    </ContentTemplate>
                    </asp:UpdatePanel>
                <%--</div> --%>       
            </div>
        </section>
    </aside>    
    <p>&nbsp;</p>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
    });
</script>
</asp:Content>

