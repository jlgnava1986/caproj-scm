﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SampleComp.aspx.cs" Inherits="ETIIOnlineServices.SampleComp" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Dashboard_Portal" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                Sample Computation Sheet
            </h1>
        </section>

        <section class="content" style="padding:10px 10px 10px 10px; background-color:#f9f9f9">
                    <%--<div class="row" style="padding:0px 10px 10px 10px">--%>
                        <div class="col-xs-12 no-padding">
                           <%-- <div class="box" style="padding:10px 10px 10px 10px">--%>
                                <!--Put the calendar to top of other controls-->
                                <script type="text/javascript">
                                    function OnClientShown(sender, args) {
                                        sender._popupBehavior._element.style.zIndex = 10005;
                                    }
                                </script>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
<%--                                        <h3>Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h3>
                                        <asp:LinkButton ID="lnkFake" runat="server" />
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                                            OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                                            <div class="header">
                                                Session Expiring!
                                            </div>
                                            <div class="body">
                                                Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
                                                Do you want to reset?
                                            </div>
                                            <div class="footer" align="right">
                                                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
                                                <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
                                            </div>
                                        </asp:Panel>--%>

                                        <div class="row">                                    
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <h4>Unit to Purchase</h4>
                                            </div>
                                        </div>                                        
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                                <!-- PROJECT -->
                                                <small>Project</small>
                                                <asp:DropDownList ID="dropProject" runat="server" CssClass="form-control" 
                                                    AutoPostBack="true" 
                                                    onselectedindexchanged="dropProject_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                                                <small>Floor No.</small>
                                                <!-- FLOOR -->
                                                <asp:DropDownList ID="dropFloor" runat="server" CssClass="form-control" 
                                                    AutoPostBack="true" AppendDataBoundItems="true"
                                                    onselectedindexchanged="dropFloor_SelectedIndexChanged">

                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                                                <!-- UNIT NO -->
                                                    <small>Unit No.</small>
                                                <asp:DropDownList ID="dropUnits" runat="server" CssClass="form-control" 
                                                    AutoPostBack="true" 
                                                    onselectedindexchanged="dropUnits_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                                                <!-- AREA SQM -->
                                                <small>Area (sqm.)</small>
                                                <asp:TextBox ID="txtAreaSqm" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                <!-- UNIT TYPE -->
                                                <small>Unit Type</small>
                                                <asp:TextBox ID="txtUnitType" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>                                    
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                <!-- UNIT MODEL -->
                                                <small>Unit Model</small>
                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>                                    
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                <!-- UNIT ORIENTATION -->
                                                <small>Unit Orientation</small>
                                                <asp:TextBox ID="txtOrientation" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3" style="padding-top:10px">
                                                <!-- VATABLE TAGGING -->
                                                <asp:CheckBox ID="cbVatable" runat="server" CssClass="checkbox" Text="Vatable" Enabled="false"></asp:CheckBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- SALE TYPE -->
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                                <small>Sale Type</small>
                                                <asp:DropDownList ID="dropSaleType" runat="server" CssClass="form-control" 
                                                    onselectedindexchanged="dropSaleType_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                                <!-- FINANCING SCHEME -->
                                                <small>Fin. Scheme</small>
                                                <asp:DropDownList ID="dropFinScheme" runat="server" CssClass="form-control" 
                                                    AutoPostBack="true" 
                                                    onselectedindexchanged="dropFinScheme_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-4">
                                                <!-- CIRCULAR NO -->
                                                <small>Circular No.</small>
                                                <asp:DropDownList ID="dropCircular" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                                                <small>Birth Date</small>
                                                <asp:TextBox ID="txtBirthdate1" runat="server" CssClass="form-control" ValidationGroup="txtDate"
                                                    ontextchanged="txtBirthdate_TextChanged" AutoPostBack="True" placeholder="mm/dd/yyyy">
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtBirthdate" runat="server" CssClass="form-control"
                                                    ontextchanged="txtBirthdate_TextChanged" TextMode="Date" placeholder="mm/dd/yyyy" AutoPostBack="true">
                                                </asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtenderBDay" runat="server"
                                                             TargetControlID="txtBirthdate1"
                                                             Mask="99/99/9999"
                                                             MessageValidatorTip="true"
                                                             OnFocusCssClass="MaskedEditFocus"
                                                             OnInvalidCssClass="MaskedEditError"
                                                             MaskType="Date"
                                                             DisplayMoney="Left"
                                                             AcceptNegative="Left"
                                                            ErrorTooltipEnabled="True" />
                                                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator5" runat="server"
                                                            ControlExtender="MaskedEditExtenderBDay"
                                                            ControlToValidate="txtBirthdate1"
                                                            EmptyValueMessage="Date is required"
                                                            InvalidValueMessage="Date is invalid"
                                                            Display="Dynamic"
                                                            TooltipMessage="Input a date"
                                                            EmptyValueBlurredText="Birthdate is required."
                                                            InvalidValueBlurredMessage="*"
                                                            ValidationGroup="txtDate" />
                                            </div>
                                        </div>
    <%--                                    <div class="row">                                        
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr />
                                                <h4>Payment Plan</h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <!-- PAYMENT PLAN -->
                                                <small>Payment Plan</small>
                                                <asp:DropDownList ID="dropPaymentPlan" runat="server" CssClass="form-control" 
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <hr />
                                                <h4>Computation Details</h4>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" style="padding:0px 2px 0px 2px">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Total Contract Price</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            List Price(LP)
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtListPrice" runat="server" CssClass="form-control" Enabled="false" DataFormatString="{0:#,##0.00;(#,##0.00);0}" style="text-align:right" Text="0.00"></asp:TextBox>
                                                        </div>
                                                
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="red">Intro Disc (10%)</font>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtListPriceDisc" runat="server" CssClass="form-control" Enabled="false" DataFormatString="{0:#,##0.00;(#,##0.00);0}" style="text-align:right" Text="0.00"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            Discounted LP
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtDiscountedLP" runat="server" CssClass="form-control" Enabled="false" DataFormatString="{0:#,##0.00;(#,##0.00);0}" style="text-align:right" Text="0.00"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            TCP Fscheme Disc
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:DropDownList ID="dropFinschemeDisc" runat="server" CssClass="form-control" 
                                                                AutoPostBack="true" 
                                                                 onselectedindexchanged="dropFinschemeDisc_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="red">TCP Fscheme Disc</font>
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtFinschemeDiscTCP" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            Net List Price A
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtNetListPriceA" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            LP Promo Discount
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:DropDownList ID="dropPromo" runat="server" CssClass="form-control" 
                                                                AutoPostBack="true" Enabled="false"
                                                                onselectedindexchanged="dropPromo_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="red">LP Promo Disc Amt</font>
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtPromoDiscAmount" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            Add Disc (%)
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox AutoPostBack="true" ID="txtAdditionalDisc" runat="server" CssClass="form-control" Enabled="false"
                                                                Text="0.00" style="text-align:right" MaxLength="5" OnTextChanged="txtAdditionalDisc_TextChanged"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="True" TargetControlID="txtAdditionalDisc" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="red">Add Disc Amount</font>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtAddDiscAmount" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            Net List Price B
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtNetListPriceB" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="green">+Vat<asp:Label ID="lblVatPcent" runat="server" Text=""></asp:Label></font>
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtVatAmount" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>

<%--                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="green">+Improvement</font>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtImprovement" runat="server" CssClass="form-control" Enabled="false"  Text="0.00" style="text-align:right"></asp:TextBox>
                                                        </div>--%>

<%--                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            Net List Price C
                                                        </div>
                                                         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtNetListPriceC" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right" Visible="false"></asp:TextBox>
                                                        </div>--%>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <font color="blue">TCP</font>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <asp:TextBox ID="txtTCP" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" Enabled="True" TargetControlID="txtTCP" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>                             
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" style="padding:0px 2px 0px 2px">
                                            <div class="panel panel-primary no-padding">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Down Payment</h3>
                                                </div>
                                                <div class="panel-body" style="padding:15px 0px 15px 0px">
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        DP %
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPPcent" runat="server" CssClass="form-control" 
                                                            MaxLength="3" Text="0" style="text-align:right" AutoPostback="true" 
                                                            ontextchanged="txtDPPcent_TextChanged"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" Enabled="True" TargetControlID="txtDPPcent" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="blue">Total DP Amount</font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtTotalDP" runat="server" CssClass="form-control" 
                                                            Enabled="false" Font-Bold="true" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="True" TargetControlID="txtTotalDP" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        DP Finscheme Disc
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:DropDownList ID="dropDPFinschemeDisc" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                                onselectedindexchanged="dropFinschemeDisc1_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
    
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="red">DP Fscheme Disc Amt</font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtFinschemeDiscDP" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="True" TargetControlID="txtFinschemeDiscDP" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                         <font color="red">Res. Fee</font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:DropDownList ID="dropReservationFee" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="dropReservationFee_SelectedIndexChanged">
                                                            <asp:ListItem Value="15000" Selected="True">15,000</asp:ListItem>
                                                            <asp:ListItem Value="20000">20,000</asp:ListItem>
                                                            <asp:ListItem Value="25000">25,000</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        Net DP Amount 
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPTotal1" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" onkeyup="javascript:this.value=Comma(this.value);" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" Enabled="True" TargetControlID="txtDPTotal1" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <hr />
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        DP Term (mos.)
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <asp:DropDownList ID="dropDPTerm" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="dropDPTerm_SelectedIndexChanged">
<%--                                                            <asp:ListItem Value="0" Selected="True">0</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        DP Mode
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:DropDownList ID="dropDPMode" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="dropDPMode_SelectedIndexChanged">
                                                            <asp:ListItem Value="1" Selected="True">Spread DP</asp:ListItem>
                                                            <asp:ListItem Value="2">Outright, Spread</asp:ListItem>
                                                            <asp:ListItem Value="3">Spread, Balloon</asp:ListItem>
                                                            <asp:ListItem Value="4">Outright</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:Label ID="lblDP1" runat="server" Text="DP 1 %"></asp:Label>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPRate1" runat="server" CssClass="form-control" MaxLength="5" AutoPostBack="true"  OnTextChanged="txtDPRate1_TextChanged" Text="0" style="text-align:right"></asp:TextBox>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:Label ID="lblDP2" runat="server" Text="DP 2 %"></asp:Label>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPRate2" runat="server" Enabled="false" 
                                                            CssClass="form-control" MaxLength="5" Text="0" style="text-align:right" 
                                                            ontextchanged="txtDPRate2_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="True" TargetControlID="txtDPRate2" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <hr />
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="blue"><asp:Label ID="lblDP1Amount" runat="server" Text="DP Amount 1"></asp:Label></font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPAmount1" runat="server" CssClass="form-control"
                                                            Enabled="false"  Text="0.00" style="text-align:right;"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="True" TargetControlID="txtDPAmount1" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="blue"><asp:Label ID="lblDP2Amount" runat="server" Text="DP Amount 2"></asp:Label></font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtDPTotal2" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" Enabled="True" TargetControlID="txtDPAmount1" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" style="padding:0px 2px 0px 2px">
                                            <div class="panel panel-primary no-padding">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Miscellaneous</h3>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        Improvement Fee
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtImprovement1" runat="server" CssClass="form-control" Enabled="false"  Text="0.00" style="text-align:right"></asp:TextBox>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        Transfer Fee Rate
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtTransferFeeRate" runat="server" CssClass="form-control" 
                                                            Text="0.00" style="text-align:right" AutoPostback="true" Enabled="false"
                                                            ontextchanged="txtTransferFeeRate_TextChanged"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="True" TargetControlID="txtTransferFeeRate" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        CCT Transfer Fee
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtTransferFee" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="True" TargetControlID="txtTransferFee" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        Move In Fee
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtMoveinFee" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" Enabled="True" TargetControlID="txtMoveinFee" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                       <font color="blue">Total Misc.</font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtTotalMisc" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="True" TargetControlID="txtTotalMisc" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" style="padding:0px 2px 0px 2px">
                                            <div class="panel panel-primary no-padding">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Monthly Amortization</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="blue"><asp:Label ID="lblLoanable" runat="server" Text="Loanable"></asp:Label></font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtLoanableAmount" runat="server" CssClass="form-control" 
                                                            Enabled="false" Text="0.00" style="text-align:right" TabIndex="1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True" TargetControlID="txtTCP" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
<%--                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        Total MA
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtTotalMA" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="True" TargetControlID="txtTotalMA" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>--%>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        MA Term(yrs)
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:DropDownList ID="dropMATerm" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="dropMATerm_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        MA Int. Rate
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:DropDownList ID="dropMAIntRate" runat="server" CssClass="form-control" 
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="dropMATerm_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <font color="blue"><asp:Label ID="lblEst" runat="server" Text=""></asp:Label> Monthly MA</font>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <asp:TextBox ID="txtMonthlyMA" runat="server" CssClass="form-control" Enabled="false" Text="0.00" style="text-align:right"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="True" TargetControlID="txtMonthlyMA" FilterType="Numbers, Custom" FilterMode="ValidChars" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">   
<%--                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="btn btn-flat btn-success btn-block" CommandName="Action"
                                                    Text='Print' onclick="btnPrint_Click" onClientClick="SetTarget();"/>
                                            </div>--%>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:Button ID="btnEmail" runat="server" CausesValidation="False" 
                                                    CssClass="btn btn-flat btn-success btn-block" CommandName="Action"
                                                    Text='Preview Sample Computation' onclick="btnEmailv2_Click"/>
                                            </div>
                                        </div>
                                        <!-- COMPOSE MESSAGE MODAL -->
                                        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog" style="width: 80%; height: 800px;">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title"><i class="fa fa-bar-chart-o"></i> Preview</h4>
                                                    </div>
                                                    <div class="modal-body" style="height:auto; min-height:100%;">
                                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <iframe id="PDFView" frameborder="0" style="width:100%; height:800px;" runat="server"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <br/> 
                                                            <h4 class="modal-title"><i class="fa fa-envelope"></i> Send to Email</h4>
                                                            <br/>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">TO:</span>
                                                                <asp:TextBox ID="txtEmailTo" runat="server" class="form-control" placeholder="Email Address"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">CC:</span>
                                                                    <asp:TextBox ID="txtEmailCC" runat="server" class="form-control" placeholder="Email CC"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <br/>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtMessage" runat="server" class="form-control" placeholder="Message" TextMode="MultiLine" Columns="50" Rows="5"></asp:TextBox>
                                                            </div>
                                                            <asp:Button ID="btnSend" runat="server" CausesValidation="False"
                                                                            CssClass="btn btn-primary pull-left" CommandName="Action"
                                                                            Text='Send Email' onclick="btnSend_Click"/>
                                                        </div>
                                                        <div class="modal-footer">
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                                            <ProgressTemplate>
                                                <div class="divWaiting">            
                                                <center>
                                                    <div style="width:40%; height:40%;">
                                                            Please wait...
                                                        <div class="progress progress-striped active" width="40%" height="40%">
                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </center>
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </ContentTemplate>                           
                                    <Triggers>
                                       <asp:AsyncPostbackTrigger ControlID="dropSaleType" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropFinScheme" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropProject" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropFloor" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropUnits" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropFinschemeDisc" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropPromo" EventName="SelectedIndexChanged" />
                                       <asp:AsyncPostbackTrigger ControlID="dropDPmode" EventName="SelectedIndexChanged" />                                   
                                       <asp:AsyncPostbackTrigger ControlID="dropMATerm" EventName="SelectedIndexChanged" />                                   
                                       <asp:AsyncPostbackTrigger ControlID="dropReservationFee" EventName="SelectedIndexChanged" />
                                       <%--<asp:AsyncPostbackTrigger ControlID="txtAdditionalDisc" EventName="onclick" />--%>
                                    </Triggers>
                                </asp:UpdatePanel>
                            <%--</div>--%>
                        </div>
                   <%--</div>--%>

                    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
                        AutoDataBind="True" DisplaGroupTree="False" GroupTreeImagesFolderUrl="" 
                        Height="10px" ToolbarImagesFolderUrl="" 
                        ToolPanelView="None" ToolPanelWidth="10px" Width="10px" 
                        Visible="False">
                    </CR:CrystalReportViewer>

<%--                <!-- PRINT PREVIEW -->
                <div class="modal fade" id="divPrint" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Sample Computation Sheet</h4>
                            </div>
                            <form action="#" method="post">
                                <div class="modal-body">                                    
                                    <div class="form-group">
                                        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"></CR:CrystalReportViewer>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->--%>
            </section>
        </aside>    
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="StyleSelection">
    <style type="text/css">
        .style1
        {
            width: 96px
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>

    <script type="text/javascript">
        //Calendar Extender on DP Start
//        function AttachCalendarEvent() {
//            var dp = $('#<%=txtBirthdate.ClientID%>');
//                dp.datepicker({
//                    changeMonth: true,
//                    changeYear: true,
//                    format: "mm/dd/yyyy",
//                    language: "tr"
//                }).on('changeDate', function (ev) {
//                    $(this).blur();
//                    $(this).datepicker('hide');
//                });
//                dp.setAttribute("onkeyup", txt.getAttribute("onchange"));
//                dp.setAttribute("onchange", "");
//        }
        window.onload = function () {
            AttachCalendarEvent();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttachCalendarEvent();
                    }
                });
            }
        };

        //Text Changed Event on Additional Discount
        function AttachEvent3() {
            var txt = document.getElementById("<%=txtDPRate1.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttachEvent3();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttachEvent3();
                    }
                });
            }
        };

        //Text Changed Event on DP Pcent
        function AttachEvent4() {
            var txt = document.getElementById("<%=txtDPPcent.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttachEvent4();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttachEvent4();
                    }
                });
            }
        };

        //Text Changed Event on Additional Discount
        function AttactEvent() {
            var txt = document.getElementById("<%=txtAdditionalDisc.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttactEvent();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttactEvent();
                    }
                });
            }
        };

        //Text Changed Event on DP Rate 1
        function AttactEvent6() {
            var txt = document.getElementById("<%=txtDPRate1.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttactEvent6();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttactEvent6();
                    }
                });
            }
        };

        //Text Changed Event on DP Rate 2
        function AttactEvent7() {
            var txt = document.getElementById("<%=txtDPRate2.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttactEvent7();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttactEvent7();
                    }
                });
            }
        };

        //Text Changed Event on Reservation Fee 
        function AttactEvent1() {
            var txt = document.getElementById("<%=txtDPRate2.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttactEvent1();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttactEvent1();
                    }
                });
            }
        };

        //Text Changed Event on Transfer Fee Rate
        function AttactEventTF() {
            var txt = document.getElementById("<%=txtTransferFeeRate.ClientID %>");
            txt.setAttribute("onkeyup", txt.getAttribute("onchange"));
            txt.setAttribute("onchange", "");
        }
        window.onload = function () {
            AttactEventTF();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        AttactEventTF();
                    }
                });
            }
        };
    </script>

    <%--<script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Default.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.
            window.location = window.location.href;
        }
    </script>--%>
</asp:Content>

